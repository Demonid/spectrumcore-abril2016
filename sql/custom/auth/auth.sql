-- Quest Completer
DROP TABLE IF EXISTS `quest_completer`;
CREATE TABLE `quest_completer` (
    `id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;

-- RBAC Custom Commands
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1000','quest complete command');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1001','quest complete status');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1002','quest complete add');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1003','quest complete del');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1004','quest complete reload');

REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1005','raid');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1006','raid info');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1007','raid list');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1008','player');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1009','player info');

REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1010','utility');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1011','utility customize');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1012','utility changerace');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1013','utility changefaction');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1014','utility unbind');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1015','utility additem');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1016','online');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1017','online account');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1018','online character');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1019','wintrade');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES('1020','disband arena');
