-- Raid Info
DROP TABLE IF EXISTS `character_raidinfo`;
CREATE TABLE `character_raidinfo` (
  `Guid` bigint(20) NOT NULL AUTO_INCREMENT,
  `GuildId` int(10) NOT NULL,
  `GuildName` varchar(255) NOT NULL,
  `CreatureEntry` int(10) NOT NULL,
  `CreatureName` varchar(255) NOT NULL,
  `PlayerCount` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LeaderName` varchar(255) NOT NULL,
  `RaidId` int(6) NOT NULL,
  `KillDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KillData` longtext NOT NULL,
  `MapId` smallint(6) unsigned NOT NULL DEFAULT '0',
  `MapName` varchar(255) NOT NULL,
  `MapSpawnMode` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MapMaxPlayers` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CompletedEncounters` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `TotalEncounters` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- GM Log
DROP TABLE IF EXISTS `gm_command_log`;
CREATE TABLE `gm_command_log` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logTime` int(10) unsigned NOT NULL DEFAULT '0',
  `gmAccId` int(10) unsigned NOT NULL DEFAULT '0',
  `gmCharGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `gmCharName` varchar(100) NOT NULL DEFAULT '',
  `targetType` varchar(12) NOT NULL DEFAULT '',
  `targetAccId` int(10) unsigned NOT NULL DEFAULT '0',
  `targetCharGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `targetCharName` varchar(100) NOT NULL DEFAULT '',
  `command` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Transmogrification: custom_transmogrification table
CREATE TABLE IF NOT EXISTS `custom_transmogrification` (
  `GUID` int(10) unsigned NOT NULL COMMENT 'Item guidLow',
  `FakeEntry` int(10) unsigned NOT NULL COMMENT 'Item entry',
  `Owner` int(10) unsigned NOT NULL COMMENT 'Player guidLow',
  PRIMARY KEY (`GUID`),
  KEY `Owner` (`Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='6_2';

-- Transmogrification: custom_transmogrification_sets table
CREATE TABLE IF NOT EXISTS `custom_transmogrification_sets` (
  `Owner` int(10) unsigned NOT NULL COMMENT 'Player guidlow',
  `PresetID` tinyint(3) unsigned NOT NULL COMMENT 'Preset identifier',
  `SetName` text COMMENT 'SetName',
  `SetData` text COMMENT 'Slot1 Entry1 Slot2 Entry2',
  PRIMARY KEY (`Owner`,`PresetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='6_1';

-- Reforging: custom_reforging table
CREATE TABLE `custom_reforging` (
  `GUID` INT(10) UNSIGNED NOT NULL COMMENT 'item guid low',
  `increase` INT(10) UNSIGNED NOT NULL COMMENT 'stat_type',
  `decrease` INT(10) UNSIGNED NOT NULL COMMENT 'stat_type',
  `stat_value` INT(10) NOT NULL DEFAULT '0' COMMENT 'stat change',
  `Owner` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'player guid',
  PRIMARY KEY (`GUID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

-- player punishment SQL
CREATE TABLE `custom_punishment` (
    `guid` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`guid`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;
