DELETE FROM `creature` WHERE id = 10808; -- Timmy the Cruel
DELETE FROM `creature` WHERE id = 15677 AND map = 571; -- Auctioneer Graves
DELETE FROM `creature` WHERE id = 31146 and map = 571; -- Raider's Training Dummy
DELETE FROM `creature` WHERE id in (32638,32639,32641,32642); -- Vendors
DELETE FROM `creature` WHERE id = 190010; -- Transmog
DELETE FROM `creature` WHERE id = 190011; -- Reforge
DELETE FROM `creature` WHERE id = 190012; -- Arena Spectator
DELETE FROM `creature` WHERE id = 190013; -- Transmog PvP
INSERT INTO `creature` (`id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`) VALUES
('10808','329','3738.86','-3259.54','129.338','3.00126'),  -- Timmy the Cruel
('15677','571','5983.39','603.602','650.612','2.95885'),   -- Auctioneer Graves
('15677','571','5622.04','691.181','651.972','6.03968'),
('15677','571','5625.38','698.656','651.977','6.06088'),
('15677','571','5986.19','610.931','650.607','3.15284'),
('31146','571','5711.69','671.433','645.763','3.82829'),   -- Raider's Training Dummy
('31146','571','5914.11','655.541','643.772','4.29952'),
('32638','571','5704.94','648.046','646.204','2.58343'),   -- Vendors
('32639','571','5706.07','649.971','646.206','2.30068'),
('32641','571','5889.86','648.322','645.866','5.64277'),
('32642','571','5890.71','650.912','645.878','5.74486'),
('190010','0','-8824.68','636.634','94.2395','2.2799'),    -- Transmog
('190010','571','5913.89','617.228','646.425','0.904865'),
('190010','1','1614.48','-4398.93','11.0942','1.72864'),
('190010','571','5694.84','682.99','645.673','4.00384'),
('190011','1','1610.08','-4399.95','10.8692','1.52836'),   -- Reforge
('190011','571','5697.71','680.386','645.766','3.99991'),
('190011','0','-8822.93','637.84','94.2329','2.00109'),
('190011','571','5911.59','619.082','646.586','0.893471'),
('190012','571','5816.15','621.83','609.158','2.26503'),   -- Arena Spectator
('190012','571','5677.68','659.315','647.163','0.893475'),
('190012','571','5928.61','640.57','645.564','4.22278'),
('190013','571','5812.19','614.55','609.158','2.3894');    -- Transmog PvP

-- Spawn Portales en Orgrimmar y Stormwind
DELETE FROM `gameobject` WHERE id = 202079 AND map = 0 OR id = 202079 AND map = 1;
INSERT INTO `gameobject` (`id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation2`, `rotation3`) VALUES
('202079','0','-8842.04','471.764','109.621','3.80168','0.946028','-0.324084'),
('202079','0','-8859.57','492.092','109.599','3.79775','0.946663','-0.322226'),
('202079','1','1680.8','-4316.17','61.4573','1.33804','0.620219','0.784429'),
('202079','1','1656.85','-4326.73','61.7507','2.68893','0.974496','0.224405');

/*-- Set visible intendants of The Sons of Hodir and Knights of the Ebon Blade
UPDATE `creature` SET phaseMask=65535 WHERE `id` in (32538,32540);*/

-- GM Helper: Item
DELETE FROM `item_template` WHERE entry = 81000;
INSERT INTO `item_template` (`entry`, `name`, `displayid`, `Quality`, `Flags`, `spellid_1`, `bonding`, `description`, `ScriptName`) VALUES
('81000','GM Virtual','30658','5','32','22838','1','Como un GM de Bolsillo!','gm_helper');

/****************************************** Icecrown Citadel ******************************************/

-- ICC Teleport
REPLACE INTO `game_tele` (`id`, `position_x`, `position_y`, `position_z`, `orientation`, `map`, `name`) VALUES('1424','5796.81','2074.77','636.064','3.58455','571','ICC');

-- Lord Marrowgar: Set inhabit type for coldflame npc trigger
UPDATE `creature_template` SET `InhabitType` = 1 WHERE `entry` = 36672;

-- Lord Marrowgar: Set bone spike immunities
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36619,38233,38459,38460);

-- Lady Deathwhisper: Vengeful Shade Movement Speed
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry` IN (38222);

-- Lady Deathwhisper: delete spell script Cultist Dark Martyrdom
DELETE FROM `spell_script_names` WHERE `spell_id` IN (70903, 71236, 72495, 72496, 72497, 72498, 72499, 72500) AND `ScriptName` = "spell_cultist_dark_martyrdom";

-- Deathbringer Saurfang: Update Flags on Triggers
UPDATE `creature_template` SET unit_flags = 33554432, flags_extra = 128 WHERE entry IN (39010, 39011, 39012, 39013);

-- Deathbringer Saurfang: Set npc_blood_beast Scriptname
UPDATE creature_template SET ScriptName = 'npc_blood_beast' WHERE entry = 38508;

-- Rotface: Condition for Vile Gas
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` IN (72272, 72273);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `Comment`) VALUES
('13','4','72272','31','4','vile gas 10h player only target'),
('13','2','72272','31','4','vile gas 10h player only target'),
('13','1','72272','31','4','vile gas 10h player only target'),
('13','2','72273','31','4','vile gas 25h player only target'),
('13','4','72273','31','4','vile gas 25h player only target'),
('13','1','72273','31','4','vile gas 25h player only target');

-- Rotface: Add immunities to Rotface
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36627,38390,38549,38550);

-- Rotface: Add immunities to Little Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36897,38138);

-- Rotface: Add immunities to Big Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36899,38123);

-- Rotface: Make Big Ooze slower
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.5 WHERE `entry` IN (36899, 38123);

-- Festergut: Immunitie to interrupt
UPDATE creature_template SET mechanic_immune_mask = 650854271 WHERE entry in (36626,37504,37505,37506);

-- Professor Putricide: Excluyendo a la abominacion del efecto del stun del choking gas bomb
DELETE FROM `spell_script_names` WHERE spell_id IN (71278, 72460, 72619, 72620, 71279, 72459, 72621, 72622);
INSERT INTO `spell_script_names` (spell_id, ScriptName) VALUES
(71278, 'spell_putricide_choking_gas_bomb_effect'),
(72460, 'spell_putricide_choking_gas_bomb_effect'),
(72619, 'spell_putricide_choking_gas_bomb_effect'),
(72620, 'spell_putricide_choking_gas_bomb_effect'),
(71279, 'spell_putricide_choking_gas_bomb_effect'),
(72459, 'spell_putricide_choking_gas_bomb_effect'),
(72621, 'spell_putricide_choking_gas_bomb_effect'),
(72622, 'spell_putricide_choking_gas_bomb_effect');

-- Professor Putricide Tear Gas for enemies
DELETE FROM `spell_script_names` WHERE spell_id = 71615 AND `scriptName` LIKE 'spell_putricide_tear_gas';
INSERT INTO `spell_script_names` VALUES (71615, 'spell_putricide_tear_gas');

-- Professor Putricide: Update Immunities for Volatile Ooze and Gas Gloud
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37697,38604,38758,38759); -- Volatile Ooze
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37562,38602,38760,38761); -- Gas Cloud

-- Professor Putricide: Update for Volatile Ooze and Gas Cloud
UPDATE `creature_template` SET speed_walk = 1, speed_run = 1 WHERE `entry` IN (37697,38604,38758,38759); -- Volatile Ooze
UPDATE `creature_template` SET speed_walk = 1, speed_run = 1 WHERE `entry` IN (37562,38602,38760,38761); -- Gas Cloud

-- Professor Putricide: Update phase 3 model bounding radius and combat reach 
UPDATE `creature_model_info` SET `BoundingRadius`=1.209, `CombatReach`=7.8 WHERE  `DisplayID`=30993;

-- BloodCouncil: Add Immunities to Kinectic Bomb
UPDATE creature_template SET mechanic_immune_mask = 650854271 WHERE entry IN (38454,38775,38776,38777);

-- BloodCouncil: Add Immunities to Dark Nucleus
UPDATE creature_template SET mechanic_immune_mask = 650854271 WHERE entry = 38369;

-- Valithria Dreamwalker: Lich King missing text
DELETE FROM `creature_text` WHERE `entry`=16980;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(16980,0,0,"Intruders have breached the inner sanctum! Hasten the destruction of the green dragon, leave only bones and sinew for the reanimation!",14,0,0,0,0,17251,"Valithria Dreamwalker - SAY_LICH_KING_INTRO");

-- Valithria Dreamwalker: only 2 equip items should drop
UPDATE `gameobject_loot_template` SET `maxcount` = 2 WHERE `entry` = 28052 AND `reference` = 34241;

-- Sindragosa - Ice Tomb resistance
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = '70157' AND `spell_effect` = '69700' AND `type` = '2' AND `comment` = 'Sindragosa - Ice Tomb resistance';

-- Sindragosa: Fix spell 69762 Unchained Magic - Add internal cooldown with 1 seconds, so multi-spell spells will only apply one stack of triggered spell 69766 (i.e. Chain Heal)
DELETE FROM `spell_proc_event` WHERE `entry` = 69762;
INSERT INTO `spell_proc_event` (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(69762, 0, 0, 0, 0, 0, 81920, 0, 0, 0, 1);

-- Sindragosa: Set InhabitType = 4
UPDATE creature_template SET InhabitType = 4 WHERE `entry` IN (36853,38265,38266,38267); -- Sindragosa
UPDATE creature_template SET InhabitType = 4 WHERE `entry` IN (37533,38220); -- Rimefang
UPDATE creature_template SET InhabitType = 4 WHERE `entry` IN (37534,38219); -- Spinestalker

-- Sindragosa: Update Immunities for Rimefang, Spinestalker and Ice Tomb
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37533,38220); -- Rimefang
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (37534,38219); -- Spinestalker
UPDATE `creature_template` SET mechanic_immune_mask = 650854271 WHERE entry IN (36980, 38320, 38321, 38322); -- Ice Tomb

-- Lich King: Update Immunities for Shambling Horror, Drudge Ghoul
UPDATE `creature_template` SET mechanic_immune_mask = 650851199 WHERE entry IN (37698,39299,39300,39301); -- Shambling Horror
UPDATE `creature_template` SET mechanic_immune_mask = 650851199 WHERE entry IN (37695,39309,39310,39311); -- Drudge Ghoul

-- Lich King: Drudge Ghoul Scriptname
UPDATE `creature_template` SET `ScriptName` = 'npc_drudge_ghoul' WHERE `entry` = '37695';

-- Lich King: Update Immunities for Ice Sphere
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36633,39305,39306,39307); -- Ice Sphere

-- Lich King: Update Immunities for Raging Spirit
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (36701,39302,39303,39304); -- Raging Spirit

-- Lich King: Update Immunities and Speed for Valkyr Shadowguard
UPDATE creature_template SET mechanic_immune_mask = 2145367039 WHERE entry IN (36609, 39120, 39121, 39122); -- Valkyr Shadowguard
UPDATE creature_template SET speed_walk = 0.642857, speed_run =0.642857 WHERE `entry` IN (36609, 39120, 39121, 39122); -- Valkyr Shadowguard

-- Lich King: Update Immunities and speed for Vile Spirits
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (37799, 39284, 39285, 39286); -- Vile Spirits
UPDATE creature_template SET speed_walk = 0.5, speed_run = 0.5 WHERE `entry` IN (37799, 39284, 39285, 39286); -- Vile Spirits

-- The Lich King: Vile Spirit Scriptname
UPDATE `creature_template` SET `ScriptName` = 'npc_vile_spirit' WHERE `entry` = '37799';

-- Lich King: Update Immunities and speed for Wicked Spirit
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry IN (39190, 39287, 39288, 39289); -- Wicked Spirit
UPDATE creature_template SET speed_walk = 0.5, speed_run = 0.5 WHERE `entry` IN (39190, 39287, 39288, 39289); -- Wicked Spirit

-- Lich King: Update Immunities and speed for Spirit Bomb
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299839 WHERE entry =39189; -- Spirit Bomb
UPDATE `creature_template` SET speed_walk = 0.5, speed_run = 0.5 WHERE entry =39189; -- Spirit Bomb

-- The Lich King: Wicked Spirit Scriptname
UPDATE `creature_template` SET `ScriptName` = 'npc_wicked_spirit' WHERE `entry` = 39190;
UPDATE `creature_template` SET `unit_flags` = 512 WHERE `entry` IN (39190, 39287, 39288, 39289);

-- Lich King: 25HC Updating Loot
UPDATE `creature_loot_template` SET `maxcount` = '2' WHERE `entry` = '39168' AND `item` = '1';
UPDATE `creature_loot_template` SET `maxcount` = '3' WHERE `entry` = '39168' AND `item` = '2';

-- Delete Hellscream's Warsong - Strength of Wrynn
DELETE FROM `spell_area` WHERE spell IN (73822, 73828);

/****************************************** Icecrown Citadel ******************************************/

-- Raid Info: Textos de anuncio para el Boss kill
DELETE FROM `trinity_string` WHERE `entry` IN (10100, 10101, 10102, 10103, 10104, 10105, 10106, 10107, 10108, 10109, 10110);
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(10100, '|Cffff0000[Raid Info] |cff00ffff%s %uN: |Cff00ff00%s |cff00ffffderrotado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10101, '|Cffff0000[Raid Info] |cff00ffff%s %uH: |Cff00ff00%s |cff00ffffderrotado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10102, '|Cffff0000[Raid Info] |cff00ffff%s %uN: |Cff00ff00Gemelas Valkyr |cff00ffffderrotadas por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10103, '|Cffff0000[Raid Info] |cff00ffff%s %uH: |Cff00ff00Gemelas Valkyr |cff00ffffderrotadas por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10104, '|Cffff0000[Raid Info] |cff00ffff%s %uN: |Cff00ff00Consejo de Sangre |cff00ffffderrotado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10105, '|Cffff0000[Raid Info] |cff00ffff%s %uH: |Cff00ff00Consejo de Sangre |cff00ffffderrotado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10106, '|Cffff0000[Raid Info] |cff00ffff%s %uN: |Cff00ff00Batalla Naval |cff00ffffsuperado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10107, '|Cffff0000[Raid Info] |cff00ffff%s %uH: |Cff00ff00Batalla Naval |cff00ffffsuperado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10108, '|Cffff0000[Raid Info] |cff00ffff%s %uN: |Cff00ff00%s |cff00ffffrescatada por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10109, '|Cffff0000[Raid Info] |cff00ffff%s %uH: |Cff00ff00%s |cff00ffffrescatada por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10110, '|Cffff0000[Raid Info] |cff00ffff%s: |Cff00ff00%s |cff00ffffderrotado por la hermandad |Cff00ff00%s|cff00ffff. Jugadores: %u/%u    Progresion: %u/%u    Detalle: .raid info %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- Info Commands
-- Strings para el Info Player
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 11300 AND 11310;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(11300, '|cff00ffff========== PLAYER INFO ===========', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11301, '|Cff00ff00 Nombre:|r %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11302, '|Cff00ff00 Hermandad:|r %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11303, '|Cff00ff00 Clase&Rama:|r %s  -  %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11304, '|Cff00ff00 Talentos:|r %u  -  %u  -  %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11305, '|Cff00ff00 Posible Rol:|r %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11306, '|Cff00ff00 GS total:|r %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11307, '|Cff00ff00 ilvl equipado:|r %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11308, '|Cff00ff00 MMR 2c2 3c3 5c5:|r %u  -  %u  -  %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11309, '  - WoWAsura - Puedes usar .raid list $nombre para conocer los cds de raids/dungeons de un jugador', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11310, '|cff00ffff=================================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- Strings para el Info Raid
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 11311 AND 11321;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(11311, '|cff00ffff==================== RAID INFO ====================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11312, '|Cff00ff00 RaidID:|r %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11313, '|Cff00ff00 Mapa&Dificultad:|r %s, %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11314, '|Cff00ff00 Encuentros:|r %u/%u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11315, '|Cff00ff00 Lider:|r  %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11316, '|Cff00ff00 Jugadores:|r %u/%u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11317, '|Cff00ff00 Posibles Roles:|r %u Tankes - %u Healers - %u Dps', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11318, '|Cff00ff00 GS Promedio:|r %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11319, '|Cff00ff00 ilvl Promedio:|r %u', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11320, '  - WoWAsura - Puedes usar .player info $nombre para conocer detalles de un jugador online', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11321, '|cff00ffff=================================================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- Strings para el List Raid
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 11322 AND 11327;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(11322, '|cff00ffff==================== RAID LIST ====================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11323, '|Cff00ff00 Jugador:|r  %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11324, '|Cff00ff00 RaidID:|r %u    |Cff00ff00 Mapa&Dificultad:|r %s, %s    [ACTIVO]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11325, '|Cff00ff00 RaidID:|r %u    |Cff00ff00 Mapa&Dificultad:|r %s, %s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11326, '  - WoWAsura - Puedes usar .raid info $raidId para conocer la composicion de un raid activo aqui listado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11327, '|cff00ffff=================================================', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- Comando
DELETE FROM `command` WHERE `name`LIKE 'raid';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES 
('raid', '1005', 'Sintaxis: .player $subcommand\r\nTeclea .player para ver un listado de posibles subcomandos.');
DELETE FROM `command` WHERE `name`LIKE 'raid info';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('raid info','1006','Sintaxis: .raid info #idraid.\r\nPuede que ninguno de los integrantes de este RaidID este conectado, o sea erroneo!');
DELETE FROM `command` WHERE `name`LIKE 'raid list';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('raid list','1007','Sintaxis: .raid list $jugador o target actual.\r\nMuestra la lista de las raids activas del objetivo! Funciona siempre que haya entrado a la instance al menos una vez y este en grupo.');
DELETE FROM `command` WHERE `name`LIKE 'player';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES 
('player', '1008', 'Sintaxis: .player $subcommand\r\nTeclea .player para ver un listado de posibles subcomandos.');
DELETE FROM `command` WHERE `name`LIKE 'player info';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('player info','1009','Sintaxis: .player info $jugador o target actual.\r\n\nMuestra informacion de interes del objetivo!');

-- Transmogrification: text
DELETE FROM `npc_text` WHERE ID IN (50000, 50001);
SET @TEXT_ID := 50000;
INSERT INTO `npc_text` (`ID`, `text0_0`) VALUES
(@TEXT_ID, 'Transmogrification allows you to change how your items look like without changing the stats of the items.\r\nItems used in transmogrification are no longer refundable, tradeable and are bound to you.\r\nUpdating a menu updates the view and prices.\r\n\r\nNot everything can be transmogrified with eachother.\r\nRestrictions include but are not limited to:\r\nOnly armor and weapons can be transmogrified\r\nGuns, bows and crossbows can be transmogrified with eachother\r\nFishing poles can not be transmogrified\r\nYou must be able to equip both items used in the process.\r\n\r\nTransmogrifications stay on your items as long as you own them.\r\nIf you try to put the item in guild bank or mail it to someone else, the transmogrification is stripped.\r\n\r\nYou can also remove transmogrifications for free at the transmogrifier.'),
(@TEXT_ID+1, 'You can save your own transmogrification sets.\r\n\r\nTo save, first you must transmogrify your equipped items.\r\nThen when you go to the set management menu and go to save set menu,\r\nall items you have transmogrified are displayed so you see what you are saving.\r\nIf you think the set is fine, you can click to save the set and name it as you wish.\r\n\r\nTo use a set you can click the saved set in the set management menu and then select use set.\r\nIf the set has a transmogrification for an item that is already transmogrified, the old transmogrification is lost.\r\nNote that same transmogrification restrictions apply when trying to use a set as in normal transmogrification.\r\n\r\nTo delete a set you can go to the set\'s menu and select delete set.');

DELETE FROM `trinity_string` WHERE entry BETWEEN 11100 AND 11110;
SET @STRING_ENTRY := 11100;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(@STRING_ENTRY+0, 'Item transmogrified'),
(@STRING_ENTRY+1, 'Equipment slot is empty'),
(@STRING_ENTRY+2, 'Invalid source item selected'),
(@STRING_ENTRY+3, 'Source item does not exist'),
(@STRING_ENTRY+4, 'Destination item does not exist'),
(@STRING_ENTRY+5, 'Selected items are invalid'),
(@STRING_ENTRY+6, 'Not enough money'),
(@STRING_ENTRY+7, 'You don\'t have enough tokens'),
(@STRING_ENTRY+8, 'Transmogrifications removed'),
(@STRING_ENTRY+9, 'There are no transmogrifications'),
(@STRING_ENTRY+10, 'Invalid name inserted');

-- Transmogrification: NPC
DELETE FROM `creature_template` WHERE entry = 190010;
SET
@Entry = 190010,
@Name = "Warpweaver";

INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `scale`, `rank`, `dmgschool`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`) VALUES
(@Entry, 19646, 0, @Name, "Transmogrifier", NULL, 0, 80, 80, 2, 35, 1, 1, 0, 0, 2000, 0, 1, 0, 7, 138936390, 0, 0, 0, '', 0, 3, 1, 0, 0, 1, 0, 0, 'Creature_Transmogrify');

-- Reforging: NPC
DELETE FROM `creature_template` WHERE entry = 190011;
SET
@Entry = 190011,
@Name = "Thaumaturge Vashreen";

INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `scale`, `rank`, `dmgschool`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`) VALUES
(@Entry, 20988, 0, @Name, "Arcane Reforger", NULL, 0, 80, 80, 2, 35, 1, 1, 0, 0, 2000, 0, 1, 0, 7, 138936390, 0, 0, 0, '', 0, 3, 1, 0, 0, 1, 0, 0, 'REFORGER_NPC');

-- Arena Spectator: Emotes are not allowed
DELETE FROM `trinity_string` WHERE `entry` = 12000;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(12000, 'No puedes hacer eso mientras eres espectador.');

--  Arena Spectator: NPC
DELETE FROM creature_template WHERE entry = '190012';
INSERT INTO creature_template (entry, modelid1, NAME, subname, IconName, gossip_menu_id, minlevel, maxlevel, HealthModifier, ManaModifier, ArmorModifier, faction, npcflag, speed_walk, speed_run, scale, rank, DamageModifier, unit_class, unit_flags, TYPE, type_flags, InhabitType, RegenHealth, flags_extra, ScriptName) VALUES 
('190012', '29348', "Arena Spectator", "WoWAsura", 'Speak', '50000', 71, 71, 1.56, 1.56, 1.56, 35, 3, 1, 1.14286, 3, 1, 1, 1, 2, 7, 138936390, 3, 1, 2, 'npc_arena_spectator');

-- Transmogrification PvP: NPC
DELETE FROM `creature_template` WHERE entry = 190013;
SET
@Entry = 190013,
@Name = "Warpweaver PvP";

INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `scale`, `rank`, `dmgschool`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`) VALUES
(@Entry, 20986, 0, @Name, "Transmogrifier", NULL, 0, 80, 80, 2, 35, 1, 1, 0, 0, 2000, 0, 1, 0, 7, 138936390, 0, 0, 0, '', 0, 3, 1, 0, 0, 1, 0, 0, 'npc_transmogrify');

-- Arena Spectator: Arena doors
UPDATE `gameobject_template` SET `flags` = `flags` | 4 WHERE entry IN (185918, 185917, 183970, 183971, 183972, 183973, 183977, 183979, 183978, 183980, 192642, 192643);

-- Camisas de Ilusion
DELETE FROM `item_template` WHERE entry IN (80001,80002,80003,80004,80005,80006,80007,80008,80009);
INSERT INTO `item_template` (`entry`, `class`, `subclass`, `name`, `displayid`, `Quality`, `InventoryType`, `spellid_1`, `spelltrigger_1`, `bonding`, `description`) VALUES
('80001','4','1','Ilusion de Humana','10058','6','4','37805','1','1','Gracias por donar!'),
('80002','4','1','Ilusion de Humano','10058','6','4','35466','1','1','Gracias por donar!'),
('80003','4','1','Ilusion de Elfa de Sangre','10058','6','4','37806','1','1','Gracias por donar!'),
('80004','4','1','Ilusion de Elfo de Sangre','10058','6','4','37807','1','1','Gracias por donar!'),
('80005','4','1','Ilusion de Tauren Female','10058','6','4','37811','1','1','Gracias por donar!'),
('80006','4','1','Ilusion de Tauren','10058','6','4','37810','1','1','Gracias por donar!'),
('80007','4','1','Ilusion de Gnoma','10058','6','4','37809','1','1','Gracias por donar!'),
('80008','4','1','Ilusion de Gnomo','10058','6','4','37808','1','1','Gracias por donar!'),
('80009','4','1','Ilusion de Garrosh','10058','6','4','42016','1','1','Gracias por donar!');

-- Warlock: Glyph of Seduction spellscript
DELETE FROM `spell_script_names` WHERE `spell_id`=6358;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(6358,'spell_warl_glyph_of_seduction');

-- Removes Master's Call stun immunity.
DELETE FROM `spell_linked_spell` WHERE spell_trigger = 54216;
INSERT INTO `spell_linked_spell`(`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (54216,-56651,1,'Removes Master''s Call stun immunity');

-- Druid: Nature Grasp ICD 2 Seconds
DELETE FROM `spell_proc_event` WHERE `entry` IN (16689,16810,16811,16812,16813,17329,27009,53312);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`)VALUES 
(16689, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16810, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16811, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16812, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(16813, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(17329, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(27009, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
(53312, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2);

-- Exploit: Fixes some Items/Enchants that should not benefit from Spell Power
DELETE FROM `spell_bonus_data` WHERE `entry` IN (10577,27655,28788,38395,55756,6297,20004,28005,20006,44525,28715,38616,43731,43733,30567,14795);
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
-- Items
(10577,0,0,0,0,'Item - Gauntlets of the Sea (Heal)'),
(27655,0,0,0,0,'Item - Heart of Wyrmthalak (Flame Lash)'),
(28788,0,0,0,0,'Item - Paladin T3 (8)'),
(38395,0,0,0,0,'Item - Warlock T5 (2)'),
(55756,0,0,0,0,'Item - Brunnhildar weapons (Chilling Blow)'),
-- Enchants
(6297,0,0,0,0,'Enchant - Fiery Blaze'),
(20004,0,0,0,0,'Enchant - Lifestealing'),
(28005,0,0,0,0,'Enchant - Battlemaster'),
(20006,0,0,0,0,'Enchant - Unholy Weapon'),
(44525,0,0,0,0,'Enchant - Icebreaker'),
-- Consumables
(28715,0,0,0,0,'Consumable - Flamecap (Flamecap Fire)'),
(38616,0,0,0,0,'Poison - Bloodboil Poison'),
(43731,0,0,0,0,'Consumable - Stormchops (Lightning Zap)'),
(43733,0,0,0,0,'Consumable - Stormchops (Lightning Zap)'),
(30567,0,0,0,0,'Consumable - Torment of the Worgen'),
(14795,0,0,0,0,'Poison - Venomhide Poison Debuff - should not get bonuses');

-- Dk: Death Grip spell_linked_spell
DELETE FROM `spell_linked_spell`  WHERE `spell_trigger` IN ('49576');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES
('49576', '49560', 'Death Grip');

-- Herbs and Minerals = x3
-- Borean Tundra
UPDATE `pool_template` SET `max_limit`=75 WHERE `entry`=987;  -- Herbs 25
UPDATE `pool_template` SET `max_limit`=90 WHERE `entry`=9900; -- Ore   30
-- Howling Fjord
UPDATE `pool_template` SET `max_limit`=90 WHERE `entry`=986;  -- Herbs 30
UPDATE `pool_template` SET `max_limit`=90 WHERE `entry`=9901; -- Ore   30
-- Grizzly Hills
UPDATE `pool_template` SET `max_limit`=60 WHERE `entry`=984;  -- Herbs 20
UPDATE `pool_template` SET `max_limit`=60 WHERE `entry`=9902; -- Ore   20
-- Dragonblight
UPDATE `pool_template` SET `max_limit`=105 WHERE `entry`=985;  -- Herbs 35
UPDATE `pool_template` SET `max_limit`=90 WHERE `entry`=5122; -- Ore    30
-- Zul'Drak
UPDATE `pool_template` SET `max_limit`=120 WHERE `entry`=983;  -- Herbs 40
UPDATE `pool_template` SET `max_limit`=90 WHERE `entry`=9904; -- Ore    30
-- Storm Peaks
UPDATE `pool_template` SET `max_limit`=105 WHERE `entry`=981;  -- Herbs 35
UPDATE `pool_template` SET `max_limit`=105 WHERE `entry`=9907; -- Ore   35
-- Sholazar Basin
UPDATE `pool_template` SET `max_limit`=120 WHERE `entry`=982;  -- Herbs 40
UPDATE `pool_template` SET `max_limit`=105 WHERE `entry`=9905; -- Ore   35
-- Icecrown
UPDATE `pool_template` SET `max_limit`=120 WHERE `entry`=980;  -- Herbs 40
UPDATE `pool_template` SET `max_limit`=180 WHERE `entry`=9908;-- Ore    60
-- Wintergrasp
UPDATE `pool_template` SET `max_limit`=45 WHERE `entry`=979;  -- Herbs  15
UPDATE `pool_template` SET `max_limit`=45 WHERE `entry`=896;  -- Ore    15

-- Battlegrounds Blizzlike Chance
UPDATE `battleground_template` SET MinPlayersPerTeam = 3 WHERE id = '32'; -- Random battleground
UPDATE `battleground_template` SET Weight = 8, MinPlayersPerTeam = 3 WHERE id = '2'; -- Warsong Gulch
UPDATE `battleground_template` SET Weight = 6, MinPlayersPerTeam = 3 WHERE id = '7'; -- Eye of The Storm
UPDATE `battleground_template` SET Weight = 6, MinPlayersPerTeam = 3 WHERE id = '3'; -- Arathi Basin
UPDATE `battleground_template` SET Weight = 6, MinPlayersPerTeam = 3 WHERE id = '9'; -- Strand of the Ancients
UPDATE `battleground_template` SET Weight = 1 WHERE id = '1'; -- Alterac Valley
UPDATE `battleground_template` SET Weight = 2 WHERE id = '30'; -- Isle of Conquest

-- Disable Alterac Valley, Isle of Conquest and Strand of the Ancients
DELETE FROM `disables` WHERE sourcetype = 3 AND entry IN (1,9,30);
INSERT INTO `disables` (`sourceType`, `entry`, `comment`) VALUES
('3','1','Disable - Alterac Valley'),
('3','9','Disable - Strand of the Ancients'),
('3','30','Disable - Isle of Conquest');

-- Disable Culling of Stratholme
DELETE FROM `disables` WHERE entry = '595' AND sourcetype IN (2,8);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(2,595,3,'Disable Culling of Stratholme (entrance)'),
(8,595,3,'Disable Culling of Stratholme (entrance) LFG');

-- Disable The Oculus
DELETE FROM `disables` WHERE entry = '578' AND sourcetype IN (2,8);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(2,578,3,'Disable The Oculus (entrance)'),
(8,578,3,'Disable The Oculus (entrance) LFG');

-- Disable Utgarde Pinnacle
DELETE FROM `disables` WHERE entry = '575' AND sourcetype IN (2,8);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(2,575,3,'Utgarde Pinnacle (entrance)'),
(8,575,3,'Utgarde Pinnacle (entrance) LFG');

DELETE FROM `disables` WHERE entry IN (603,631,650,724) AND sourcetype IN (2,8);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(2,603,3,'Disable Ulduar Raid entrance'),
(2,631,15,'Disable IceCrown Citadel (Entrance)'),
(2,650,3,'Disable Trial of the Champion'),
(8,650,3,'Disable Trial of the Champion LFG'),
(2,724,15,'Disable The Ruby Sanctum (Entrance)');


-- Bags and Totem at Character Creation
DELETE FROM `playercreateinfo_item` WHERE itemid IN (5765, 46978);
INSERT INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES
('0','1','5765','4'),
('0','2','5765','4'),
('0','4','5765','4'),
('0','3','5765','3'),
('0','5','5765','4'),
('0','7','5765','4'),
('0','8','5765','4'),
('0','11','5765','4'),
('0','9','5765','4'),
('11','7','46978','1'),
('2','7','46978','1'),
('6','7','46978','1'),
('8','7','46978','1');

-- Force Brain Freeze and Fingers of Frost to proc only if spell hits
UPDATE `spell_proc_event` SET `procEx` = 0x0040000 WHERE `entry` IN (44546,44548, 44549, 44543, 44545);

-- Remove Fingers of Frost Aura
DELETE FROM `spell_linked_spell`  WHERE `spell_trigger` IN ('-44544');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES
('-44544','-74396','Fingers of Frost - Remove Aura');

-- Add Wintersaber Trainers faction Horde counterpart
DELETE FROM `player_factionchange_reputations` where `alliance_id` = '589' and `horde_id` = '530';
INSERT INTO `player_factionchange_reputations` (`alliance_id`, `horde_id`) VALUES ('589', '530');

-- Shaman: Maelstrom Weapon will be removed after...
DELETE FROM `spell_linked_spell` WHERE `spell_effect`=-53817;
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
-- Lightning Bolt
(403,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 1)'),
(529,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 2)'),
(548,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 3)'),
(915,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 4)'),
(943,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 5)'),
(6041,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 6)'),
(10391,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 7)'),
(10392,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 8)'),
(15207,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 9)'),
(15208,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 10)'),
(25448,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 11)'),
(25449,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 12)'),
(49237,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 13)'),
(49238,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lightning Bolt (Rank 14)'),
-- Chain Lightning
(421,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 1)'),
(930,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 2)'),
(2860,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 3)'),
(10605,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 4)'),
(25439,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 5)'),
(25442,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 6)'),
(49270,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 7)'),
(49271,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Lightning (Rank 8)'),
-- Lesser Healing Wave
(8004,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 1)'),
(8008,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 2)'),
(8010,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 3)'),
(10466,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 4)'),
(10467,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 5)'),
(10468,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 6)'),
(25420,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 7)'),
(49275,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 8)'),
(49276,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Lesser Healing Wave (Rank 9)'),
-- Healing Wave
(331,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 1)'),
(332,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 2)'),
(547,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 3)'),
(913,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 4)'),
(939,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 5)'),
(959,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 6)'),
(8005,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 7)'),
(10395,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 8)'),
(10396,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 9)'),
(25357,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 10)'),
(25391,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 11)'),
(25396,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 12)'),
(49272,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 13)'),
(49273,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Healing Wave (Rank 14)'),
-- Chain Heal
(1064,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 1)'),
(10622,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 2)'),
(10623,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 3)'),
(25422,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 4)'),
(25423,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 5)'),
(55458,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 6)'),
(55459,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Chain Heal (Rank 7)'),
-- Hex
(51514,-53817,0,'Shaman - Remove Maelstrom Weapon on cast Hex');

-- Proc Fix for Solace of the Fallen / Solace of the Defeated
DELETE FROM spell_proc_event WHERE entry=67698;
INSERT INTO spell_proc_event (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(67698, 0, 0, 0, 0, 0, 0, 65536, 0, 0, 0);

DELETE FROM spell_proc_event WHERE entry=67752;
INSERT INTO spell_proc_event (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(67752, 0, 0, 0, 0, 0, 0, 65536, 0, 0, 0);

-- Mage: Remove procs from Frostfire Bolt/Frost & Ice Armors:
DELETE FROM `spell_proc_event` WHERE `entry` IN ('44546', '44548', '44549');
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES
('44546','0','3','544','0','0','0','0','0','0','0'), -- Brain Freeze (Rank 1)
('44548','0','3','544','0','0','0','0','0','0','0'), -- Brain Freeze (Rank 2)
('44549','0','3','544','0','0','0','0','0','0','0'); -- Brain Freeze (Rank 3)

-- Mage: Brain Freeze - Fingers of Frost
UPDATE `spell_proc_event` SET `procEx` = 0x0040000 WHERE `entry` IN (44546,44548, 44549, 44543, 44545);

-- Warrior: Stop Bladestorm when Hand of Protection buff is given
DELETE FROM `spell_linked_spell` WHERE `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(1022, -46924, 1, 'Hand of protection (Rank1) - remove bladestorm'),
(5599, -46924, 1, 'Hand of protection (Rank2) - remove bladestorm'),
(10278, -46924, 1, 'Hand of protection (Rank3) - remove bladestorm'),
(66009, -46924, 1, 'Hand of protection (rank1 dupe) - remove bladestorm');

-- Warrior: Bladestorm should be immune to Cycylone
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = 46924;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
('46924','-33786','2','Bladestorm - Cyclone');

-- Friendly Rep Requiered Reputation Commendation Badges
UPDATE `item_template` SET RequiredReputationFaction = 1119, RequiredReputationRank = 4 WHERE entry = "49702";
UPDATE `item_template` SET RequiredReputationFaction = 1091, RequiredReputationRank = 4 WHERE entry = "44710";
UPDATE `item_template` SET RequiredReputationFaction = 1106, RequiredReputationRank = 4 WHERE entry = "44711";
UPDATE `item_template` SET RequiredReputationFaction = 1098, RequiredReputationRank = 4 WHERE entry = "44713";
UPDATE `item_template` SET RequiredReputationFaction = 1090, RequiredReputationRank = 4 WHERE entry = "43950";

-- Enable MMaps on ToC
DELETE FROM `disables` WHERE `entry` = 649 AND `sourceType` = 7;

-- Night elf shadowmeld en spell_script
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_gen_shadowmeld';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(58984,'spell_gen_shadowmeld');

-- Implement ToC > ICC > Halion Attunement
/*UPDATE `access_requirement` SET completed_achievement = 3917, quest_failed_text = "Debes conseguir el logro [La Llamada de la Cruzada (10 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 631 AND difficulty = 0;
UPDATE `access_requirement` SET completed_achievement = 3916, quest_failed_text = "Debes conseguir el logro [La Llamada de la Cruzada (25 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 631 AND difficulty = 1;
UPDATE `access_requirement` SET completed_achievement = 4530, quest_failed_text = "Debes conseguir el logro [El Trono Helado (10 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 631 AND difficulty = 2;
UPDATE `access_requirement` SET completed_achievement = 4597, quest_failed_text = "Debes conseguir el logro [El Trono Helado (25 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 631 AND difficulty = 3;
UPDATE `access_requirement` SET completed_achievement = 3917, quest_failed_text = "Debes conseguir el logro [La Llamada de la Cruzada (10 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 649 AND difficulty = 2;
UPDATE `access_requirement` SET completed_achievement = 3916, quest_failed_text = "Debes conseguir el logro [La Llamada de la Cruzada (25 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 649 AND difficulty = 3;
UPDATE `access_requirement` SET completed_achievement = 4530, quest_failed_text = "Debes conseguir el logro [El Trono Helado (10 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 724 AND difficulty = 0;
UPDATE `access_requirement` SET completed_achievement = 4597, quest_failed_text = "Debes conseguir el logro [El Trono Helado (25 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 724 AND difficulty = 1;
UPDATE `access_requirement` SET completed_achievement = 4817, quest_failed_text = "Debes conseguir el logro [El Destructor del Crepúsculo (10 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 724 AND difficulty = 2;
UPDATE `access_requirement` SET completed_achievement = 4815, quest_failed_text = "Debes conseguir el logro [El Destructor del Crepúsculo (25 j.)] antes de poder entrar a esta mazmorra." WHERE mapid = 724 AND difficulty = 3;*/

-- Warrior: Stacking enrage(with other enrages) and death wish/wreckincg crew
DELETE FROM `spell_group` WHERE `spell_id`IN(12880,14201,14202,14203,14204,57518,57519,57520,57521,57522,57514,57516) AND `id` = 1107;
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES
(1107, 12880),
(1107, 14201),
(1107, 14202),
(1107, 14203),
(1107, 14204),
(1107, 57518),
(1107, 57519),
(1107, 57520),
(1107, 57521),
(1107, 57522),
(1107, 57514),
(1107, 57516);

-- Blade Warding proc
UPDATE `spell_proc_event` SET `Cooldown` = '0' WHERE `entry` = 64440;

-- Disable Cannnon on Jotunheim Rapid-Fire Harpoon
UPDATE `creature_template` SET spell3 = 0 WHERE entry = 30337;

-- DB/String: Add trinity strings for the lookup online commands.
DELETE FROM `trinity_string` WHERE entry IN (12001, 12002);
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
('12001','Player not online!'),
('12002','Online characters at account %s (Id: %u)');

-- Love is in the Air
UPDATE `creature_template` SET `ScriptName`='npc_apothecary_baxter' WHERE `entry`=36565;
UPDATE `creature_template` SET `ScriptName`='npc_apothecary_hummel' WHERE `entry`=36296;
UPDATE `creature_template` SET `ScriptName`='npc_apothecary_frye' WHERE `entry`=36272;
UPDATE `creature_template` SET `ScriptName`='npc_crazed_apothecary' WHERE `entry`=36568;

-- Creature Text
SET @HUMMEL  := 36296;
SET @FRYE    := 36272;
SET @BAXTER  := 36565;
DELETE FROM `creature_text` WHERE `entry` IN (@HUMMEL,@FRYE,@BAXTER);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@HUMMEL,0,0,'Did they bother to tell you who I am and why I am doing this?',12,0,100,0,0,0,'Apothecary Hummel - SAY_AGGRO_1'),
(@HUMMEL,1,0,'...or are they just using you like they do everybody else?',12,0,100,0,0,0,'Apothecary Hummel - SAY_AGGRO_2'),
(@HUMMEL,2,0,'But what does it matter. It is time for this to end.',12,0,100,0,0,0,'Apothecary Hummel - SAY_AGGRO_3'),
(@HUMMEL,3,0,'Baxter! Get in there and help! NOW!',12,0,100,0,0,0,'Apothecary Hummel - SAY_CALL_BAXTER'),
(@HUMMEL,4,0,'It is time, Frye! Attack!',12,0,100,0,0,0,'Apothecary Hummel - SAY_CALL_FRYE'),
(@HUMMEL,5,0,'Apothecaries! Give your life for the Crown!',12,0,100,0,0,0,'Apothecary Hummel - SAY_SUMMON_ADDS'),
(@HUMMEL,6,0,'...please don''t think less of me.',12,0,100,0,0,0,'Apothecary Hummel - SAY_DEATH'),
(@FRYE,0,0,'Great. We''re not gutless, we''re incompetent.',12,0,100,0,0,0,'Apothecary Frye - SAY_DEATH'), -- Implementar en script
(@BAXTER,0,0,'It has been the greatest honor of my life to serve with you, Hummel.',12,0,100,0,0,0,'Apothecary Baxter - SAY_DEATH'); -- Implementar en script

-- Spell Scripts
DELETE FROM `spell_script_names` WHERE `spell_id` IN (68798,68614);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(68798,'spell_trio_alluring_perfume'),
(68614,'spell_trio_irresistible_cologne');

-- Only one Immunity can be present on player at the time
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (68530,68529);
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(68530,-68529,0,'Remove Perfume Immune when Cologne Immune is applied'),
(68529,-68530,0,'Remove Cologne Immune when Perfume Immune is applied');

-- Loot Template
DELETE FROM `creature_loot_template` WHERE `entry`=36296 AND `item` IN (49715,50250,50471,50446,50741);
-- Insert items into Heart-Shaped Box
SET @BOX := 54537; -- Heart-Shaped Box
DELETE FROM `item_loot_template` WHERE `entry`=@BOX;
INSERT INTO `item_loot_template` (`entry`, `item`, `chance`, `lootmode`, `groupid`, `mincount`, `maxcount`) VALUES
(@BOX,50250,2,1,0,1,1), -- Big Love Rocket
(@BOX,50446,2,1,0,1,1), -- Toxic Wasteling
(@BOX,50471,5,1,0,1,1), -- The Heartbreaker
(@BOX,50741,5,1,0,1,1), -- Vile Fumigator's Mask
(@BOX,49715,5,1,0,1,1), -- Forever-Lovely Rose
(@BOX,49426,100,1,1,2,2); -- 1-2 Emblems of Frost

-- Ruby Sanctum: Halion
SET @CGUID := 600009;

UPDATE `spell_dbc` SET `EffectBasePoints1`=20 WHERE `Id`=70507;
UPDATE `creature_template` SET `unit_flags`=32832 WHERE `entry` IN(40142,40143,40144,40145);
UPDATE `creature_template` SET `flags_extra`=128 WHERE `entry` IN (40081,40470,40471,40472);

DELETE FROM `creature` WHERE `guid`=@CGUID;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `VerifiedBuild`) VALUES
(@CGUID, 40146, 724, 0, 0, 15, 33, 0, 0, 3156.037, 533.2656, 72.97205, 0, 604800, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `spell_script_names` WHERE `ScriptName` IN( 'spell_halion_blazing_aura','spell_halion_combustion_consumption_periodic');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(75886, 'spell_halion_blazing_aura'),
(75887, 'spell_halion_blazing_aura'),
(74803, 'spell_halion_combustion_consumption_periodic'),
(74629, 'spell_halion_combustion_consumption_periodic');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry` IN(75886,75887);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 2, 75886, 0, 0, 31, 0, 3, 40683, 0, 0, 0, 0, '', 'Blazing Aura can only target Living Embers'),
(13, 2, 75887, 0, 0, 31, 0, 3, 40683, 0, 0, 0, 0, '', 'Blazing Aura can only target Living Embers');

-- Fix Halion - Orb Rotation Focus visible for players
UPDATE `creature_template` SET `modelid1`=169, `modelid2`=11686, `flags_extra` = `flags_extra` | 128 WHERE `entry` IN (40091, 43280, 43281, 43282);

-- Ruby Sanctum Immunities
UPDATE `creature_template` SET `mechanic_immune_mask`=`mechanic_immune_mask` | 33554432 where `entry` IN (
39751, 39920,   -- Baltharus the Warborn
39899, 39922,   -- Baltharus the Warborn Clone
39746, 39805,    -- General Zarithrian
39863, 39864, 39944, 39945, -- Halion
40142, 40143, 40144, 40145, -- Twilight Halion
39747, 39823   -- Saviana Ragefire
);

-- Bosses spawntime & corpse remove time
UPDATE `creature` SET `spawntimesecs` = 604800 WHERE `id` IN (39863,39751,39746,39747);
UPDATE `creature_template` SET `rank` = 3 WHERE `entry` IN  (39863,39751,39746,39747);

-- Trash spawntime
UPDATE `creature` SET `spawntimesecs`=1209600 WHERE `map`=724 AND `id` NOT IN (39863,39751,39746,39747);

-- Ruby Sanctum bosses id saving
UPDATE `creature_template` SET `flags_extra` = 1 WHERE `entry` IN 
(39863,39864,39944,39945, -- Halion
39751,39920, -- Baltharus
39746,39805, -- General Zarithrian
39747,39823); -- Saviana Ragefire

-- Restore Hand of A'dal Achievement + Title
DELETE FROM `achievement_reward` WHERE `entry` IN (431);
INSERT INTO `achievement_reward` (`entry`, `title_A`, `title_H`) VALUES
(431,64,64); -- Hand of A'dal

UPDATE `creature_queststarter` SET quest = 10445 WHERE quest = 13432;
UPDATE `quest_template` SET Flags = 200 WHERE id = 10445;

-- Crossfaction Bg
-- UPDATE creature_template SET faction = 1 WHERE entry IN (4255,4257,5134,5135,5139,11948,11949,11997,12050,12096,12127,13086,13096,13138,13216,13257,13296,13298,13299,13317,13318,13319,13320,13326,13327,13331,13422,13437,13438,13439,13442,13443,13447,13546,13576,13577,13598,13617,13797,14187,14188,14284,14762,14763,14765,14766,14768,14769,12047,13396,13358,13080,13078);
-- UPDATE creature_template SET faction = 2 WHERE entry IN (2225,3343,3625,10364,10367,11946,11947,11998,12051,12052,12053,12097,12121,12122,13088,13089,13097,13137,13140,13143,13144,13145,13146,13147,13152,13153,13154,13176,13179,13180,13181,13218,13236,13284,13316,13359,13377,13397,13425,13428,13441,13448,13536,13539,13545,13597,13616,13618,13798,14185,14186,14282,14285,14772,14773,14774,14775,14776,14777,13332,13099,13079);

-- Wintergrasp Temporal
DELETE FROM `creature` WHERE id = 32294 and position_x = 5375.96;
DELETE FROM `creature` WHERE id = 39172 and position_x = 5374.55;
INSERT INTO `creature` (`id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`, `curhealth`, `unit_flags`) VALUES
('32294','571','5375.96','2890.77','409.239','3.50073','12600','134218496'),
('39172','571','5374.55','2894.52','409.239','3.50073','12600','134218496');
UPDATE `creature` SET phaseMask = 1 WHERE guid = 88339;
DELETE FROM `gameobject` WHERE id = 194323;

-- Update Game Events
UPDATE `game_event` SET `start_time`='2016-06-24 00:01:00' WHERE `eventEntry`=1;
UPDATE `game_event` SET `start_time`='2016-12-15 06:00:00' WHERE `eventEntry`=2;
UPDATE `game_event` SET `start_time`='2016-03-27 00:01:00' WHERE `eventEntry`=9;
UPDATE `game_event` SET `start_time`='2016-06-18 00:01:00' WHERE `eventEntry`=10;
UPDATE `game_event` SET `start_time`='2016-09-09 00:01:00' WHERE `eventEntry`=11;
UPDATE `game_event` SET `start_time`='2016-10-18 01:00:00' WHERE `eventEntry`=12;
UPDATE `game_event` SET `start_time`='2016-09-20 00:01:00' WHERE `eventEntry`=24;
UPDATE `game_event` SET `start_time`='2016-11-21 01:00:00' WHERE `eventEntry`=26;
UPDATE `game_event` SET `start_time`='2016-09-19 00:01:00' WHERE `eventEntry`=50;
UPDATE `game_event` SET `start_time`='2016-11-01 01:00:00' WHERE `eventEntry`=51;

UPDATE `quest_template` SET `AllowableRaces`=1101 WHERE `ID`=11921;
UPDATE `quest_template` SET `AllowableRaces`=690 WHERE `ID`=11926;
UPDATE `quest_template` SET `TimeAllowed`=45 WHERE `ID` IN (11922,11731);
UPDATE `quest_template` SET `TimeAllowed`=90 WHERE `ID` IN (11921,11926);
UPDATE `quest_template_addon` SET `PrevQuestId`=11731 WHERE `ID`=11921; -- More Torch Tossing(A) requires Torch Tossing
UPDATE `quest_template_addon` SET `PrevQuestId`=11922 WHERE `ID`=11926; -- More Torch Tossing(H) requires Torch Tossing
UPDATE `quest_template_addon` SET `PrevQuestId`=11731 WHERE `ID`=11657; -- Torch Catching(A) requires Torch Tossing(A)
UPDATE `quest_template_addon` SET `PrevQuestId`=11922 WHERE `ID`=11923; -- Torch Catching(H) requires Torch Tossing(H)
UPDATE `quest_template_addon` SET `PrevQuestId`=11657 WHERE `ID`=11924; -- More Torch Catching(A) requires Torch Catching
UPDATE `quest_template_addon` SET `PrevQuestId`=11923 WHERE `ID`=11925; -- More Torch Catching(H) requires Torch Catching

DELETE FROM `creature_queststarter` WHERE `id` IN (26113,25975);
-- Master Flame/Fire Eater SAI
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN (25975,26113);
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`IN (25975,26113);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25975,0,0,0,19,0,100,0,11731,0,0,0,11,45716,2,0,0,0,0,7,0,0,0,0,0,0,0,'Master Fire Eater - On Quest \'Torch Tossing\' Accepted - Cast \'Torch Tossing Training\''),
(25975,0,1,0,19,0,100,0,11921,0,0,0,11,46630,2,0,0,0,0,7,0,0,0,0,0,0,0,'Master Fire Eater - On Quest \'More Torch Tossing\' Accepted - Cast \'Torch Tossing Practice\''),
(26113,0,0,0,19,0,100,0,11922,0,0,0,11,45716,2,0,0,0,0,7,0,0,0,0,0,0,0,'Master Flame Eater - On Quest \'Torch Tossing\' Accepted - Cast \'Torch Tossing Training\''),
(26113,0,1,0,19,0,100,0,11926,0,0,0,11,46630,2,0,0,0,0,7,0,0,0,0,0,0,0,'Master Flame Eater - On Quest \'More Torch Tossing\' Accepted - Cast \'Torch Tossing Practice\'');

-- [DNT] Torch Tossing Target Bunny
DELETE FROM `creature_template_addon` WHERE `entry`=25535;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(25535,0,0,0,0,0,45720);

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`=25535;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=25535;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25535,0,0,0,8,0,100,0,45732,0,0,0,11,45724,2,0,0,0,0,7,0,0,0,0,0,0,0,'[DNT] Torch Tossing Target Bunny - On Spellhit \'Torch Land\' - Cast \'Brazzier Hit\'');

-- [DNT] Torch Tossing Target Bunny Controller
UPDATE `creature_template` SET `ScriptName`='npc_torch_tossing_target_bunny_controller' WHERE `entry`=25536;

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry` IN (45732);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition` ,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13,1,45732,0,0,1,0,45723,0,0,0,0,'','Spell \'Torch Toss\' can only hit targets with Aura \'Target Indicator\'');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (45719,46651,46630,45723,-46630,45716,-45716);
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(45719,-46630,0,'Torch Tossing Success remove Torch Tossing Practice'),
(46651,-45716,0,'Torch Tossing Success remove Torch Tossing Training'),
(46630,45725 ,2,'Torch Tossing Practice trigger Detect Invisibility'),
(45716,45725 ,2,'Torch Tossing Training trigger Detect Invisibility'),
(46630,-45724,0,'Torch Tossing Practice remove Brazier Hit'),
(45716,-45724,0,'Torch Tossing Training remove Brazier Hit'),
(-46630,-45724,0,'Torch Tossing Practice expired - clear Braziers Hit!'),
(-45716,-45724,0,'Torch Tossing Training expired - clear Braziers Hit!'),
(45723,43313,2,'Target Indicator(duration) trigger Target Indicator(visual)');

-- Fix A Thief's Reward
DELETE FROM `creature_queststarter` WHERE `quest` IN (9365, 9339);
UPDATE `quest_template_addon` SET `NextQuestID`=9365 WHERE `ID` IN (9324,9325,9326,11935);
UPDATE `quest_template_addon` SET `NextQuestID`=9339 WHERE `ID` IN (9330,9331,9332,11933);

SET @OGUID := 345000; -- 20 free guids required
SET @CGUID := 600000; -- 8 free guids required

SET @NPC := @CGUID+1;
SET @PATH := @NPC * 10;

UPDATE `creature_template` SET `difficulty_entry_1`=0 WHERE `entry` IN (25740,25755,25756,25865);
UPDATE `creature_template` SET  `minlevel`=82,`maxlevel`=82, `mechanic_immune_mask`=617299839, `ScriptName`='boss_ahune' WHERE `entry`=25740; -- Ahune
UPDATE `creature_template` SET  `minlevel`=80,`maxlevel`=80, `AIName`='SmartAI' WHERE `entry`=25755; -- Ahunite Hailstone
UPDATE `creature_template` SET  `minlevel`=80,`maxlevel`=80, `AIName`='SmartAI' WHERE `entry`=25756; -- Ahunite Coldwave
UPDATE `creature_template` SET  `minlevel`=80,`maxlevel`=80, `AIName`='SmartAI' WHERE `entry`=25757; -- Ahunite Frostwind
UPDATE `creature_template` SET  `minlevel`=80,`maxlevel`=80, `flags_extra`=`flags_extra`|0x40000000, `mechanic_immune_mask`=617299839, `ScriptName`='npc_frozen_core' WHERE `entry`=25865; -- Frozen Core
UPDATE `creature_template` SET `ScriptName`='npc_ahune_bunny' WHERE `entry`=25745;
UPDATE `creature_template` SET `ScriptName`='npc_earthen_ring_flamecaller' WHERE `entry`=25754;
UPDATE `creature_template` SET `unit_flags`=33554432, `MovementType`=2 WHERE `entry` IN (25964,25965,25966); -- Shaman beam bunny
UPDATE `creature_template` SET `unit_flags`=33554432 WHERE `entry`=26239; -- Ghost of Ahune
UPDATE `creature_template` SET `flags_extra`=128 WHERE `entry`=25985; -- Ahune Ice Spear Bunny
UPDATE `gameobject_template` SET `ScriptName`='go_ahune_ice_stone' WHERE `entry`=187882;
UPDATE `creature` SET `orientation`=2.408554 WHERE `guid`=202734; -- Luma
UPDATE `creature` SET `orientation`=3.804818 WHERE `guid`=202737; -- Flamecaller
UPDATE `creature_template` SET `HealthModifier`=94.5, `unit_flags`=33554688 WHERE `entry`=25865;
UPDATE `creature_template` SET `HealthModifier`=18.8 WHERE `entry`=25755;
UPDATE `creature_template` SET `HealthModifier`=3.538 WHERE `entry`=25756;
UPDATE `creature_template` SET `HealthModifier`=1.5 WHERE `entry`=25757;
UPDATE `creature_template` SET `HealthModifier`=4 WHERE `entry`=40446;
UPDATE `creature_template` SET `InhabitType`=7 WHERE `entry` IN (25964,25965,25966,26190);

DELETE FROM `spell_script_names` WHERE `ScriptName` IN
('spell_ahune_synch_health',
'spell_ice_spear_control_aura',
'spell_slippery_floor_periodic',
'spell_summon_ice_spear_delayer',
'spell_summoning_rhyme_aura',
'spell_ahune_spanky_hands',
'spell_ahune_minion_despawner',
'spell_ice_spear_target_picker',
'spell_ice_bombardment_dest_picker');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(46430, 'spell_ahune_synch_health'),
(46371, 'spell_ice_spear_control_aura'),
(46320, 'spell_slippery_floor_periodic'),
(46878, 'spell_summon_ice_spear_delayer'),
(45926, 'spell_summoning_rhyme_aura'),
(46146, 'spell_ahune_spanky_hands'),
(46843, 'spell_ahune_minion_despawner'),
(46372, 'spell_ice_spear_target_picker'),
(46398, 'spell_ice_bombardment_dest_picker');

DELETE FROM `creature_text` WHERE `entry` IN (25745,25754,25697,40446);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(25745,0,0,'The Earthen Ring\'s Assault Begins.',41,0,100,0,0,0,24930,1,'Ahune Bunny- EMOTE_EARTHEN_ASSAULT'),
(25754,0,0,'Ahune Retreats.  His defenses diminish.',41,0,100,0,0,0,24931,1,'Earthen Ring Flamecaller - EMOTE_RETREAT'),
(25754,1,0,'Ahune will soon resurface.',41,0,100,0,0,0,24932,1,'Earthen Ring Flamecaller - EMOTE_RESURFACE'),
(40446,0,0,'How DARE you! You will not stop the coming of Lord Ahune!',14,0,100,0,0,0,40437,0,'Skar\'this the Summoner'),
(25697,0,0,'The Ice Stone has melted!',14,0,100,0,0,0,24895,0,'Luma Skymother - SAY_PLAYER_1'),
(25697,1,0,'Ahune, your strength grows no more!',14,0,100,0,0,0,24893,0,'Luma Skymother - SAY_PLAYER_2'),
(25697,2,0,'Your frozen reign will not come to pass!',14,0,100,0,0,0,24894,0,'Luma Skymother - SAY_PLAYER_3');

DELETE FROM `gossip_menu` WHERE `entry`=11389;
INSERT INTO `gossip_menu` VALUES
(11389,15864);

DELETE FROM `gossip_menu_option` WHERE `menu_id`=11389;
INSERT INTO `gossip_menu_option` (`menu_id`,`id`,`option_icon`,`option_text`,`OptionBroadcastTextID`,`option_id`,`npc_option_npcflag`,`action_menu_id`,`action_poi_id`,`box_coded`,`box_money`,`box_text`,`BoxBroadcastTextID`) VALUES
(11389,1,0,'Disturb the stone and summon Lord Ahune.',40443,1,1,0,0,0,0,NULL,0);

DELETE FROM `creature_template_addon` WHERE `entry` IN (25740,25755,25865,25985,25952);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(25740, 0, 0, 9, 1, 61976),
(25755, 0, 0, 0, 0, 46542),
(25865, 0, 0, 0, 0, '46810 61976'),
(25985, 0, 0, 0, 0, '75498 46878'),
(25952, 0, 0, 0, 0, 46314);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry` IN (46603,46593,46735,45930,45941,46809,46843,46396,46398,46236);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13,1,46603,0,0,31,0,3,26121,0,0,0,0,'',"Spell 'Force Wisp Flight Missile' can  hit 'Wisp Source Bunny'"),
(13,1,46593,0,0,31,0,3,26120,0,0,0,0,'',"Spell 'Wisp Flight Missile and Beam' can hit 'Wisp Dest Bunny'"),
(13,1,46735,0,0,31,0,3,26190,0,0,0,0,'',"Spell 'Spank - Force Bunny To Knock You To' can hit '[PH] Spank Target Bunny'"),
(13,1,45930,0,1,31,0,3,25971,0,0,0,0,'',"Spell 'Ahune - Summoning Rhyme Spell, make bonfire' can hit 'Shaman Bonfire Bunny 000'"),
(13,1,45930,0,2,31,0,3,25972,0,0,0,0,'',"Spell 'Ahune - Summoning Rhyme Spell, make bonfire' can hit 'Shaman Bonfire Bunny 001'"),
(13,1,45930,0,3,31,0,3,25973,0,0,0,0,'',"Spell 'Ahune - Summoning Rhyme Spell, make bonfire' can hit 'Shaman Bonfire Bunny 002'"),
(13,1,45941,0,0,31,0,3,25746,0,0,0,0,'',"Spell 'Summon Ahune's Loot Missile' can hit '[PH] Ahune Loot Loc Bunny'"),
(13,1,46809,0,0,31,0,3,26239,0,0,0,0,'',"Spell 'Make Ahune's Ghost Burst' can hit 'Ghost of Ahune"),
(13,1,46843,0,1,31,0,3,25756,0,0,0,0,'',"Spell 'Minion Despawner' can hit 'Ahunite Coldwave'"),
(13,1,46843,0,2,31,0,3,25757,0,0,0,0,'',"Spell 'Minion Despawner' can hit 'Ahunite Frostwind'"),
(13,1,46843,0,3,31,0,3,25755,0,0,0,0,'',"Spell 'Minion Despawner' can hit 'Ahunite Hailstone'"),
(13,1,46398,0,0,31,0,3,25972,0,0,0,0,'',"Spell 'Ice Bombardment Dest Picker' can hit 'Shaman Bonfire Bunny'"),
(13,1,46396,0,0,31,0,3,25972,0,0,0,0,'',"Spell 'Ice Bombardment' can hit 'Shaman Bonfire Bunny'"),
(13,1,46236,0,1,31,0,3,25971,0,0,0,0,'',"Spell 'Close opening Visual' can hit 'Shaman Bonfire Bunny 000'"),
(13,1,46236,0,2,31,0,3,25972,0,0,0,0,'',"Spell 'Close opening Visual' can hit 'Shaman Bonfire Bunny 001'"),
(13,1,46236,0,3,31,0,3,25973,0,0,0,0,'',"Spell 'Close opening Visual' can hit 'Shaman Bonfire Bunny 002'");

DELETE FROM `disables` WHERE `sourceType`=0 AND `entry` IN (46314,46603,46593,46422);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES
(0,46314,64,0,0,'Disable LOS for spell Ahune - Slippery Floor Ambient'),
(0,46603,64,0,0,'Disable LOS for spell Force Whisp to Flight'),
(0,46593,64,0,0,'Disable LOS for spell Whisp Flight Missile and Beam'),
(0,46422,64,0,0,'Disable LOS for spell Shamans Look for Opening');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (45947,-45964,45964);
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(45947,45946,2,''),
(-45964,-46333,0,''),
(45964,46333,0,'');

-- Skar'this the Summoner
UPDATE `creature_template` SET  `AIName`='SmartAI' WHERE `entry`=40446;
DELETE FROM `smart_scripts` WHERE `entryorguid`=40446 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(40446,0,0,0,1,0,100,1,0,0,0,0,11,75427,0,0,0,0,0,1,0,0,0,0,0,0,0,'Skar\'this the Summoner - OOC - Cast \'Frost Channelling\''),
(40446,0,1,0,4,0,100,1,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Skar\'this the Summoner - On agro - say'),
(40446,0,2,0,0,0,100,0,5000,5000,15000,15000,11,55909,1,0,0,0,0,2,0,0,0,0,0,0,0,'Skar\'this the Summoner - IC - Cast Crashing Wave'),
(40446,0,3,0,0,0,100,0,10000,10000,20000,20000,11,11831,1,0,0,0,0,2,0,0,0,0,0,0,0,'Skar\'this the Summoner - IC - Cast Frost Nova'),
(40446,0,4,0,0,0,100,0,7000,7000,9000,9000,11,15043,0,0,0,0,0,2,0,0,0,0,0,0,0,'Skar\'this the Summoner - IC - Cast Frostbolt');

-- Summon Loot Bunny SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=25746;
DELETE FROM `smart_scripts` WHERE `entryorguid`=25746 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25746,0,0,0,8,0,100,0,45941,0,0,0,11,46891,0,0,0,0,0,1,0,0,0,0,0,0,0,'[PH] Ahune Loot Loc Bunny - On SpellHit - Cast \'Summon Loot\'');

-- [PH] Spank Target Bunny SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=26190;
DELETE FROM `smart_scripts` WHERE `entryorguid`=26190 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(26190,0,0,0,8,0,100,0,46735,0,0,0,11,46734,0,0,0,0,0,7,0,0,0,0,0,0,0,'[PH] Spank Target Bunny - On SpellHit \'Spank - Force Bunny To Knock You To\' - Cast \'Knock To\'');

-- Ghost of Ahune
UPDATE `creature_template` SET `AIName`='SmartAI', `flags_extra`='2' WHERE `entry`=26239;
DELETE FROM `smart_scripts` WHERE `entryorguid`=26239 AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`=2623900 AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(26239,0,0,0,25,0,100,0,0,0,0,0,3,0,11686,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - On Reset - Morph to Model 11686'),
(26239,0,1,0,8,0,100,0,46809,0,4000,4000,80,2623900,2,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - On SpellHit \'Make Ahune\'s Ghost Burst\' - Call Timed ActionList'),
(2623900,9,0,0,0,0,100,0,0,0,0,0,3,0,23707,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - Timed ActionList - Morph to Model 23707'),
(2623900,9,1,0,0,0,100,0,0,0,0,0,11,46786,0,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - Timed ActionList - Cast \'Ahune\'s Ghost Disguise\''),
(2623900,9,2,0,0,0,100,0,2400,2400,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - Timed ActionList - Set Visibility Off'),
(2623900,9,3,0,0,0,100,0,500,500,0,0,3,0,11686,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - Timed ActionList - Morph to Model 11686'),
(2623900,9,4,0,0,0,100,0,0,0,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Ghost of Ahune - Timed ActionList - Set Visibility On');

-- Wisp Source Bunny SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=26121;
DELETE FROM `smart_scripts` WHERE `entryorguid`=26121 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(26121,0,0,1,8,0,100,0,46603,0,0,0,11,46593,0,0,0,0,0,11,26120,100,0,0,0,0,0,'Wisp Source Bunny - On SpellHit \'Force Wisp Flight Missile\' - Cast \'Wisp Flight Missile and Beam\''),
(26121,0,1,0,61,0,100,0,0,0,0,0,41,9000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Wisp Source Bunny - On SpellHit \'Force Wisp Flight Missile\' - Despawn in 9s');

-- Wisp Dest Bunny SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=26120;
DELETE FROM `smart_scripts` WHERE `entryorguid`=26120 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(26120,0,0,0,8,0,100,0,46593,0,0,0,41,9000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Wisp Dest Bunny - On SpellHit \'Wisp Flight Missile and Beam\' - Despawn in 9s');

-- Shaman Beam Bunny SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN (25971,25972,25973);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (25971,25972,25973) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25971,0,0,0,8,0,100,0,45930,0,0,0,11,46339,0,0,0,0,0,1,0,0,0,0,0,0,0,'Shaman Beam Bunny 000 - On SpellHit - Cast \'Bonfire Disguise\''),
(25972,0,0,0,8,0,100,0,45930,0,0,0,11,46339,0,0,0,0,0,1,0,0,0,0,0,0,0,'Shaman Beam Bunny 001 - On SpellHit - Cast \'Bonfire Disguise\''),
(25973,0,0,0,8,0,100,0,45930,0,0,0,11,46339,0,0,0,0,0,1,0,0,0,0,0,0,0,'Shaman Beam Bunny 002 - On SpellHit - Cast \'Bonfire Disguise\'');

-- Ahunite Hailstone SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=25755;
DELETE FROM `smart_scripts` WHERE `entryorguid`=25755 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25755,0,0,0,0,0,100,0,6000,8000,6000,8000,11,2676,0,0,0,0,0,2,0,0,0,0,0,0,0,'Ahunite Hailstone - In Combat - Cast \'Pulverize\'');

-- Ahunite Coldwave SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=25756;
DELETE FROM `smart_scripts` WHERE `entryorguid`=25756 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25756,0,0,0,0,0,100,0,5000,7000,6000,8000,11,46406,0,0,0,0,0,2,0,0,0,0,0,0,0,'Ahunite Coldwave - In Combat - Cast \'Bitter Blast\'');

-- Ahunite Frostwind SAI
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=25757;
DELETE FROM `smart_scripts` WHERE `entryorguid`=25757 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(25757,0,0,0,54,0,100,0,0,0,0,0,11,12550,0,0,0,0,0,1,0,0,0,0,0,0,0,'Ahunite Frostwind - On Just Summoned - Cast \'Lightning Shield\''),
(25757,0,1,0,0,0,100,0,2000,2000,5000,7000,11,46568,0,0,0,0,0,18,120,0,0,0,0,0,0,'Ahunite Frostwind - In Combat - Cast \'Wind Buffet\'');

DELETE FROM `item_loot_template` WHERE `entry`=35512;
INSERT INTO `item_loot_template` (`Entry`,`Item`,`Reference`,`Chance`,`QuestRequired`,`LootMode`,`GroupId`,`MinCount`,`MaxCount`,`Comment`) VALUES
(35512,17202,0,100,0,1,0,2,5,NULL);

DELETE FROM `item_loot_template` WHERE `Entry`=54536;
INSERT INTO `item_loot_template` (`Entry`, `Item`, `Reference`, `Chance`, `QuestRequired`, `LootMode`, `GroupId`, `MinCount`, `MaxCount`) VALUES
(54536, 54806, 0, 3, 0, 1, 0, 1, 1),
(54536, 23247, 0, 100, 0, 1, 1, 5, 10),
(54536, 53641, 0, 3, 0, 1, 0, 1, 1);

DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID AND @CGUID+8;
INSERT INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(@CGUID,25745,547,1,1,0,0,-96.64146,-230.8864,4.780959,1.413717,300,0,0,1,1,0,0,0,0), -- [PH] Ahune Summon Loc Bunny
(@CGUID+1,25964,547,1,1,0,0,-90.00211,-224.9285,-1.378754,2.956095,300,0,0,1,1,2,0,0,0), -- Shaman Beam Bunny 000
(@CGUID+2,25965,547,1,1,0,0,-97.39627,-223.761,-1.494899,0.9130945,300,0,0,1,1,2,0,0,0), -- Shaman Beam Bunny 001
(@CGUID+3,25966,547,1,1,0,0,-103.3054,-224.0149,0.5259815,5.676991,300,0,0,1,1,2,0,0,0), -- Shaman Beam Bunny 002
(@CGUID+4,26190,547,1,1,0,0,-95.33572,-207.4834,16.28742,4.904375,300,0,0,1,1,0,0,0,0), -- [PH] Spank Target Bunny
(@CGUID+5,25952,547,1,1,0,0,-96.64146,-230.8864,4.780959,1.413717,300,0,0,1,1,0,0,0,0), -- Slippery Floor Bunny
(@CGUID+6,25952,547,1,1,0,0,-69.83901,-162.474,-2.303646,2.513274,300,0,0,1,1,0,0,0,0), -- Slippery Floor Bunny
(@CGUID+7,26239,547,1,1,0,0,-99.10214,-233.1872,-1.22297,1.466077,300,0,0,1,1,0,0,0,0), -- Ghost of Ahune
(@CGUID+8,25746,547,1,1,0,0,-96.8723,-212.8425,-1.149142,4.153883,300,0,0,1,1,0,0,0,0); -- [PH] Ahune Loot Loc Bunny

DELETE FROM `gameobject` WHERE `guid` BETWEEN @OGUID AND @OGUID+19;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `VerifiedBuild`) VALUES
(@OGUID+0, 187882, 547, 0, 0, 1, 1, -69.90455, -162.2449, -2.366563, 2.426008, 0, 0, 0.9366722, 0.3502074, 120, 255, 1,0), -- Icestone
(@OGUID+1, 188067, 547, 0, 0, 1, 1,-79.397, -219.7025, -4.042892, -2.199115, 0, 0, -0.8910065, 0.4539906, 120, 255, 1, 0), -- Ice Block
(@OGUID+2, 188067, 547, 0, 0, 1, 1, -115.5985, -162.7724, -1.924025, -0.5585039, 0, 0, -0.2756367, 0.9612619, 120, 255, 1, 0), -- Ice Block
(@OGUID+3, 188067, 547, 0, 0, 1, 1, -71.89625, -145.4974, -1.551813, -1.954766, 0, 0, -0.8290367, 0.5591941, 120, 255, 1, 0), -- Ice Block
(@OGUID+4, 188067, 547, 0, 0, 1, 1, -49.27251, -168.9859, -1.898811, 2.007128, 0, 0, 0.8433914, 0.5372996, 120, 255, 1, 0), -- Ice Block
(@OGUID+5, 188067, 547, 0, 0, 1, 1, -75.95139, -182.771, -4.882017, -1.151916, 0, 0, -0.5446386, 0.8386708, 120, 255, 1, 0), -- Ice Block
(@OGUID+6, 188067, 547, 0, 0, 1, 1, -83.52528, -172.1806, -3.816522, 0.01745246, 0, 0, 0.00872612, 0.9999619, 120, 255, 1, 0), -- Ice Block
(@OGUID+7, 188067, 547, 0, 0, 1, 1, -83.52528, -217.3293, -3.0728, -0.4886912, 0, 0, -0.2419214, 0.9702958, 120, 255, 1, 0), -- Ice Block
(@OGUID+8, 188072, 547, 0, 0, 1, 1, -71.48915, -160.7316, -4.18569, -0.4188786, 0, 0, -0.2079115, 0.9781476, 120, 255, 1, 0), -- Ice Stone Mount
(@OGUID+9, 188072, 547, 0, 0, 1, 1, -69.21773, -163.491, -2.044773, 2.967041, 0, 0, 0.9961939, 0.08716504, 120, 255, 1, 0), -- Ice Stone Mount
(@OGUID+10, 188072, 547, 0, 0, 1, 1, -71.82486, -164.475, -3.962982, -0.9250239, 0, 0, -0.4461975, 0.8949345, 120, 255, 1, 0), -- Ice Stone Mount
(@OGUID+11, 188072, 547, 0, 0, 1, 1, -69.20837, -160.345, -4.25643, 1.850049, 0, 0, 0.7986355, 0.601815, 120, 255, 1, 0), -- Ice Stone Mount
(@OGUID+12, 188073, 547, 0, 0, 1, 1, -89.75205, -113.5002, -2.709442, 0.453785, 0, 0, 0.2249508, 0.9743701, 120, 255, 1, 0), -- Ahune Bonfire
(@OGUID+13, 188073, 547, 0, 0, 1, 1,-114.9574, -117.3017, -2.71, 2.007128, 0, 0, 0.8433914, 0.5372996, 120, 255, 1, 0), -- Ahune Bonfire
(@OGUID+14, 188142, 547, 0, 0, 1, 1,  -74.65959, -243.8125, -2.735999, 2.216565, 0, 0, 0.8949337, 0.4461992, 120, 255, 1, 0), -- Ice Block, Big
(@OGUID+15, 188142, 547, 0, 0, 1, 1, -72.75314, -185.1547, -4.930593, 0.157079, 0, 0, 0.07845879, 0.9969174, 120, 255, 1, 0), -- Ice Block, Big
(@OGUID+16, 188142, 547, 0, 0, 1, 1, -103.7134, -245.5041, -1.377881, -1.291542, 0, 0, -0.6018143, 0.7986361, 120, 255, 1, 0), -- Ice Block, Big
(@OGUID+17, 188142, 547, 0, 0, 1, 1, -118.9196, -204.8023, -1.504161, 1.919862, 0, 0, 0.8191519, 0.5735767, 120, 255, 1, 0), -- Ice Block, Big
(@OGUID+18, 188142, 547, 0, 0, 1, 1, -117.3857, -165.9649, -2.018646, 0.5585039, 0, 0, 0.2756367, 0.9612619, 120, 255, 1, 0), -- Ice Block, Big
(@OGUID+19, 188142, 547, 0, 0, 1, 1, -75.42784, -221.16, -2.882941, 0.4886912, 0, 0, 0.2419214, 0.9702958, 120, 255, 1, 0); -- Ice Block, Big

DELETE FROM `creature_addon` WHERE `guid` IN (@NPC,@NPC+1,@NPC+2);
INSERT INTO `creature_addon` (`guid`,`path_id`) VALUES
(@NPC,@PATH),
(@NPC+1,@PATH+10),
(@NPC+2,@PATH+20);

DELETE FROM `waypoint_data` WHERE `id` IN (@PATH,@PATH+10,@PATH+20);
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-107.1537,-233.7247,27.1834,0,0,0,100,0),
(@PATH,2,-109.4618,-232.0907,25.12787,0,0,0,100,0),
(@PATH,3,-109.4792,-229.4328,20.98899,0,0,0,100,0),
(@PATH,4,-105.9522,-226.8887,17.26674,0,0,0,100,0),
(@PATH,5,-101.0044,-224.8914,16.04452,0,0,0,100,0),
(@PATH,6,-96.82773,-225.9608,15.73896,0,0,0,100,0),
(@PATH,7,-92.59879,-227.0505,15.54452,0,0,0,100,0),
(@PATH,8,-90.07465,-229.0938,16.58224,0,0,0,100,0),
(@PATH,9,-88.24558,-231.7715,22.47455,0,0,0,100,0),
(@PATH,10,-91.0969,-232.6422,24.65563,0,0,0,100,0),
(@PATH,11,-97.20647,-234.4709,28.46118,0,0,0,100,0),
(@PATH,12,-101.5825,-234.9054,29.35008,0,0,0,100,0),

(@PATH+10,1,-109.4618,-232.0907,25.12787,0,0,0,100,0),
(@PATH+10,2,-109.4792,-229.4328,20.98899,0,0,0,100,0),
(@PATH+10,3,-105.9522,-226.8887,17.26674,0,0,0,100,0),
(@PATH+10,4,-101.0044,-224.8914,16.04452,0,0,0,100,0),
(@PATH+10,5,-96.82773,-225.9608,15.73896,0,0,0,100,0),
(@PATH+10,6,-92.59879,-227.0505,15.54452,0,0,0,100,0),
(@PATH+10,7,-90.07465,-229.0938,16.58224,0,0,0,100,0),
(@PATH+10,8,-88.24558,-231.7715,22.47455,0,0,0,100,0),
(@PATH+10,9,-91.0969,-232.6422,24.65563,0,0,0,100,0),
(@PATH+10,10,-97.20647,-234.4709,28.46118,0,0,0,100,0),
(@PATH+10,11,-101.5825,-234.9054,29.35008,0,0,0,100,0),
(@PATH+10,12,-107.1537,-233.7247,27.1834,0,0,0,100,0),

(@PATH+20,1,-97.20647,-234.4709,28.46118,0,0,0,100,0),
(@PATH+20,2,-101.5825,-234.9054,29.35008,0,0,0,100,0),
(@PATH+20,3,-107.1537,-233.7247,27.1834,0,0,0,100,0),
(@PATH+20,4,-109.4618,-232.0907,25.12787,0,0,0,100,0),
(@PATH+20,5,-109.4792,-229.4328,20.98899,0,0,0,100,0),
(@PATH+20,6,-105.9522,-226.8887,17.26674,0,0,0,100,0),
(@PATH+20,7,-101.0044,-224.8914,16.04452,0,0,0,100,0),
(@PATH+20,8,-96.82773,-225.9608,15.73896,0,0,0,100,0),
(@PATH+20,9,-92.59879,-227.0505,15.54452,0,0,0,100,0),
(@PATH+20,10,-90.07465,-229.0938,16.58224,0,0,0,100,0),
(@PATH+20,11,-88.24558,-231.7715,22.47455,0,0,0,100,0),
(@PATH+20,12,-91.0969,-232.6422,24.65563,0,0,0,100,0);

DELETE FROM `game_event_creature` WHERE `guid` BETWEEN @CGUID AND @CGUID+8 AND `eventEntry`=1;
INSERT INTO `game_event_creature` (`eventEntry`,`guid`) VALUES
(1,@CGUID),
(1,@CGUID+1),
(1,@CGUID+2),
(1,@CGUID+3),
(1,@CGUID+4),
(1,@CGUID+5),
(1,@CGUID+6),
(1,@CGUID+7),
(1,@CGUID+8);

DELETE FROM `game_event_gameobject` WHERE `guid` BETWEEN @OGUID AND @OGUID+19 AND `eventEntry`=1;
INSERT INTO `game_event_gameobject` (`eventEntry`,`guid`) VALUES
(1,@OGUID),
(1,@OGUID+1),
(1,@OGUID+2),
(1,@OGUID+3),
(1,@OGUID+4),
(1,@OGUID+5),
(1,@OGUID+6),
(1,@OGUID+7),
(1,@OGUID+8),
(1,@OGUID+9),
(1,@OGUID+10),
(1,@OGUID+11),
(1,@OGUID+12),
(1,@OGUID+13),
(1,@OGUID+14),
(1,@OGUID+15),
(1,@OGUID+16),
(1,@OGUID+17),
(1,@OGUID+18),
(1,@OGUID+19);

UPDATE `creature_template` SET `unit_flags`=64 WHERE `entry`=25740;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=45947;
DELETE FROM `creature_template_addon` WHERE `entry`=25754;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(25754,0,0,1,1,0,NULL);

-- Midsummer Reveler - 3 pieces bonus: Infused with the spirit of Midsummer.
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=58933 AND `spell_effect`=45427;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
('58933','45427','2','Midsummer - Bonus Set');

-- Daily Heroic Random (1st)
UPDATE `quest_template` SET rewarditem2 = '38186', rewardamount2 = '2' WHERE id = '24788';

-- Daily Heroic Random (Nth)
UPDATE `quest_template` SET rewarditem2 = '38186', rewardamount2 = '1' WHERE id = '24789';

-- Ethereal Credit - Bind to Account - Disable Delete Item
UPDATE `item_template` SET quality = '7', flags = '134217760' WHERE entry = '38186';

-- Remove Item Level Requirement for Low Level Heroics
UPDATE `access_requirement` SET item_level = '0' WHERE mapid IN (574,575,576,578,595,599,600,601,602,604,608,619);

-- Spell threat entries fix
DELETE FROM `spell_threat` WHERE `entry` IN (52372,47520,47488,20243,12809,48568);
INSERT INTO `spell_threat` (`entry`, `flatMod`, `pctMod`, `apPctMod`) VALUES 
(52372, 0, 7.00, 0.0), -- Icy Touch
(47520, 225, 1.00, 0.0), -- Cleave (Rank 8)
(47488, 770, 1.30, 0.0), -- Shield Slam (Rank 8)
(20243, 315, 1.00, 0.05), -- Devastate (Rank 1)
(12809, 0, 2.00, 0.0), -- Concussion Blow
(48568, 1031, 0.50, 0.0); -- Lacerate (Rank 3)

-- Mod el spawntimesecs de brann para el reinicio del evento de HoS si muere el NPC.
UPDATE `creature` SET `spawntimesecs`= '0' WHERE `id`='28070';
