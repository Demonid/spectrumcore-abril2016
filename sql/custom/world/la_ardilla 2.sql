DELETE FROM `creature_template` WHERE entry = 60000;
INSERT INTO `creature_template` (`entry`, `modelid1`, `name`, `subname`, `minlevel`, `maxlevel`, `faction`, `npcflag`, `scale`, `rank`) VALUES
(60000,7937,'La Ardilla 2.0',NULL,80,80,35,2,4,4);


DELETE FROM `creature` WHERE id = 60000;
INSERT INTO `creature` (`id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`) VALUES
(60000,571,5942.51,633.389,650.615,3.56794),
(60000,571,5664.77,666.612,651.987,0.262649);


DELETE FROM `quest_template` WHERE id BETWEEN 60010 AND 60100;
INSERT INTO `quest_template` (`ID`, `QuestLevel`, `MinLevel`, `RewardItem1`, `RewardAmount1`, `RewardChoiceItemID1`, `RewardChoiceItemQuantity1`, `RewardChoiceItemID2`, `RewardChoiceItemQuantity2`, `RewardChoiceItemID3`, `RewardChoiceItemQuantity3`, `LogTitle`, `LogDescription`, `QuestDescription`) VALUES
(60010,80,80,0,0,60011,1,60012,1,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60020,80,80,60021,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60030,80,80,60031,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60040,80,80,60041,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60050,80,80,0,0,60051,1,60052,1,60053,1,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60060,80,80,60061,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60070,80,80,0,0,60071,1,60072,1,60073,1,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60080,80,80,60081,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60090,80,80,0,0,60091,1,60092,1,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.'),
(60100,80,80,60101,1,0,0,0,0,0,0,'Set Basico','Azeroth es un lugar peligroso, toma esto. Es mi vieja armadura de guerra, te sera de ayuda para conquistar los desafios que te esperan.','A todo viaje de un heroe le llega la hora de recibir ayuda para entrar en accion.');


DELETE FROM `quest_template_addon` WHERE id BETWEEN 60010 AND 60100;
INSERT INTO `quest_template_addon` (`ID`, `AllowableClasses`) VALUES
(60010,16),
(60020,128),
(60030,256),
(60040,8),
(60050,1024),
(60060,4),
(60070,64),
(60080,1),
(60090,2),
(60100,32);


DELETE FROM `creature_queststarter` WHERE id = 60000;
INSERT INTO `creature_queststarter` (`id`, `quest`) VALUES
(60000,60010),
(60000,60020),
(60000,60030),
(60000,60040),
(60000,60050),
(60000,60060),
(60000,60070),
(60000,60080),
(60000,60090),
(60000,60100);


DELETE FROM `creature_questender` WHERE id = 60000;
INSERT INTO `creature_questender` (`id`, `quest`) VALUES
(60000,60010),
(60000,60020),
(60000,60030),
(60000,60040),
(60000,60050),
(60000,60060),
(60000,60070),
(60000,60080),
(60000,60090),
(60000,60100);


DELETE FROM `item_template` WHERE entry BETWEEN 60011 AND 60102;
INSERT INTO `item_template` (`entry`, `name`, `displayid`, `Quality`, `Flags`, `ItemLevel`, `bonding`) VALUES
(60011,'Sagrado',39459,4,4,78,1),
(60012,'Sombra',39459,4,4,78,1),
(60013,'Armas y Trinkets Sagrado',39459,4,4,78,1),
(60014,'Armas y Trinkets Sombra',39459,4,4,78,1),
(60021,'Mago',39459,3,4,78,1),
(60022,'Armas y Trinkets Mago',39459,4,4,78,1),
(60031,'Brujo',39459,4,4,78,1),
(60032,'Armas y Trinkets Brujo',39459,4,4,78,1),
(60041,'Picaro',39459,4,4,78,1),
(60042,'Armas y Trinkets Picaro',39459,4,4,78,1),
(60051,'Equilibrio',39459,4,4,78,1),
(60052,'Feral',39459,4,4,78,1),
(60053,'Restauracion',39459,4,4,78,1),
(60054,'Armas y Trinkets Equilibrio',39459,4,4,78,1),
(60055,'Armas y Trinkets Feral',39459,4,4,78,1),
(60056,'Armas y Trinkets Restauracion',39459,4,4,78,1),
(60061,'Cazador',39459,4,4,78,1),
(60062,'Armas y Trinkets Cazador',39459,4,4,78,1),
(60071,'Elemental',39459,4,4,78,1),
(60072,'Mejora',39459,4,4,78,1),
(60073,'Restauracion',39459,4,4,78,1),
(60074,'Armas y Trinkets Elemental',39459,4,4,78,1),
(60075,'Armas y Trinkets Mejora',39459,4,4,78,1),
(60076,'Armas y Trinkets Restauracion',39459,4,4,78,1),
(60081,'Guerrero',39459,4,4,78,1),
(60082,'Armas y Trinkets Guerrero',39459,4,4,78,1),
(60091,'Sagrado',39459,4,4,78,1),
(60092,'Reprension y Proteccion',39459,4,4,78,1),
(60093,'Armas y Trinkets Sagrado',39459,4,4,78,1),
(60094,'Armas y Trinkets Reprension y Proteccion',39459,4,4,78,1),
(60101,'Caballero de la Muerte',39459,4,4,78,1),
(60102,'Armas y Trinkets Caballero de la Muerte',39459,4,4,78,1);


DELETE FROM `item_loot_template` WHERE entry BETWEEN 60011 AND 60102;
INSERT INTO `item_loot_template` (`Entry`, `Item`) VALUES
-- Sacerdote Sagrado
(60011,41854), -- Cabeza
(60011,41869), -- Hombros
(60011,41859), -- Pecho
(60011,41874), -- Guantes
(60011,41864), -- Pantalones
(60011,41893), -- Brazal
(60011,41881), -- Cinturon
(60011,41885), -- Botas
(60011,42039), -- Cuello
(60011,42073), -- Capa
(60011,42114), -- Anillo Mortal
(60011,42116), -- Anillo Furioso
(60011,60013), -- Bolsa 2
-- Bolsa 2 Sacerdote Sagrado
(60013,42126), -- Trinket PvP Horda
(60013,42124), -- Trinket PvP Alianza
(60013,42132), -- Trinket 2
(60013,42385), -- Arma 1
(60013,42514), -- Arma 2

-- Sacerdote Sombra
(60012,41915), -- Cabeza
(60012,41934), -- Hombros
(60012,41921), -- Pecho
(60012,41940), -- Guantes
(60012,41927), -- Pantalones
(60012,41893), -- Brazal
(60012,41881), -- Cinturon
(60012,41885), -- Botas
(60012,42037), -- Cuello
(60012,42071), -- Capa
(60012,42114), -- Anillo Mortal
(60012,42116), -- Anillo Furioso
(60012,60014), -- Bolsa 2
-- Bolsa 2 Sacerdote Sombra
(60014,42126), -- Trinket PvP Horda
(60014,42124), -- Trinket PvP Alianza
(60014,42132), -- Trinket 2
(60014,42364), -- Arma 1
(60014,42503), -- Arma 2

-- Mago
(60021,41946), -- Cabeza
(60021,41965), -- Hombros
(60021,41953), -- Pecho
(60021,41971), -- Guantes
(60021,41959), -- Pantalones
(60021,41909), -- Brazal
(60021,41898), -- Cinturon
(60021,41903), -- Botas
(60021,42037), -- Cuello
(60021,42071), -- Capa
(60021,42116), -- Anillo Mortal
(60021,42114), -- Anillo Furioso
(60021,60022), -- Bolsa 2
-- Bolsa 2 Mago
(60022,42126), -- Trinket PvP Horda
(60022,42124), -- Trinket PvP Alianza
(60022,42132), -- Trinket 2
(60022,42364), -- Arma 1
(60022,42520), -- Arma 2

-- Brujo
(60031,41993), -- Cabeza
(60031,42011), -- Hombros
(60031,41998), -- Pecho
(60031,42017), -- Guantes
(60031,42005), -- Pantalones
(60031,41909), -- Brazal
(60031,41898), -- Cinturon
(60031,41903), -- Botas
(60031,42037), -- Cuello
(60031,42071), -- Capa
(60031,42116), -- Anillo Mortal
(60031,42114), -- Anillo Furioso
(60031,60032), -- Bolsa 2
-- Bolsa 2 Brujo
(60032,42126), -- Trinket PvP Horda
(60032,42124), -- Trinket PvP Alianza
(60032,42132), -- Trinket 2
(60032,42364), -- Arma 1
(60032,42503), -- Arma 2

-- Picaro
(60041,41672), -- Cabeza
(60041,41683), -- Hombros
(60041,41650), -- Pecho
(60041,41767), -- Guantes
(60041,41655), -- Pantalones
(60041,41840), -- Brazal
(60041,41832), -- Cinturon
(60041,41836), -- Botas
(60041,42035), -- Cuello
(60041,42075), -- Capa
(60041,42115), -- Anillo Mortal
(60041,42117), -- Anillo Furioso
(60041,60042), -- Bolsa 2
-- Bolsa 2 Picaro
(60042,42126), -- Trinket PvP Horda
(60042,42124), -- Trinket PvP Alianza
(60042,42129), -- Trinket 2
(60042,42243), -- Arma 1
(60042,42249), -- Arma 2
(60042,42451), -- Arma 3

-- Druida Equilibrio
(60051,41327), -- Cabeza
(60051,41281), -- Hombros
(60051,41316), -- Pecho
(60051,41293), -- Guantes
(60051,41304), -- Pantalones
(60051,41625), -- Brazal
(60051,41617), -- Cinturon
(60051,41621), -- Botas
(60051,42037), -- Cuello
(60051,42071), -- Capa
(60051,42114), -- Anillo Mortal
(60051,42116), -- Anillo Furioso
(60051,60054), -- Bolsa 2
-- Bolsa 2 Druida Equilibrio
(60054,42126), -- Trinket PvP Horda
(60054,42124), -- Trinket PvP Alianza
(60054,42132), -- Trinket 2
(60054,42364), -- Arma 1
(60054,42584), -- Arma 2

-- Druida Feral
(60052,41678), -- Cabeza
(60052,41715), -- Hombros
(60052,41661), -- Pecho
(60052,41773), -- Guantes
(60052,41667), -- Pantalones
(60052,41840), -- Brazal
(60052,41832), -- Cinturon
(60052,41836), -- Botas
(60052,42035), -- Cuello
(60052,42075), -- Capa
(60052,42115), -- Anillo Mortal
(60052,42117), -- Anillo Furioso
(60052,60055), -- Bolsa 2
-- Bolsa 2 Druida Feral
(60055,42126), -- Trinket PvP Horda
(60055,42124), -- Trinket PvP Alianza
(60055,42129), -- Trinket 2
(60055,42391), -- Arma 1
(60055,42589), -- Arma 2


-- Druida Restauracion
(60053,41321), -- Cabeza
(60053,41275), -- Hombros
(60053,41310), -- Pecho
(60053,41287), -- Guantes
(60053,41298), -- Pantalones
(60053,41625), -- Brazal
(60053,41617), -- Cinturon
(60053,41621), -- Botas
(60053,42039), -- Cuello
(60053,42073), -- Capa
(60053,42114), -- Anillo Mortal
(60053,42116), -- Anillo Furioso
(60053,60056), -- Bolsa 2
-- Bolsa 2 Druida Restauracion
(60056,42126), -- Trinket PvP Horda
(60056,42124), -- Trinket PvP Alianza
(60056,42132), -- Trinket 2
(60056,42385), -- Arma 1
(60056,42579), -- Arma 2

-- Cazador
(60061,41157), -- Cabeza
(60061,41217), -- Hombros
(60061,41087), -- Pecho
(60061,41143), -- Guantes
(60061,41205), -- Pantalones
(60061,41225), -- Brazal
(60061,41235), -- Cinturon
(60061,41230), -- Botas
(60061,42035), -- Cuello
(60061,42075), -- Capa
(60061,42115), -- Anillo Mortal
(60061,42117), -- Anillo Furioso
(60061,60062), -- Bolsa 2
-- Bolsa 2 Cazador
(60062,42126), -- Trinket PvP Horda
(60062,42124), -- Trinket PvP Alianza
(60062,42129), -- Trinket 2
(60062,42209), -- Arma 1
(60062,42233), -- Arma 2
(60062,42491), -- Arma 3

-- Shaman Elemental
(60071,41019), -- Cabeza
(60071,41044), -- Hombros
(60071,40993), -- Pecho
(60071,41007), -- Guantes
(60071,41033), -- Pantalones
(60071,41065), -- Brazal
(60071,41070), -- Cinturon
(60071,41075), -- Botas
(60071,42037), -- Cuello
(60071,42071), -- Capa
(60071,42114), -- Anillo Mortal
(60071,42116), -- Anillo Furioso
(60071,60074), -- Bolsa 2
-- Bolsa 2 Shaman Elemental
(60074,42126), -- Trinket PvP Horda
(60074,42124), -- Trinket PvP Alianza
(60074,42132), -- Trinket 2
(60074,42347), -- Arma 1
(60074,42565), -- Arma 2
(60074,42603), -- Arma 3

-- Shaman Mejora
(60072,41151), -- Cabeza
(60072,41211), -- Hombros
(60072,41081), -- Pecho
(60072,41137), -- Guantes
(60072,41199), -- Pantalones
(60072,41225), -- Brazal
(60072,41235), -- Cinturon
(60072,41230), -- Botas
(60072,42035), -- Cuello
(60072,42075), -- Capa
(60072,42115), -- Anillo Mortal
(60072,42117), -- Anillo Furioso
(60072,60075), -- Bolsa 2
-- Bolsa 2 Shaman Mejora
(60075,42126), -- Trinket PvP Horda
(60075,42124), -- Trinket PvP Alianza
(60075,42129), -- Trinket 2
(60075,42209), -- Arma 1
(60075,42233), -- Arma 2
(60075,42608), -- Arma 3

-- Shaman Restauracion
(60073,41013), -- Cabeza
(60073,41038), -- Hombros
(60073,40992), -- Pecho
(60073,41001), -- Guantes
(60073,41027), -- Pantalones
(60073,41060), -- Brazal
(60073,41051), -- Cinturon
(60073,41055), -- Botas
(60073,42040), -- Cuello
(60073,42072), -- Capa
(60073,42114), -- Anillo Mortal
(60073,42116), -- Anillo Furioso
(60073,60076), -- Bolsa 2
-- Bolsa 2 Shaman Restauracion
(60076,42126), -- Trinket PvP Horda
(60076,42124), -- Trinket PvP Alianza
(60076,42132), -- Trinket 2
(60076,42353), -- Arma 1
(60076,42571), -- Arma 2
(60076,42598), -- Arma 3

-- Guerrero
(60081,40826), -- Cabeza
(60081,40866), -- Hombros
(60081,40789), -- Pecho
(60081,40807), -- Guantes
(60081,40847), -- Pantalones
(60081,40889), -- Brazal
(60081,40881), -- Cinturon
(60081,40882), -- Botas
(60081,42035), -- Cuello
(60081,42075), -- Capa
(60081,42115), -- Anillo Mortal
(60081,42117), -- Anillo Furioso
(60081,60082), -- Bolsa 2
-- Bolsa 2 Guerrero
(60082,42126), -- Trinket PvP Horda
(60082,42124), -- Trinket PvP Alianza
(60082,42129), -- Trinket 2
(60082,42318), -- Arma 1
(60082,42333), -- Arma 2                       
(60082,42209), -- Arma 3
(60082,42560), -- Arma 4
(60082,42491), -- Arma 5

-- Paladin Sagrado
(60091,40933), -- Cabeza
(60091,40963), -- Hombros
(60091,40907), -- Pecho
(60091,40927), -- Guantes
(60091,40939), -- Pantalones
(60091,40983), -- Brazal
(60091,40976), -- Cinturon
(60091,40977), -- Botas
(60091,42040), -- Cuello
(60091,42072), -- Capa
(60091,42114), -- Anillo Mortal
(60091,42116), -- Anillo Furioso
(60091,60093), -- Bolsa 2
-- Bolsa 2 Paladin Sagrado
(60093,42126), -- Trinket PvP Horda
(60093,42124), -- Trinket PvP Alianza
(60093,42132), -- Trinket 2
(60093,42353), -- Arma 1
(60093,42571), -- Arma 2
(60093,42615), -- Arma 3

-- Paladin Reprension
(60092,40828), -- Cabeza
(60092,40869), -- Hombros
(60092,40788), -- Pecho
(60092,40808), -- Guantes
(60092,40849), -- Pantalones
(60092,40889), -- Brazal
(60092,40881), -- Cinturon
(60092,40882), -- Botas
(60092,42035), -- Cuello
(60092,42075), -- Capa
(60092,42115), -- Anillo Mortal
(60092,42117), -- Anillo Furioso
(60092,60094), -- Bolsa 2
-- Bolsa 2 Paladin Reprension
(60094,42126), -- Trinket PvP Horda
(60094,42124), -- Trinket PvP Alianza
(60094,42129), -- Trinket 2
(60094,42323), -- Arma 1
(60094,42853), -- Arma 2
(60094,42276), -- Arma 3
(60094,42560), -- Arma 4

-- Caballero de la Muerte
(60101,40827), -- Cabeza
(60101,40868), -- Hombros
(60101,40787), -- Pecho
(60101,40809), -- Guantes
(60101,40848), -- Pantalones
(60101,40889), -- Brazal
(60101,40881), -- Cinturon
(60101,40882), -- Botas
(60101,42035), -- Cuello
(60101,42075), -- Capa
(60101,42115), -- Anillo Mortal
(60101,42117), -- Anillo Furioso
(60101,60102), -- Bolsa 2
-- Bolsa 2 Caballero de la Muerte
(60102,42126), -- Trinket PvP Horda
(60102,42124), -- Trinket PvP Alianza
(60102,42129), -- Trinket 2
(60102,42333), -- Arma 1
(60102,42621), -- Arma 2
(60102,42286), -- Arma 3
(60102,42291); -- Arma 3
