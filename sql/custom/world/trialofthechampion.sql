-- ----------------------
--  TRIAL OF CHAMPION (ToC 5)
-- ----------------------

-- Vehicleid para Warhorse, el 529 no sirve
UPDATE `creature_template` SET `VehicleId`= 486 WHERE `entry`= 35644;
-- Spells de monturas
UPDATE `creature_template` SET `spell1` = 68505, `spell2` = 62575, `spell3` = 68282, `spell4` = 66482 WHERE `entry` IN (35644, 36558);
-- Faccion de monturas
UPDATE `creature_template` SET `faction` = 84 WHERE `entry` = 35644;
UPDATE `creature_template` SET `faction` = 83 WHERE `entry` = 36558;

-- ----------------------
--     MONTURAS NPCS
-- ----------------------
-- Monturas del evento Grand Faction Champions
UPDATE `creature_template` SET `type_flags` = 2048, `ScriptName` = 'generic_vehicleAI_toc5' WHERE `entry` IN (35328, 35329, 35331, 35332, 35330, 35314, 35325, 35327, 35323, 35326);
UPDATE `creature_template` SET `type_flags` = 2048,`mechanic_immune_mask` = 617299835 WHERE `entry` IN (35572, 35569, 35571, 35570, 35617, 34705, 34702, 34701, 34657, 34703);

-- ----------------------
--   EL CABALLERO NEGRO
-- ----------------------
-- Flags y configuracion de la montura
UPDATE `creature_template` SET `unit_flags` = 64 WHERE `entry` IN (35451, 35490);
DELETE FROM `vehicle_template_accessory` WHERE `entry` = 35491;
INSERT INTO `vehicle_template_accessory` (`entry`, `accessory_entry`, `seat_id`, `minion`, `description`, `summontype`, `summontimer`) VALUES
(35491,35451,0,0,'Trial of the Champion - Black Knight on his gryphon',6,30000);

-- -----------------------
-- MONTURA CABALLERO NEGRO
-- -----------------------

-- Agregando el spawn del Black Knight Gryphon
DELETE FROM `creature` WHERE `id` = 35491;
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
(35491,650,3,1,0,0,799.806,639.236,475.88,2.18738,300,0,0,189000,0,0,0,0,0);

-- Black Knight va en montura real
DELETE FROM `creature_addon` WHERE `guid` = '35491'; 

-- Black Knight Gryphon, faccion, unitflags y vehicleid basicamente
UPDATE `creature_template` SET
`faction`=35,
`unit_flags`=33554432,
`VehicleId`=486 WHERE `entry`=35491;

-- Asignacion de nombre de script
UPDATE `creature_template` SET `flags_extra` = 2, `ScriptName` = 'npc_black_knight_skeletal_gryphon' WHERE `entry` = 35491;-- S

-- Monta con spell
DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` = 35491;
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
(35491,46598,0,0);

-- Especificando que vuela y que tiene waypoints para efecto de vuelo
UPDATE `creature_template` SET `inhabittype` = 4 WHERE `entry` = 35491;
-- UPDATE `creature` SET `movementtype` =  WHERE `id` = 35491;

-- Waypoints (esto hay que retocar por si no queda bien)
DELETE FROM `script_waypoint` WHERE `entry` = 35491;
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES
(35491,1,781.513,657.99,466.821,0,''),
(35491,2,759.005,665.142,462.541,0,''),
(35491,3,732.937,657.164,452.678,0,''),
(35491,4,717.491,646.009,440.137,0,''),
(35491,5,707.57,628.978,431.129,0,''),
(35491,6,705.164,603.628,422.957,0,''),
(35491,7,716.351,588.49,420.802,0,''),
(35491,8,741.703,580.168,420.523,0,''),
(35491,9,761.634,586.383,422.206,0,''),
(35491,10,775.983,601.992,423.606,0,''),
(35491,11,769.051,624.686,420.035,0,''),
(35491,12,756.582,631.692,412.53,0,''),
(35491,13,744.841,634.505,411.575,2000,''),
(35491,14,759.005,665.142,462.541,0,''),
(35491,15,747.127,813.51,460.707,0,'');


-- ----------------------
--   SOLDADOS ARGENTA
-- ----------------------
-- Configuraciones
-- -- Argent Lightwielder
UPDATE `creature_template` SET `modelid3` = 0 WHERE `entry` = 35309;
UPDATE `creature_template` SET `minlevel` = 80, `maxlevel` = 80, `mechanic_immune_mask` = 650854267 WHERE `entry` = 35310;
-- -- Argent Monk
UPDATE `creature_template` SET `modelid3` = 0 WHERE `entry` = 35305;
UPDATE `creature_template` SET `minlevel` = 80, `maxlevel` = 80, `mechanic_immune_mask` = 650854267 WHERE `entry` = 35306;
-- -- Argent Priestess
UPDATE `creature_template` SET `modelid3` = 0 WHERE `entry` = 35307;
UPDATE `creature_template` SET `minlevel` = 80, `maxlevel` = 80, `mechanic_immune_mask` = 650854267 WHERE `entry` = 35308;
-- Script Names de los Soldados Argenta
UPDATE creature_template SET ScriptName = 'npc_argent_monk' WHERE entry = 35305;
UPDATE creature_template SET ScriptName = 'npc_argent_lightwielder' WHERE entry = 35309;
UPDATE creature_template SET ScriptName = 'npc_argent_priest' WHERE entry = 35307;
-- Inmunidades de los Soldados Argenta
UPDATE `creature_template` SET `mechanic_immune_mask` = '650854267' WHERE `entry` IN (35305, 35309, 35307, 12488, 12439, 12448);
-- Ajustes para los Soldados Argenta HERO
UPDATE `creature_template` SET `HealthModifier` = 29.4806 WHERE `entry`IN  (35306, 35308, 35310);
UPDATE `creature_template` SET `HealthModifier` = 51.8533 WHERE `entry` = 35490;
UPDATE `creature_template` SET `HealthModifier` = 16.4286 WHERE `entry` = 35451;

-- Addons para los Soldados Argenta y varias auras
DELETE FROM `creature_template_addon` WHERE `entry` IN (35305, 35306, 35307, 35308, 35309, 35310);
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
(35305, 0, 0, 0, 1, 27, '63501'),
(35306, 0, 0, 0, 1, 27, '63501'),
(35307, 0, 0, 0, 1, 375, '63501'),
(35308, 0, 0, 0, 1, 375, '63501'),
(35309, 0, 0, 0, 1, 375, '63501'),
(35310, 0, 0, 0, 1, 375, '63501');
DELETE FROM `creature_template_addon` WHERE (`entry` IN (35614, 35311));
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
(35614, 0, 0, 0, 0, 0, 67782), -- Desecration Stalker (Periodic Desecration)
(35311, 0, 0, 0, 0, 0, 67196); -- Fountain of Light (Periodic Light rain)
UPDATE `creature_template` SET `flags_extra` = 130 WHERE `entry` = 35614; -- Set Desecration Stalker as invisible passive trigger
UPDATE `creature_template` SET `AIName` = 'SmartAI' WHERE `entry` = 35311; -- Set Light Fountain as passive

DELETE FROM `smart_scripts` WHERE (`entryorguid`=35311 AND `source_type`=0);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(35311, 0, 0, 0, 25, 0, 100, 7, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Fountain of Light - Set passive');

-- ----------------------
--       MEMORIAS
-- ----------------------
-- Hero
UPDATE `creature_template` SET `mechanic_immune_mask` = 617297499 WHERE `name` LIKE 'Memory of%(1)';
-- Normal
UPDATE `creature_template` SET `faction`=16, `rank`=1, `minlevel`=82, `maxlevel`=82, `exp`=2 WHERE `name` LIKE 'Memory of %';
-- ----------------------
--    RESURRECCIONES
-- ----------------------
-- Aqui estan Arelas y Jaeren en el caballero negro
-- UPDATE `creature_template` SET `mindmg` = 420, `maxdmg` = 630, `attackpower` = 158 WHERE `entry` = 35717;
UPDATE `creature_template` SET `difficulty_entry_1` = 35546 WHERE `entry` = 35545;
UPDATE `creature_template` SET `difficulty_entry_1` = 35568 WHERE `entry` = 35564;
UPDATE `creature_template` SET `minlevel` = 80, `maxlevel` = 80 WHERE `entry` IN (35568, 35546);
UPDATE `creature_template` SET `minlevel` = 80, `maxlevel` = 80, `faction` = 14 WHERE `entry` IN (35564, 35568, 35545, 35546, 35717, 35590);


-- ----------------------
--     INMUNIDADES
-- ----------------------
-- Los eadric, la chica y el caballero negro
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299835 WHERE `entry` IN (34928,35517,35119,35518,35451,35490);

-- ----------------------
--       SPELLS
-- ----------------------
DELETE FROM `spell_script_names` WHERE spell_id IN (66515, 66867, 67534, 67830, 66482);
INSERT INTO `spell_script_names` VALUES
(66867, 'spell_eadric_hammer_of_righteous'), -- Hammer can be picked by target if isn't under HoJ effect
(66515, 'spell_paletress_shield'),           -- Reflect 25% of abosorbed damage
(67534, 'spell_toc5_hex_mending'),           -- Hex of mending spell effect
(67830, 'spell_toc5_ride_mount'),            -- Allow ride only if a Lance is equiped
(66482, 'spell_toc5_defend');                -- Add visual shields depending on stack size

-- Trigger en eadric
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` = 66905;
INSERT INTO `spell_linked_spell` VALUES
(66905, -66904, 0, 'Eadric Hammer of Righteous');  -- Remove the Hammer of Righteous spell after using it

-- Condiciones en eventos en medio de la pelea
DELETE FROM `conditions` WHERE SourceTypeOrReferenceId = 13 AND SourceEntry IN (66905, 67705, 67715, 66798);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13, 1, 66905, 0, 31, 3, 35119, 0, 0, '', 'Eadric Hammer of Rigtheous Interrupt'), -- Target Eadric with Hammer of Righteous
(13, 2, 66905, 0, 31, 3, 35119, 0, 0, '', 'Eadric Hammer of Rigtheous Interrupt'),
(13, 4, 66905, 0, 31, 3, 35119, 0, 0, '', 'Eadric Hammer of Rigtheous aura'),
(13, 1, 66798, 0, 31, 3, 35005, 0, 0, '', 'The Black Kinght - Deaths Respite'),
(13, 1, 66798, 0, 31, 3, 35004, 0, 0, '', 'The Black Kinght - Deaths Respite'),
(13, 1, 67705, 0, 31, 3, 35005, 0, 0, '', 'The Black Kinght - Raise Arelas Brightstar'),
(13, 1, 67715, 0, 31, 3, 35004, 0, 0, '', 'The Black Kinght - Raise Arelas Brightstar');


-- ----------------------
--       TEXTOS
-- ----------------------
-- Aqui hubo que arreglar las tildes y demas
SET @TIRION       := 34996;
SET @THRALL       := 34994;
SET @GARROSH      := 34995;
SET @VARIAN       := 34990;
SET @JAINA        := 34992;

SET @EADRIC       := 35119;
SET @PALETRESS    := 34928;
SET @BLACK_KNIGHT := 35451;

SET @SCRIPT_TEXTS_ID := -1999900;

-- INTROS
DELETE FROM creature_text WHERE entry IN (35004, 35005, @THRALL, @JAINA, @EADRIC, @PALETRESS, @BLACK_KNIGHT);
DELETE FROM `creature_text` WHERE `entry` IN (@GARROSH,@VARIAN) AND `groupid`>6;
DELETE FROM `creature_text` WHERE `entry`=@TIRION AND `groupid`>16;
INSERT INTO creature_text (entry, groupid, id, TEXT, TYPE, LANGUAGE, probability, emote, duration, sound, COMMENT) VALUES
-- -- Presentacion
(35004,0,0,'Los Atracasol estan orgullosos de presentar a sus representantes en estas pruebas de combate.',14,0,100,1,0,0,'Announcer - Player Horde Champions - Intro'),
(35005,0,0,'El Pacto de Plata esta encantado de presentar a sus luchadores para este evento, Alto Senor.',14,0,100,1,0,0,'Announcer - Player Alliance Champions - Intro'),
(@TIRION,17,0,'Bienvenidos campeones. Hoy, ante los ojos de vuestros lideres y companeros os probareis como combatientes dignos.',14,0,100,1,0,0,'Tirion - Intro'),
-- -- Horda
(@THRALL,0,0,'Luchad con fuerza, Horda! Lok\'tar Ogar!',14,0,100,1,0,0,'Thrall - Intro'),
(@GARROSH,7,0,'Por fin! Un combate digno de ser contemplado.',12,0,100,1,0,0,'Garrosh - Intro'),
(@VARIAN,7,0,'No he venido hasta aqui para ver animales despezandose entre ellos sin ningun criterio, Tirion.',12,0,100,1,0,0,'Varian - Intro'),
(@JAINA,0,0,'Son combatientes dignos, ya lo veras.',12,0,100,1,0,0,'Jaina - Intro'),
-- -- Ali
(@VARIAN,8,0,'No le veo la gracia a estos juegos, Tirion. Aun asi... Confio en que lo haran de una forma admirable.',12,0,100,1,0,0,'Thrall - Intro'),
(@JAINA,1,0,'Por supuesto que lo haran.',12,0,100,1,0,0,'Garrosh - Intro'),
(@GARROSH,8,0,'Admirable? Ja! Voy a disfrutar viendo a tus pequenos campeones fracasar, humano.',14,0,100,1,0,0,'Varian - Intro'),
(@THRALL,1,0,'Garrosh, es suficiente.',12,0,100,1,0,0,'Jaina - Intro'),
(@TIRION,18,0,'Primero os enfrentareis a tres de los Grandes Campeones del Torneo. Estos feroces contendientes han derrotado a todos los demas hasta alcanzar la maxima habilidad en la justa.',14,0,100,1,0,0,'Tirion - Intro'),

-- CAMPEONES
-- -- Horda
(35004,1,0,'Aqui llega el pequeno pero mortal Ambrose Chisparrayo, Gran Campeon de Gnomeregan.',14,0,100,1,0,0,'Announcer - Alliance Champions - Intro Mage'),
(35004,2,0,'Colosos, el enorme Gran Campeon de El Exodar, esta saliendo por la puerta.',14,0,100,1,0,0,'Announcer - Alliance Champions - Intro Shaman'),
(35004,3,0,'Esta entrando en la arena la Gran Campeona de Darnassus, la habil centinela Jaelyne Unicanto.',14,0,100,1,0,0,'Announcer - Alliance Champions - Intro Hunter'),
(35004,4,0,'Fuerte y orgulloso, aclamad al mariscal Jacob Alerius, el Gran Campeon de Ventormenta!',14,0,100,1,0,0,'Announcer - Alliance Champions - Intro Warrior'),
(35004,5,0,'Hoy el poder de los enanos esta representado por la Gran Campeona de Forjaz, Lana Martillotenaz.',14,0,100,1,0,0,'Announcer - Alliance Champions - Intro Rouge'),
-- -- Ali
(35005,1,0,'Saliendo por la puerta Eressea Cantoalba, habil maga y Gran Campeona de Lunargenta!',14,0,100,1,0,0,'Announcer - Horde Champions - Intro Mage'),
(35005,2,0,'En lo alto de su kodo, aqui esta el venerable Runok Ferocrin, Gran Campeon de Cima del Trueno!',14,0,100,1,0,0,'Announcer - Horde Champions - Intro Shaman'),
(35005,3,0,'Entrando en la arena el enjuto pero peligroso Zul\'tore, Campeon de los Sen\'jin!',14,0,100,1,0,0,'Announcer - Horde Champions - Intro Hunter'),
(35005,4,0,'Presentamos al Gran Campeon de Orgrimmar, Mokra el Trituracraneos!',14,0,100,1,0,0,'Announcer - Horde Champions - Intro Warrior'),
(35005,5,0,'Representando la tenacidad de los Renegados, aqui esta el Gran Campeon de Entranas, Mortacechador Visceri!',14,0,100,1,0,0,'Announcer - Horde Champions - Intro Rouge'),

-- ARGENTAS
(@TIRION,19,0,'Buen combate! Vuestro proximo reto viene de los mismisimos cargos de la Cruzada. Sereis puestos a prueba contra sus considerables habilidades.',14,0,100,1,0,0,'Tirion - Intro'),
-- -- Eadric
(35004,11,0,'Entrando en la arena, tenemos a un paladin que no es un extrano para los campos de batalla, ni los Campos del Torneo. El gran campeon de la Cruzada Argenta, Eadric el Puro!',14,0,100,1,0,0,'Announcer - Eadric - Intro'),
(35005,11,0,'Entrando en la arena, tenemos a un paladin que no es un extrano para los campos de batalla, ni los Campos del Torneo. El gran campeon de la Cruzada Argenta, Eadric el Puro!',14,0,100,1,0,0,'Announcer - Eadric - Intro'),
(@EADRIC,10,0,'Aceptais el reto? No hay vuelta atras!',12,0,100,1,0,16134,'Eadric - Intro'),
-- -- Paletress
(35004,12,0,'La siguiente combatiente no tiene rival alguno en su pasion al apoyar a la Luz. Les entrego a la confesora Argenta Cabelloclaro!',14,0,100,1,0,0,'Announcer - Paletress - Intro'),
(35005,12,0,'La siguiente combatiente no tiene rival alguno en su pasion al apoyar a la Luz. Les entrego a la confesora Argenta Cabelloclaro!',14,0,100,1,0,0,'Announcer - Paletress - Intro'),
(@PALETRESS,10,0,'Gracias buen heraldo. Tus palabras son muy amables.',12,0,100,1,0,16245,'Paletress - Intro'),
(@PALETRESS,11,0,'Que la luz me de fuerzas para ser un reto digno.',12,0,100,1,0,16246,'Paletress - Intro'),
-- -- Tirion
(@TIRION,20,0,'Podeis comenzar!',14,0,100,1,0,0,'Tirion - Intro'),

-- CABALLERO NEGRO
(@TIRION,21,0,'Bien hecho. Habeis probado vuestra valia hoy...',14,0,100,1,0,0,'Tirion - Intro'),
(35004,13,0,'Que es eso que hay cerca de las vigas?',12,0,100,1,0,0,'Announcer - Black Knight - Intro'),
(35005,13,0,'Que es eso que hay cerca de las vigas?',12,0,100,1,0,0,'Announcer - Black Knight - Intro'),
(@BLACK_KNIGHT,10,0,'Has estropeado mi gran entrada, rata.',12,0,100,1,0,16256,'Black Knight - Intro'),
(@TIRION,22,0,'Que significa esto?',14,0,100,1,0,0,'Tirion - Intro'),
(@BLACK_KNIGHT,11,0,'Realmente pensabas que derrotarias a un agente del Rey Exanime en los campos de tu patetico torneo?',12,0,100,1,0,16257,'Black Knight - Intro'),
(@BLACK_KNIGHT,12,0,'He venido a terminar mi cometido.',12,0,100,1,0,16258,'Black Knight - Intro'),
(@GARROSH,9,0,'Hacedlo picadillo!',14,0,100,1,0,0,'Garrosh - Black Knight - Intro'),
(@VARIAN,9,0,'No os quedeis ahi mirando; matadlo!',14,0,100,1,0,0,'Varian - Black Knight - Intro'),

-- TEXTOS EN COMBATES
-- -- Eadric
(@EADRIC,1,0,'Preparaos!',14,0,100,0,0,16135,'Eadric - Combat - Aggro'),
(@EADRIC,2,0,'Martillo del honrado!',14,0,100,0,0,16136,'Eadric - Combat - Hammer'),
(@EADRIC,3,0,'Tu! Tienes que practicar mas!',14,0,100,0,0,16137,'Eadric - Combat - Slay 1'),
(@EADRIC,3,1,'No! No! Y otra vez no! No es suficiente!',14,0,100,0,0,16138,'Eadric - Combat - Slay 2'),
(@EADRIC,4,0,'Me rindo! Lo admito. Un trabajo excelente. Puedo escaparme ya?',14,0,100,0,0,16139,'Eadric - Combat - Death'),
(@EADRIC,5,0,'%s comienza a irradiar luz. Cubrios los ojos!',41,0,100,0,0,0,'Eadric - Combat - Warning - Radiance'),
(@EADRIC,6,0,'%s comienza a lanzar Martillo del Honrado sobre $N',41,0,100,0,0,0,'Eadric - Combat - Warning - Hammer'),
-- -- Paletress
(@PALETRESS,1,0,'Bien entonces. Comencemos.',14,0,100,0,0,16247,'Paletress - Combat - Aggro'),
(@PALETRESS,2,0,'Aprovecha este tiempo para pensar en tus hazanas.',14,0,100,0,0,16248,'Paletress - Combat - Summon Memory'),
(@PALETRESS,3,0,'Descansa.',14,0,100,0,0,16250,'Paletress - Combat - Slay 1'),
(@PALETRESS,3,1,'Ve en paz.',14,0,100,0,0,16251,'Paletress - Combat - Slay 2'),
(@PALETRESS,4,0,'Un trabajo excelente!',14,0,100,0,0,16252,'Paletress - Combat - Death'),
(@PALETRESS,5,0,'Incluso el recuerdo mas oscuro se desvanece al afrontarlo.',14,0,100,0,0,16249,'Paletress - Combat - Memory dies'),
(@PALETRESS,6,0,'$N comienza a lanzar Pesadilla Lucida!',41,0,100,0,0,0,'Paletress - Memory warning'),
-- -- Black Knight
(@BLACK_KNIGHT,1,0,'Esta farsa acaba aqui!',14,0,100,0,0,16259,'Black Knight - Combat - Aggro'),
(@BLACK_KNIGHT,2,0,'Patetico.',14,0,100,0,0,16260,'Black Knight - Combat - Slay 1'),
(@BLACK_KNIGHT,2,1,'Que desperdicio!',14,0,100,0,0,16261,'Black Knight - Combat - Slay 2'),
(@BLACK_KNIGHT,3,0,'Me estorbaba esa carne putrefacta!',14,0,100,0,0,16262,'Black Knight - Combat - Skeleton Res'),
(@BLACK_KNIGHT,4,0,'No necesito huesos para vencerte.',14,0,100,0,0,16263,'Black Knight - Combat - Ghost Res'),
(@BLACK_KNIGHT,5,0,'No! No debo fallar... otra vez...',14,0,100,0,0,16264,'Black Knight - Combat - Death');

-- Abrimos ToC 5
DELETE FROM `disables` WHERE `sourceType` = '2' AND `entry` = '650';
