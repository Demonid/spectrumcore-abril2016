/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: custom_Commandscript
%Complete: 90 will never complete
Comment: commandscript for custom commands
Category: Scripts
EndScriptData */

#include "AccountMgr.h"
#include "ArenaTeam.h"
#include "ArenaTeamMgr.h"
#include "Chat.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include <vector>

/*
-- player punishment SQL
CREATE TABLE `custom_punishment` (
    `guid` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`guid`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;
*/

static void PunishPlayer(Player* player)
{
    static uint32 const gear[] =
    {
        51538, 51540, 51536, 51539, 51537, 51327, 51328, 51365, 51366, 40850, 51534,
        51406, 51397, 51398, 51399, 51337, 51338, 51453, 51454, 51404, 51456, 51400,
        51403, 51401, 51405, 51457, 51402, 51532, 51531, 51410, 51451, 51407, 51396,
        51408, 51409, 51543, 51545, 51541, 51542, 51544, 51476, 51479, 51474, 51475,
        51477, 51470, 51473, 51468, 51469, 51471, 51465, 51467, 51463, 51464, 51466,
        51499, 51502, 51497, 51498, 51500, 51505, 51508, 51503, 51504, 51506, 51511,
        51514, 51509, 51510, 51512, 51460, 51462, 51458, 51459, 51461, 51494, 51496,
        51492, 51493, 51495, 51421, 51424, 51419, 51420, 51422, 51427, 51430, 51425,
        51426, 51428, 51435, 51438, 51433, 51434, 51436, 51489, 51491, 51487, 51488,
        51490, 51484, 51486, 51482, 51483, 51485, 51415, 51418, 51413, 51414, 51416,
        51374, 51375, 51371, 51372, 51350, 51351, 51343, 51344, 51340, 51341, 51368,
        51369, 51359, 51360, 51362, 51363, 51429, 51437, 51423, 51478, 51472, 42591,
        42585, 42580, 42854, 42616, 51523, 51530, 51443, 51517, 51527, 51441, 51521,
        51447, 51392, 51515, 51525, 51439, 51524, 51529, 51444, 51518, 51442, 51528,
        51522, 51448, 51393, 51516, 51440, 51526, 51388, 51519, 51445, 51390, 51480,
        51431, 51394, 51411, 51449, 51389, 51520, 51446, 51391, 51481, 51432, 51395,
        51412, 51450, 51452, 51455, 51533, 51535, 40830, 40871, 40791, 40811, 40851,
        41855, 41870, 41860, 41875, 41865, 41916, 41935, 41922, 41941, 41928, 41328,
        41282, 41317, 41294, 41305, 41679, 41716, 41662, 41774, 41668, 41322, 41276,
        41311, 41288, 41299, 41673, 41684, 41651, 41768, 41656, 41158, 41218, 41088,
        41144, 41206, 41020, 41045, 40995, 41008, 41034, 41152, 41212, 41082, 41138,
        41200, 41014, 41039, 40994, 41002, 41028, 41947, 41966, 41954, 41972, 41960,
        41994, 42012, 41999, 42018, 42006, 40934, 40964, 40910, 40928, 40940, 40831,
        40872, 40792, 40812, 40852, 40829, 40870, 40790, 40810, 40850, 51534, 51361,
        51334, 51348, 51330, 51346, 51332, 51354, 51356, 51336, 51358, 51377, 51378,
        51335, 51349, 51331, 51347, 51333, 51353, 51355, 51357, 31534, 51329, 51417,
        51367, 51339, 51345, 51342, 51370, 51376, 51373, 51507, 51513, 51501, 51364,
        51352
    };

    std::vector<uint32> gearList;
    for (uint16 i = 0; i < (sizeof(gear) / sizeof(*gear)); i++)
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(gear[i]))
            if (player->CanUseItem(itemTemplate) == EQUIP_ERR_OK)
                gearList.push_back(gear[i]);

    for (std::vector<uint32>::const_iterator it = gearList.begin(); it < gearList.end(); it++)
        player->DestroyItemCount(*it, -1, true, false);

    player->SetArenaPoints(0);
    player->SetHonorPoints(0);
}

static std::unordered_set<uint32> punishedplayers;

class player_punishment : PlayerScript
{
public:
    player_punishment() : PlayerScript("player_punishment")
    {
        punishedplayers.clear();

        QueryResult result = CharacterDatabase.Query("SELECT `guid` from custom_punishment");
        if (!result)
            return;

        do
        {
            Field* fields = result->Fetch();
            punishedplayers.insert(fields[0].GetUInt32());
        } while (result->NextRow());
    }

    void OnLogin(Player* player, bool firstlogin) override
    {
        if (punishedplayers.find(player->GetGUID().GetCounter()) == punishedplayers.end())
            return;

        PunishPlayer(player);
        punishedplayers.erase(player->GetGUID().GetCounter());
        CharacterDatabase.PExecute("DELETE FROM custom_punishment WHERE `guid` = %u", player->GetGUID().GetCounter());
    }
};

class custom_commandscript : public CommandScript
{
public:
    custom_commandscript() : CommandScript("custom_commandscript"){ }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> utilityCommandTable =
        {
            { "customize",        rbac::RBAC_PERM_COMMAND_UTILITY_CUSTOMIZE,      false,  &HandleCustomizeCommand,             "" },
            { "changerace",       rbac::RBAC_PERM_COMMAND_UTILITY_CHANGERACE,     false,  &HandleChangeRaceCommand,            "" },
            { "changefaction",    rbac::RBAC_PERM_COMMAND_UTILITY_CHANGEFACTION,  false,  &HandleChangeFactionCommand,         "" },
            { "unbind",           rbac::RBAC_PERM_COMMAND_UTILITY_UNBIND,         false,  &HandleUnbindCommand,                "" },
            { "additem",          rbac::RBAC_PERM_COMMAND_UTILITY_ADDITEM,        false,  &HandleAddItemCommand,               "" },
        };
        static std::vector<ChatCommand> onlineCommandTable =
        {
            { "account",          rbac::RBAC_PERM_COMMAND_ONLINE_ACC,             true,   &HandleOnlineAccountCommand,         "" },
            { "character",        rbac::RBAC_PERM_COMMAND_ONLINE_CHAR,            true,   &HandleOnlineCharacterCommand,       "" },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "utility",          rbac::RBAC_PERM_COMMAND_UTILITY,                true,   NULL,                                "", utilityCommandTable },
            { "online",           rbac::RBAC_PERM_COMMAND_ONLINE,                 true,   NULL,                                "", onlineCommandTable },
            { "wintrade",         rbac::RBAC_PERM_COMMAND_WINTRADE,               true,   &HandleWintradeCommand,              "" },
            { "disbandarena",     rbac::RBAC_PERM_COMMAND_DISBAND_ARENA,          true,   &HandleArenaDisbandCommand,          "" },
        };
        return commandTable;
    }

    static bool HandleCustomizeCommand(ChatHandler* handler, const char* args)
    {
        Player* me = handler->GetSession()->GetPlayer();
        me->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
        handler->PSendSysMessage("Relog to customize your character");
        return true;
    }

    static bool HandleChangeRaceCommand(ChatHandler* handler, const char* args)
    {
        Player* me = handler->GetSession()->GetPlayer();
        me->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
        handler->PSendSysMessage("Relog to change race of your character");
        return true;
    }

    static bool HandleChangeFactionCommand(ChatHandler* handler, const char* args)
    {
        Player* me = handler->GetSession()->GetPlayer();
        me->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
        handler->PSendSysMessage("Relog to change faction of your character");
        return true;
    }

    static bool HandleUnbindCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->GetSession()->GetPlayer();
        if (!playerTarget)
            playerTarget = player;

        for (uint8 i = 0; i < 3; ++i)
        {
            Player::BoundInstancesMap &binds = playerTarget->GetBoundInstances(Difficulty(i));
            for (Player::BoundInstancesMap::iterator itr = binds.begin(); itr != binds.end();)
            {
                playerTarget->UnbindInstance(itr, Difficulty(i));
            }
        }

        handler->PSendSysMessage("Your instance binds have been reseted");
        return true;
    }

    static bool HandleAddItemCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        uint32 itemId = 0;

        if (args[0] == '[')
        {
            char const* itemNameStr = strtok((char*)args, "]");

            if (itemNameStr && itemNameStr[0])
            {
                std::string itemName = itemNameStr+1;
                WorldDatabase.EscapeString(itemName);

                PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ITEM_TEMPLATE_BY_NAME);
                stmt->setString(0, itemName);
                PreparedQueryResult result = WorldDatabase.Query(stmt);

                if (!result)
                {
                    handler->PSendSysMessage(LANG_COMMAND_COULDNOTFIND, itemNameStr+1);
                    handler->SetSentErrorMessage(true);
                    return false;
                }
                itemId = result->Fetch()->GetUInt32();
            }
            else
                return false;
        }
        else
        {
            char const* id = handler->extractKeyFromLink((char*)args, "Hitem");
            if (!id)
                return false;
            itemId = uint32(atol(id));
        }

        char const* ccount = strtok(NULL, " ");

        int32 count = 1;

        if (ccount)
            count = strtol(ccount, NULL, 10);

        if (count == 0)
            count = 1;

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->GetSession()->GetPlayer();
        if (!playerTarget)
            playerTarget = player;

        TC_LOG_DEBUG("misc", handler->GetTrinityString(LANG_ADDITEM), itemId, count);

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (count < 0)
        {
            playerTarget->DestroyItemCount(itemId, -count, true, false);
            handler->PSendSysMessage(LANG_REMOVEITEM, itemId, -count, handler->GetNameLink(playerTarget).c_str());
            return true;
        }

        uint32 noSpaceForCount = 0;

        ItemPosCountVec dest;
        InventoryResult msg = playerTarget->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, count, &noSpaceForCount);
        if (msg != EQUIP_ERR_OK)
            count -= noSpaceForCount;

        if (count == 0 || dest.empty())
        {
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);
            handler->SetSentErrorMessage(true);
            return false;
        }

        Item* item = playerTarget->StoreNewItem(dest, itemId, true, Item::GenerateItemRandomPropertyId(itemId));

        if (count > 0 && item)
        {
            player->SendNewItem(item, count, false, true);
            if (player != playerTarget)
                playerTarget->SendNewItem(item, count, true, false);
        }

        if (noSpaceForCount > 0)
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);

        return true;
    }

    //Added a way to see if a player is on a different character without having to pinfo/lookup all toons on the account
    static bool HandleOnlineAccountCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        std::string account = strtok((char*)args, " ");
        char* limitStr = strtok(NULL, " ");
        int32 limit = limitStr ? atoi(limitStr) : -1;

        if (!AccountMgr::normalizeString(account))
            return false;

        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_LIST_BY_ONLINE_NAME);
        stmt->setString(0, account);
        PreparedQueryResult result = LoginDatabase.Query(stmt);

        return OnlineSearchCommand(result, limit, handler);
    }

    //Using the same way as above but using a character name to get the account id
    static bool HandleOnlineCharacterCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* name = strtok((char*)args, " ");
        char* limitStr = strtok(NULL, " ");
        int32 limit = limitStr ? atoi(limitStr) : -1;
        name[0] = toupper(name[0]);
        uint32 account = 0;
        account = AccountMgr::GetId(name);
        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_LIST_BY_ONLINE_ID);
        stmt->setUInt32(0, account);
        PreparedQueryResult result = LoginDatabase.Query(stmt);
        return OnlineSearchCommand(result, limit, handler);
    }

    static bool OnlineSearchCommand(PreparedQueryResult result, int32 limit, ChatHandler* handler)
    {
        if (!result)
        {
            handler->PSendSysMessage("Player is not online!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        int32 counter = 0;
        uint32 count = 0;
        uint32 maxResults = sWorld->getIntConfig(CONFIG_MAX_RESULTS_LOOKUP_COMMANDS);

        do
        {
            if (maxResults && count++ == maxResults)
            {
                handler->PSendSysMessage("LOOKUP MAX RESULT: %i", maxResults);
                return true;
            }

            Field* fields           = result->Fetch();
            uint32 accountId        = fields[0].GetUInt32();
            std::string accountName = fields[1].GetString();

            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_GUID_NAME_BY_ONLINE);
            stmt->setUInt32(0, accountId);
            PreparedQueryResult result2 = CharacterDatabase.Query(stmt);

            if (result2)
            {
                handler->PSendSysMessage("Looking up Account: %s [id: %i]", accountName.c_str(), accountId);

                do
                {
                    Field* characterFields  = result2->Fetch();
                    uint32 guid             = characterFields[0].GetUInt32();
                    std::string name        = characterFields[1].GetString();

                    handler->PSendSysMessage("Character: %s [guid: %i] is online.", name.c_str(), guid);
                    ++counter;
                }
                while (result2->NextRow() && (limit == -1 || counter < limit));
            }
        }
        while (result->NextRow());

        if (counter == 0) // empty accounts only
        {
            handler->PSendSysMessage("Player is not online!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }

    static bool HandleWintradeCommand(ChatHandler* handler, char const* args)
    {
        char const* TargetName = strtok((char*)args, " "); // get entered name
        if (!TargetName) // if no name entered use target
        {
            if (Player* player = handler->getSelectedPlayer())
            {
                PunishPlayer(player);
                handler->PSendSysMessage("%s punished", player->GetName().c_str());
                return true;
            }

            handler->PSendSysMessage("Select a player or insert a player name");
            handler->SetSentErrorMessage(true);
            return false;
        }

        std::string name = TargetName;
        normalizePlayerName(name);

        if (Player* player = ObjectAccessor::FindPlayerByName(name))
        {
            PunishPlayer(player);
            handler->PSendSysMessage("%s punished", player->GetName().c_str());
            return true;
        }

        ObjectGuid guid = sObjectMgr->GetPlayerGUIDByName(name);
        if (guid != ObjectGuid::Empty)
        {
            // delay punishment to login
            uint32 lowguid = guid.GetCounter();
            punishedplayers.insert(lowguid);
            CharacterDatabase.PExecute("REPLACE INTO custom_punishment (`guid`) VALUES (%u)", lowguid);
            handler->PSendSysMessage("%s punished", name.c_str());
            return true;
        }

        handler->PSendSysMessage("No players online or offline with name \"%s\"", name.c_str());
        handler->SetSentErrorMessage(true);
        return false;
    }

    static bool HandleArenaDisbandCommand(ChatHandler* handler, char const* args)
    {

       std::string name;
       Player* player;

       char const* TargetName = strtok((char*)args, " "); // get entered name
       if (!TargetName) // if no name entered use target
       {
            player = handler->getSelectedPlayer();
            if (player) //prevent crash with creature as target
            {
                name = player->GetName();
                normalizePlayerName(name);
            }
        }
        else // if name entered
        {
            name = TargetName;
            normalizePlayerName(name);
            player = ObjectAccessor::FindPlayerByName(name);
        }

        if (!player)
        {
            handler->PSendSysMessage(LANG_NO_PLAYERS_FOUND);
            return true;
        }

        for (uint32 arena_slot = 0; arena_slot < MAX_ARENA_SLOT; ++arena_slot)
        {
          ArenaTeam* arena = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(arena_slot));
 
          if (!arena)
            continue;

          if (arena->IsFighting())
            continue;

          arena->Disband(handler->GetSession());
        }
 
       return true;
    }
};

void AddSC_custom_commandscript()
{
    new player_punishment();
    new custom_commandscript();
}
