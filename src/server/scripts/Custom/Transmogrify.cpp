
#include "Common.h"
#include "ScriptMgr.h"
#include "Player.h"
#include "Creature.h"
#include "GossipDef.h"
#include "ScriptedGossip.h"
#include "Transmogrification.h"
#include "WorldSession.h"
#include "ObjectMgr.h"

enum ArenaSlots
{
    ARENA_2V2,
    ARENA_3V3,
    ARENA_5V5,
};

struct GossipData
{
    uint32 rating;
    ArenaSlots arenaslot;
    std::string label;
    std::vector< std::pair<uint32, EquipmentSlots> > items;
};

static const std::map< Classes, std::vector<GossipData> > data =
{
    { CLASS_PALADIN,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 27704, EQUIPMENT_SLOT_HEAD },
            { 27706, EQUIPMENT_SLOT_SHOULDERS },
            { 27702, EQUIPMENT_SLOT_CHEST },
            { 27703, EQUIPMENT_SLOT_HANDS },
            { 27705, EQUIPMENT_SLOT_LEGS },
            { 28641, EQUIPMENT_SLOT_WAIST },
            { 28642, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 31997, EQUIPMENT_SLOT_HEAD },
            { 31996, EQUIPMENT_SLOT_SHOULDERS },
            { 31992, EQUIPMENT_SLOT_CHEST },
            { 31993, EQUIPMENT_SLOT_HANDS },
            { 31995, EQUIPMENT_SLOT_LEGS },
            { 32801, EQUIPMENT_SLOT_WAIST },
            { 32789, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33697, EQUIPMENT_SLOT_HEAD },
            { 33699, EQUIPMENT_SLOT_SHOULDERS },
            { 33695, EQUIPMENT_SLOT_CHEST },
            { 33696, EQUIPMENT_SLOT_HANDS },
            { 33698, EQUIPMENT_SLOT_LEGS },
            { 32342, EQUIPMENT_SLOT_WAIST },
            { 33890, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35029, EQUIPMENT_SLOT_HEAD },
            { 35031, EQUIPMENT_SLOT_SHOULDERS },
            { 35027, EQUIPMENT_SLOT_CHEST },
            { 35028, EQUIPMENT_SLOT_HANDS },
            { 35030, EQUIPMENT_SLOT_LEGS },
            { 35155, EQUIPMENT_SLOT_WAIST },
            { 35140, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_WARRIOR,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 24545, EQUIPMENT_SLOT_HEAD },
            { 24546, EQUIPMENT_SLOT_SHOULDERS },
            { 24544, EQUIPMENT_SLOT_CHEST },
            { 24549, EQUIPMENT_SLOT_HANDS },
            { 24547, EQUIPMENT_SLOT_LEGS },
            { 28385, EQUIPMENT_SLOT_WAIST },
            { 28383, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 30488, EQUIPMENT_SLOT_HEAD },
            { 30490, EQUIPMENT_SLOT_SHOULDERS },
            { 30486, EQUIPMENT_SLOT_CHEST },
            { 30487, EQUIPMENT_SLOT_HANDS },
            { 30489, EQUIPMENT_SLOT_LEGS },
            { 32805, EQUIPMENT_SLOT_WAIST },
            { 32793, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33730, EQUIPMENT_SLOT_HEAD },
            { 33732, EQUIPMENT_SLOT_SHOULDERS },
            { 33728, EQUIPMENT_SLOT_CHEST },
            { 33729, EQUIPMENT_SLOT_HANDS },
            { 33731, EQUIPMENT_SLOT_LEGS },
            { 33811, EQUIPMENT_SLOT_WAIST },
            { 33812, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35068, EQUIPMENT_SLOT_HEAD },
            { 35070, EQUIPMENT_SLOT_SHOULDERS },
            { 35066, EQUIPMENT_SLOT_CHEST },
            { 35067, EQUIPMENT_SLOT_HANDS },
            { 35069, EQUIPMENT_SLOT_LEGS },
            { 35161, EQUIPMENT_SLOT_WAIST },
            { 35146, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_DEATH_KNIGHT,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 24545, EQUIPMENT_SLOT_HEAD },
            { 24546, EQUIPMENT_SLOT_SHOULDERS },
            { 24544, EQUIPMENT_SLOT_CHEST },
            { 24549, EQUIPMENT_SLOT_HANDS },
            { 24547, EQUIPMENT_SLOT_LEGS },
            { 28385, EQUIPMENT_SLOT_WAIST },
            { 28383, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 30488, EQUIPMENT_SLOT_HEAD },
            { 30490, EQUIPMENT_SLOT_SHOULDERS },
            { 30486, EQUIPMENT_SLOT_CHEST },
            { 30487, EQUIPMENT_SLOT_HANDS },
            { 30489, EQUIPMENT_SLOT_LEGS },
            { 32805, EQUIPMENT_SLOT_WAIST },
            { 32793, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33730, EQUIPMENT_SLOT_HEAD },
            { 33732, EQUIPMENT_SLOT_SHOULDERS },
            { 33728, EQUIPMENT_SLOT_CHEST },
            { 33729, EQUIPMENT_SLOT_HANDS },
            { 33731, EQUIPMENT_SLOT_LEGS },
            { 33811, EQUIPMENT_SLOT_WAIST },
            { 33812, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35068, EQUIPMENT_SLOT_HEAD },
            { 35070, EQUIPMENT_SLOT_SHOULDERS },
            { 35066, EQUIPMENT_SLOT_CHEST },
            { 35067, EQUIPMENT_SLOT_HANDS },
            { 35069, EQUIPMENT_SLOT_LEGS },
            { 35161, EQUIPMENT_SLOT_WAIST },
            { 35146, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_HUNTER,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 28331, EQUIPMENT_SLOT_HEAD },
            { 28333, EQUIPMENT_SLOT_SHOULDERS },
            { 28334, EQUIPMENT_SLOT_CHEST },
            { 28335, EQUIPMENT_SLOT_HANDS },
            { 28332, EQUIPMENT_SLOT_LEGS },
            { 28450, EQUIPMENT_SLOT_WAIST },
            { 28449, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 31962, EQUIPMENT_SLOT_HEAD },
            { 31964, EQUIPMENT_SLOT_SHOULDERS },
            { 31960, EQUIPMENT_SLOT_CHEST },
            { 31961, EQUIPMENT_SLOT_HANDS },
            { 31963, EQUIPMENT_SLOT_LEGS },
            { 32797, EQUIPMENT_SLOT_WAIST },
            { 32785, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33666, EQUIPMENT_SLOT_HEAD },
            { 33668, EQUIPMENT_SLOT_SHOULDERS },
            { 33664, EQUIPMENT_SLOT_CHEST },
            { 33665, EQUIPMENT_SLOT_HANDS },
            { 33667, EQUIPMENT_SLOT_LEGS },
            { 33877, EQUIPMENT_SLOT_WAIST },
            { 33878, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 34992, EQUIPMENT_SLOT_HEAD },
            { 34994, EQUIPMENT_SLOT_SHOULDERS },
            { 34990, EQUIPMENT_SLOT_CHEST },
            { 34991, EQUIPMENT_SLOT_HANDS },
            { 34993, EQUIPMENT_SLOT_LEGS },
            { 35151, EQUIPMENT_SLOT_WAIST },
            { 35136, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_SHAMAN,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 25998, EQUIPMENT_SLOT_HEAD },
            { 25999, EQUIPMENT_SLOT_SHOULDERS },
            { 25997, EQUIPMENT_SLOT_CHEST },
            { 26000, EQUIPMENT_SLOT_HANDS },
            { 26001, EQUIPMENT_SLOT_LEGS },
            { 28639, EQUIPMENT_SLOT_WAIST },
            { 28640, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 32006, EQUIPMENT_SLOT_HEAD },
            { 32008, EQUIPMENT_SLOT_SHOULDERS },
            { 32004, EQUIPMENT_SLOT_CHEST },
            { 32005, EQUIPMENT_SLOT_HANDS },
            { 32007, EQUIPMENT_SLOT_LEGS },
            { 32803, EQUIPMENT_SLOT_WAIST },
            { 32792, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33708, EQUIPMENT_SLOT_HEAD },
            { 33710, EQUIPMENT_SLOT_SHOULDERS },
            { 33706, EQUIPMENT_SLOT_CHEST },
            { 33707, EQUIPMENT_SLOT_HANDS },
            { 33709, EQUIPMENT_SLOT_LEGS },
            { 33895, EQUIPMENT_SLOT_WAIST },
            { 33896, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35044, EQUIPMENT_SLOT_HEAD },
            { 35046, EQUIPMENT_SLOT_SHOULDERS },
            { 35042, EQUIPMENT_SLOT_CHEST },
            { 35043, EQUIPMENT_SLOT_HANDS },
            { 35045, EQUIPMENT_SLOT_LEGS },
            { 35157, EQUIPMENT_SLOT_WAIST },
            { 35142, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_ROGUE,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 25830, EQUIPMENT_SLOT_HEAD },
            { 25832, EQUIPMENT_SLOT_SHOULDERS },
            { 25831, EQUIPMENT_SLOT_CHEST },
            { 25834, EQUIPMENT_SLOT_HANDS },
            { 25833, EQUIPMENT_SLOT_LEGS },
            { 28423, EQUIPMENT_SLOT_WAIST },
            { 28422, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 31999, EQUIPMENT_SLOT_HEAD },
            { 32001, EQUIPMENT_SLOT_SHOULDERS },
            { 32002, EQUIPMENT_SLOT_CHEST },
            { 31998, EQUIPMENT_SLOT_HANDS },
            { 32000, EQUIPMENT_SLOT_LEGS },
            { 32802, EQUIPMENT_SLOT_WAIST },
            { 32790, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33701, EQUIPMENT_SLOT_HEAD },
            { 33703, EQUIPMENT_SLOT_SHOULDERS },
            { 33704, EQUIPMENT_SLOT_CHEST },
            { 33700, EQUIPMENT_SLOT_HANDS },
            { 33702, EQUIPMENT_SLOT_LEGS },
            { 33891, EQUIPMENT_SLOT_WAIST },
            { 33892, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35033, EQUIPMENT_SLOT_HEAD },
            { 35035, EQUIPMENT_SLOT_SHOULDERS },
            { 35036, EQUIPMENT_SLOT_CHEST },
            { 35032, EQUIPMENT_SLOT_HANDS },
            { 35034, EQUIPMENT_SLOT_LEGS },
            { 35156, EQUIPMENT_SLOT_WAIST },
            { 35141, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_DRUID,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 28127, EQUIPMENT_SLOT_HEAD },
            { 28129, EQUIPMENT_SLOT_SHOULDERS },
            { 28130, EQUIPMENT_SLOT_CHEST },
            { 28126, EQUIPMENT_SLOT_HANDS },
            { 28128, EQUIPMENT_SLOT_LEGS },
            { 28443, EQUIPMENT_SLOT_WAIST },
            { 28444, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 31968, EQUIPMENT_SLOT_HEAD },
            { 31971, EQUIPMENT_SLOT_SHOULDERS },
            { 31972, EQUIPMENT_SLOT_CHEST },
            { 31967, EQUIPMENT_SLOT_HANDS },
            { 31969, EQUIPMENT_SLOT_LEGS },
            { 32798, EQUIPMENT_SLOT_WAIST },
            { 32786, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33672, EQUIPMENT_SLOT_HEAD },
            { 33674, EQUIPMENT_SLOT_SHOULDERS },
            { 33675, EQUIPMENT_SLOT_CHEST },
            { 33671, EQUIPMENT_SLOT_HANDS },
            { 33673, EQUIPMENT_SLOT_LEGS },
            { 33879, EQUIPMENT_SLOT_WAIST },
            { 33880, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 34999, EQUIPMENT_SLOT_HEAD },
            { 35001, EQUIPMENT_SLOT_SHOULDERS },
            { 35002, EQUIPMENT_SLOT_CHEST },
            { 34998, EQUIPMENT_SLOT_HANDS },
            { 35000, EQUIPMENT_SLOT_LEGS },
            { 35152, EQUIPMENT_SLOT_WAIST },
            { 35137, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_MAGE,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 25855, EQUIPMENT_SLOT_HEAD },
            { 25854, EQUIPMENT_SLOT_SHOULDERS },
            { 25856, EQUIPMENT_SLOT_CHEST },
            { 25857, EQUIPMENT_SLOT_HANDS },
            { 25858, EQUIPMENT_SLOT_LEGS },
            { 28409, EQUIPMENT_SLOT_WAIST },
            { 28410, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 32048, EQUIPMENT_SLOT_HEAD },
            { 32047, EQUIPMENT_SLOT_SHOULDERS },
            { 32050, EQUIPMENT_SLOT_CHEST },
            { 32049, EQUIPMENT_SLOT_HANDS },
            { 32051, EQUIPMENT_SLOT_LEGS },
            { 32807, EQUIPMENT_SLOT_WAIST },
            { 32795, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33758, EQUIPMENT_SLOT_HEAD },
            { 33757, EQUIPMENT_SLOT_SHOULDERS },
            { 33760, EQUIPMENT_SLOT_CHEST },
            { 33759, EQUIPMENT_SLOT_HANDS },
            { 33761, EQUIPMENT_SLOT_LEGS },
            { 33912, EQUIPMENT_SLOT_WAIST },
            { 33914, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35097, EQUIPMENT_SLOT_HEAD },
            { 35096, EQUIPMENT_SLOT_SHOULDERS },
            { 35099, EQUIPMENT_SLOT_CHEST },
            { 35098, EQUIPMENT_SLOT_HANDS },
            { 35100, EQUIPMENT_SLOT_LEGS },
            { 35164, EQUIPMENT_SLOT_WAIST },
            { 35149, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_WARLOCK,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 24553, EQUIPMENT_SLOT_HEAD },
            { 24554, EQUIPMENT_SLOT_SHOULDERS },
            { 24552, EQUIPMENT_SLOT_CHEST },
            { 30188, EQUIPMENT_SLOT_HANDS },
            { 24556, EQUIPMENT_SLOT_LEGS },
            { 28404, EQUIPMENT_SLOT_WAIST },
            { 28402, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 31974, EQUIPMENT_SLOT_HEAD },
            { 31976, EQUIPMENT_SLOT_SHOULDERS },
            { 31977, EQUIPMENT_SLOT_CHEST },
            { 31973, EQUIPMENT_SLOT_HANDS },
            { 31975, EQUIPMENT_SLOT_LEGS },
            { 32799, EQUIPMENT_SLOT_WAIST },
            { 32787, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33677, EQUIPMENT_SLOT_HEAD },
            { 33679, EQUIPMENT_SLOT_SHOULDERS },
            { 33680, EQUIPMENT_SLOT_CHEST },
            { 33676, EQUIPMENT_SLOT_HANDS },
            { 33678, EQUIPMENT_SLOT_LEGS },
            { 33882, EQUIPMENT_SLOT_WAIST },
            { 33884, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35004, EQUIPMENT_SLOT_HEAD },
            { 35006, EQUIPMENT_SLOT_SHOULDERS },
            { 35007, EQUIPMENT_SLOT_CHEST },
            { 35003, EQUIPMENT_SLOT_HANDS },
            { 35005, EQUIPMENT_SLOT_LEGS },
            { 35153, EQUIPMENT_SLOT_WAIST },
            { 35138, EQUIPMENT_SLOT_FEET },
        } },
    } },

    { CLASS_PRIEST,{
        { 2200, ARENA_2V2, "Season 1 - Rating 2200 2v2",{
            { 27708, EQUIPMENT_SLOT_HEAD },
            { 27710, EQUIPMENT_SLOT_SHOULDERS },
            { 27711, EQUIPMENT_SLOT_CHEST },
            { 27707, EQUIPMENT_SLOT_HANDS },
            { 27709, EQUIPMENT_SLOT_LEGS },
            { 28404, EQUIPMENT_SLOT_WAIST },
            { 29003, EQUIPMENT_SLOT_FEET },
        } },
        { 2400, ARENA_2V2, "Season 2 - Rating 2400 2v2",{
            { 32016, EQUIPMENT_SLOT_HEAD },
            { 32018, EQUIPMENT_SLOT_SHOULDERS },
            { 32019, EQUIPMENT_SLOT_CHEST },
            { 32015, EQUIPMENT_SLOT_HANDS },
            { 32017, EQUIPMENT_SLOT_LEGS },
            { 32979, EQUIPMENT_SLOT_WAIST },
            { 32981, EQUIPMENT_SLOT_FEET },
        } },
        { 2600, ARENA_2V2, "Season 3 - Rating 2600 2v2",{
            { 33718, EQUIPMENT_SLOT_HEAD },
            { 33720, EQUIPMENT_SLOT_SHOULDERS },
            { 33721, EQUIPMENT_SLOT_CHEST },
            { 33717, EQUIPMENT_SLOT_HANDS },
            { 33719, EQUIPMENT_SLOT_LEGS },
            { 33900, EQUIPMENT_SLOT_WAIST },
            { 33902, EQUIPMENT_SLOT_FEET },
        } },
        { 2800, ARENA_2V2, "Season 4 - Rating 2800 2v2",{
            { 35054, EQUIPMENT_SLOT_HEAD },
            { 35056, EQUIPMENT_SLOT_SHOULDERS },
            { 35057, EQUIPMENT_SLOT_CHEST },
            { 35053, EQUIPMENT_SLOT_HANDS },
            { 35055, EQUIPMENT_SLOT_LEGS },
            { 35159, EQUIPMENT_SLOT_WAIST },
            { 35144, EQUIPMENT_SLOT_FEET },
        } },
    } },
};

class npc_transmogrify : public CreatureScript
{
public:
    npc_transmogrify() : CreatureScript("npc_transmogrify") { }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        auto it = data.find(static_cast<Classes>(player->getClass()));
        if (it == data.end())
        {
            player->GetSession()->SendNotification("No hay Sets de Transfiguracion disponibles para ti");
            return true;
        }
        for (size_t i = 0; i < it->second.size(); ++i)
        {
            auto& v = it->second[i];
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, v.label, GOSSIP_SENDER_MAIN + 1, i);
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Remove set transmogrifications", GOSSIP_SENDER_MAIN, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();
        if (sender == GOSSIP_SENDER_MAIN)
        {
            bool removed = false;
            for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i)
            {
                if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                {
                    if (uint32 fake = sTransmogrification->GetFakeEntry(item))
                        if (RemoveTransmogIfFromScript(player, item, fake) && !removed)
                            removed = true;
                }
            }
            player->GetSession()->SendAreaTriggerMessage("Transfiguracion removida");
        }
        else if (sender == GOSSIP_SENDER_MAIN + 1)
        {
            auto it = data.find(static_cast<Classes>(player->getClass()));
            if (it == data.end())
            {
                player->GetSession()->SendNotification("No hay Sets de Transfiguracion disponibles para ti");
                return true;
            }
            if (action >= it->second.size())
            {
                OnGossipHello(player, creature);
                return true;
            }
            auto& itemdata = it->second[action];
            if (player->GetArenaPersonalRating(itemdata.arenaslot) < itemdata.rating)
            {
                player->GetSession()->SendNotification("Tu rating no es suficientemente alto para este set");
                OnGossipHello(player, creature);
                return true;
            }
            for (auto& v : itemdata.items)
            {
                Item* itemTransmogrified = player->GetItemByPos(INVENTORY_SLOT_BAG_0, v.second);
                if (!itemTransmogrified)
                    continue;
                if (!sTransmogrification->CanTransmogrifyItemWithItem(player, itemTransmogrified->GetTemplate(), sObjectMgr->GetItemTemplate(v.first)))
                    continue;

                sTransmogrification->SetFakeEntry(player, itemTransmogrified, v.first);

                itemTransmogrified->UpdatePlayedTime(player);

                itemTransmogrified->SetOwnerGUID(player->GetGUID());
                itemTransmogrified->SetNotRefundable(player);
                itemTransmogrified->ClearSoulboundTradeable(player);
            }
        }
        OnGossipHello(player, creature);
        return true;
    }

    static bool RemoveTransmogIfFromScript(Player* player, Item* item, uint32 transmog)
    {
        for (auto& v1 : data)
            for (auto& v2 : v1.second)
                for (auto& v3 : v2.items)
                {
                    if (transmog == v3.first)
                    {
                        sTransmogrification->DeleteFakeEntry(player, item);
                        return true;
                    }
                }
        return false;
    }
};

void AddSC_transmogrify_script()
{
    new npc_transmogrify();
}
