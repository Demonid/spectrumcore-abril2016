#include "Chat.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"

enum AshenRingRecover
{
    FACTION_ASHEN_VEREDICT          = 1156,
    QUEST_CHOOSE_YOUR_PATH          = 24815,
};

#define GMSG 537006

class npc_icc_ring_recover : public CreatureScript
{
public:
    npc_icc_ring_recover() : CreatureScript("npc_icc_ring_recover") { }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        if ((player->GetQuestStatus(QUEST_CHOOSE_YOUR_PATH) != QUEST_STATUS_COMPLETE) && (player->GetQuestStatus(QUEST_CHOOSE_YOUR_PATH) != QUEST_STATUS_REWARDED))
        {
            ChatHandler(player->GetSession()).PSendSysMessage("No tienes la mision [Elige tu camino] completada");
            player->CLOSE_GOSSIP_MENU();
            return true;
        }

        for (int i = 0; i < 20; ++i)
        {
            if (player->HasItemCount(rings[i], 1, true))
            {
                ChatHandler(player->GetSession()).PSendSysMessage("Ya cuentas con un Anillo de Veredicto Cinereo en tu Inventario o Banco");
                player->CLOSE_GOSSIP_MENU();
                return true;
            }
        }

        switch (player->GetReputationRank(FACTION_ASHEN_VEREDICT))
        {
            case REP_FRIENDLY:
            {
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_83:35|t Ring Caster (Friendly)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+1);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_85:35|t Ring Healer (Friendly)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+2);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Strength (Friendly)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+3);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Agility (Friendly)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+4);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_84:35|t Ring Tank (Friendly)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+5);
                break;
            }

            case REP_HONORED:
            {
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_83:35|t Ring Caster (Honored)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+6);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_85:35|t Ring Healer (Honored)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+7);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Strength (Honored)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+8);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Agility (Honored)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+9);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_84:35|t Ring Tank (Honored)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+10);
                break;
            }

            case REP_REVERED:
            {
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_83:35|t Ring Caster (Revered)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+11);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_85:35|t Ring Healer (Revered)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+12);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Strength (Revered)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+13);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Agility (Revered)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+14);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_84:35|t Ring Tank (Revered)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+15);
                break;
            }

            case REP_EXALTED:
            {
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_83:35|t Ring Caster (Exalted)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+16);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_85:35|t Ring Healer (Exalted)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+17);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Strength (Exalted)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+18);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_81:35|t Ring Melee - Agility (Exalted)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+19);
                player->ADD_GOSSIP_ITEM(4,"|TInterface/icons/inv_jewelry_ring_84:35|t Ring Tank (Exalted)",GOSSIP_SENDER_MAIN,GOSSIP_ACTION_INFO_DEF+20);
                break;
            }
        }

        player->SEND_GOSSIP_MENU(GMSG, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* /*creature*/, uint32 /*sender*/, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();

        int id = action - (GOSSIP_ACTION_INFO_DEF + 1);
        if (id < 20)
            player->AddItem(rings[id], 1);

        player->CLOSE_GOSSIP_MENU();
        return true;
    }

private:
    uint32 static const rings[20];
};

uint32 const npc_icc_ring_recover::rings[20] = {
    50377, // Caster Friendly
    50378, // Healer Friendly
    52569, // Melee Strength Friendly
    50376, // Melee Agility Friendly
    50375, // Tank Friendly
    50384, // Caster Honored
    50386, // Healer Honored
    52570, // Melee Strength Honored
    50387, // Melee Agility Honored
    50388, // Tank Honored
    50397, // Caster Revered
    50399, // Healer Revered
    52571, // Melee Strength Revered
    50401, // Melee Agility Revered
    50403, // Tank Revered
    50398, // Caster Exalted
    50400, // Healer Exalted
    52572, // Melee Strength Exalted
    50402, // Melee Agility Exalted
    50404  // Tank Exalted
};

void AddSC_npc_icc_ring_recover()
{
    new npc_icc_ring_recover();
}

/*
-- Reset Ashen Veredict Rings
DELETE FROM `item_instance` WHERE ItemEntry IN (
50377, -- Caster Friendly
50378, -- Healer Friendly
52569, -- Melee Strength Friendly
50376, -- Melee Agility Friendly
50375, -- Tank Friendly
50384, -- Caster Honored
50386, -- Healer Honored
52570, -- Melee Strength Honored
50387, -- Melee Agility Honored
50388, -- Tank Honored
50397, -- Caster Revered
50399, -- Healer Revered
52571, -- Melee Strength Revered
50401, -- Melee Agility Revered
50403, -- Tank Revered
50398, -- Caster Exalted
50400, -- Healer Exalted
52572, -- Melee Strength Exalted
50402, -- Melee Agility Exalted
50404  -- Tank Exalted
);
 
DELETE FROM `character_queststatus` WHERE quest IN (
24815, -- Choose Your Path
24825, -- Path of Wisdom
24830, -- Path of Wisdom
24831, -- Path of Wisdom
24826, -- Path of Vengeance
24832, -- Path of Vengeance
24833, -- Path of Vengeance
25239, -- Path of Might
25240, -- Path of Might
25242, -- Path of Might
24823, -- Path of Destruction
24828, -- Path of Destruction
24829, -- Path of Destruction
24827, -- Path of Courage
24834, -- Path of Courage
24835 -- Path of Courage
);
 
DELETE FROM `character_queststatus_rewarded` WHERE quest IN (
24815, -- Choose Your Path
24825, -- Path of Wisdom
24830, -- Path of Wisdom
24831, -- Path of Wisdom
24826, -- Path of Vengeance
24832, -- Path of Vengeance
24833, -- Path of Vengeance
25239, -- Path of Might
25240, -- Path of Might
25242, -- Path of Might
24823, -- Path of Destruction
24828, -- Path of Destruction
24829, -- Path of Destruction
24827, -- Path of Courage
24834, -- Path of Courage
24835 -- Path of Courage
);
*/
