#include "Chat.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ReputationMgr.h"
#include "ScriptMgr.h"

/*
-- Quest autocompleter SQL
-- Execute to auth DB

CREATE TABLE `quest_completer` (
    `id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;
*/

// Read only! unless in a command, otherwise unsafe
static std::unordered_set<uint32> autocompletequests;

// HandleQuestComplete copy paste from cs_quest.cpp
static bool QuestCompleteHelper(Player* player, uint32 entry)
{
    Quest const* quest = sObjectMgr->GetQuestTemplate(entry);

    // If player doesn't have the quest
    if (!quest || player->GetQuestStatus(entry) == QUEST_STATUS_NONE)
    {
        return false;
    }

    // Add quest items for quests that require items
    for (uint8 x = 0; x < QUEST_ITEM_OBJECTIVES_COUNT; ++x)
    {
        uint32 id = quest->RequiredItemId[x];
        uint32 count = quest->RequiredItemCount[x];
        if (!id || !count)
            continue;

        uint32 curItemCount = player->GetItemCount(id, true);

        ItemPosCountVec dest;
        uint8 msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, id, count - curItemCount);
        if (msg == EQUIP_ERR_OK)
        {
            Item* item = player->StoreNewItem(dest, id, true);
            player->SendNewItem(item, count - curItemCount, true, false);
        }
    }

    // All creature/GO slain/cast (not required, but otherwise it will display "Creature slain 0/10")
    for (uint8 i = 0; i < QUEST_OBJECTIVES_COUNT; ++i)
    {
        int32 creature = quest->RequiredNpcOrGo[i];
        uint32 creatureCount = quest->RequiredNpcOrGoCount[i];

        if (creature > 0)
        {
            if (CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(creature))
                for (uint16 z = 0; z < creatureCount; ++z)
                    player->KilledMonster(creatureInfo, ObjectGuid::Empty);
        }
        else if (creature < 0)
            for (uint16 z = 0; z < creatureCount; ++z)
                player->KillCreditGO(creature);
    }

    // If the quest requires reputation to complete
    if (uint32 repFaction = quest->GetRepObjectiveFaction())
    {
        uint32 repValue = quest->GetRepObjectiveValue();
        uint32 curRep = player->GetReputationMgr().GetReputation(repFaction);
        if (curRep < repValue)
            if (FactionEntry const* factionEntry = sFactionStore.LookupEntry(repFaction))
                player->GetReputationMgr().SetReputation(factionEntry, repValue);
    }

    // If the quest requires a SECOND reputation to complete
    if (uint32 repFaction = quest->GetRepObjectiveFaction2())
    {
        uint32 repValue2 = quest->GetRepObjectiveValue2();
        uint32 curRep = player->GetReputationMgr().GetReputation(repFaction);
        if (curRep < repValue2)
            if (FactionEntry const* factionEntry = sFactionStore.LookupEntry(repFaction))
                player->GetReputationMgr().SetReputation(factionEntry, repValue2);
    }

    // If the quest requires money
    int32 ReqOrRewMoney = quest->GetRewOrReqMoney();
    if (ReqOrRewMoney < 0)
        player->ModifyMoney(-ReqOrRewMoney);

    if (sWorld->getBoolConfig(CONFIG_QUEST_ENABLE_QUEST_TRACKER)) // check if Quest Tracker is enabled
    {
        // prepare Quest Tracker datas
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_QUEST_TRACK_GM_COMPLETE);
        stmt->setUInt32(0, quest->GetQuestId());
        stmt->setUInt32(1, player->GetGUID().GetCounter());

        // add to Quest Tracker
        CharacterDatabase.Execute(stmt);
    }

    player->CompleteQuest(entry);

    ChatHandler(player->GetSession()).PSendSysMessage("La mision [%s] ha sido AutoCompletada.", quest->GetTitle().c_str());
    return true;
}

bool AutoCompleteQuest(Player* player, uint32 entry)
{
    if (autocompletequests.find(entry) != autocompletequests.end())
        return QuestCompleteHelper(player, entry);
    return false;
}

class quest_completer_commandscript : public CommandScript
{
public:
    quest_completer_commandscript() : CommandScript("quest_completer_commandscript")
    {
        ReloadAutocompleteQuests();
    }

    static void ReloadAutocompleteQuests()
    {
        autocompletequests.clear();

        QueryResult result = LoginDatabase.Query("SELECT `id` from quest_completer");
        if (!result)
            return;

        do
        {
            Field* fields = result->Fetch();
            autocompletequests.insert(fields[0].GetUInt32());
        } while (result->NextRow());
    }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> questcompleterCommandTable =
        {
            { "add",              rbac::RBAC_PERM_COMMAND_QUESTCOMPLETER_ADD,     true,   &HandleQuestCompleterAddCommand,     "", },
            { "del",              rbac::RBAC_PERM_COMMAND_QUESTCOMPLETER_DEL,     true,   &HandleQuestCompleterDelCommand,     "", },
            { "reload",           rbac::RBAC_PERM_COMMAND_QUESTCOMPLETER_RELOAD,  true,   &HandleQuestCompleterReloadCommand,  "", },
            { "",                 rbac::RBAC_PERM_COMMAND_QUESTCOMPLETER_STATUS,  false,  &HandleQuestCompleterStatusCommand,  "", },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "qc",               rbac::RBAC_PERM_COMMAND_QUESTCOMPLETER,         true,   NULL,                                "", questcompleterCommandTable },
        };
        return commandTable;
    }

    static bool HandleQuestCompleterReloadCommand(ChatHandler* handler, char const* args)
    {
        ReloadAutocompleteQuests();
        handler->SendSysMessage("Autocomplete quests reloaded");
        return true;
    }

    static bool HandleQuestCompleterStatusCommand(ChatHandler* handler, char const* args)
    {
        char* cId = handler->extractKeyFromLink((char*)args, "Hquest");
        if (!cId)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 entry = atoul(cId);
        Quest const* quest = sObjectMgr->GetQuestTemplate(entry);
        if (!quest)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!AutoCompleteQuest(handler->GetSession()->GetPlayer(), entry))
        {
            handler->PSendSysMessage("La mision [%s] no esta en la lista de misiones autocompletadas, si se encuentra Bug reportala en el Foro.", quest->GetTitle().c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }

    static bool HandleQuestCompleterAddCommand(ChatHandler* handler, char const* args)
    {
        char* cId = handler->extractKeyFromLink((char*)args, "Hquest");
        if (!cId)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 entry = atoul(cId);
        Quest const* quest = sObjectMgr->GetQuestTemplate(entry);
        if (!quest)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        autocompletequests.insert(entry);
        LoginDatabase.PExecute("REPLACE INTO quest_completer (`id`) VALUES (%u)", entry);
        handler->PSendSysMessage("La mision [%s] ha sido agregada a la lista de misiones autocompletables, recuerda usar .qc reload", quest->GetTitle().c_str());
        return true;
    }

    static bool HandleQuestCompleterDelCommand(ChatHandler* handler, char const* args)
    {
        char* cId = handler->extractKeyFromLink((char*)args, "Hquest");
        if (!cId)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 entry = atoul(cId);
        Quest const* quest = sObjectMgr->GetQuestTemplate(entry);
        if (!quest)
        {
            handler->PSendSysMessage("Debes agregar un QuestID o QuestLink.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        autocompletequests.erase(entry);
        LoginDatabase.PExecute("DELETE FROM quest_completer WHERE `id` = %u", entry);
        handler->PSendSysMessage("La mision [%s] ha sido eliminada de la lista de misiones autocompletables, .qc reload", quest->GetTitle().c_str());
        return true;
    }
};

void AddSC_quest_completer_commandscript()
{
    new quest_completer_commandscript();
}
