#include "Creature.h"
#include "SpellMgr.h"
#include "Spell.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "Config.h"

enum TSpellType
{
    NONE,
    PROFESSION,
    WEAPON,
    RIDING,
    CLASS,
    QUEST,
    MOUNT,
};

struct TSpell : public TrainerSpell
{
    TSpell() : racemask(RACEMASK_ALL_PLAYABLE), classmask(CLASSMASK_ALL_PLAYABLE), type(NONE), TrainerSpell()
    {
        RequiresSelf();
    }
    TSpell(TSpellType type, const TrainerSpell& tr, uint32 rrace, uint32 cclass, bool raceandclassaremasks = false) : type(type), TrainerSpell(tr)
    {
        if (!rrace)
            racemask = RACEMASK_ALL_PLAYABLE;
        else
            racemask = raceandclassaremasks ? rrace : 1 << (rrace - 1);

        if (!cclass)
            classmask = CLASSMASK_ALL_PLAYABLE;
        else
            classmask = raceandclassaremasks ? cclass : 1 << (cclass - 1);
        RequiresSelf();
    }

    void RequiresSelf()
    {
        selfteached.clear();
        for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
        {
            if (!learnedSpell[i])
                continue;
            SpellsRequiringSpellMapBounds spellsRequired = sSpellMgr->GetSpellsRequiredForSpellBounds(learnedSpell[i]);
            for (SpellsRequiringSpellMap::const_iterator itr = spellsRequired.first; itr != spellsRequired.second; ++itr)
            {
                for (uint8 j = 0; j < MAX_SPELL_EFFECTS; ++j)
                {
                    if (!learnedSpell[j])
                        continue;
                    if (learnedSpell[j] == itr->second)
                    {
                        bool found = false;
                        for (auto&& spell : selfteached)
                            if (spell == itr->second)
                            {
                                found = true;
                                break;
                            }
                        if (!found)
                            selfteached.push_back(itr->second);
                    }
                }
            }
        }
        std::sort(selfteached.begin(), selfteached.end());
    }

    bool IsSelfTeached(uint32 spell) const { return std::binary_search(selfteached.begin(), selfteached.end(), spell); }

    uint32 racemask;
    uint32 classmask;
    TSpellType type;
    std::vector<uint32> selfteached;
};

typedef std::unordered_multimap < uint32, TSpell* > SpellUSet;
typedef std::vector< TSpell* > SpellList;
static SpellList reqskillspells;
static SpellList generalspells;

static bool enableautolearn = false;

// when to check
static bool checkspelloncreate = false;
static bool checkspellonlogin = true;
static bool checkspellonlevel = true;
static bool checkspellonskillupdate = true;
static bool checkspellontalentupdate = true;

// what to learn
static bool autolearndualspec = true;
static bool autolearnclassspells = true;
static bool autolearnquestclassspells = true;
static bool autolearnweaponskills = true;
static bool autolearnprofessions = true;
static bool autolearnriding = true;
static bool autolearnmounts = true;

class auto_learn_WorldScript : public WorldScript
{
public:
    auto_learn_WorldScript() : WorldScript("auto_learn_WorldScript") { }

    static bool TSpellSkillValueComp(const TSpell* l, const TSpell* r)
    {
        return (l->reqSkillValue < r->reqSkillValue);
    }

    static bool TSpellLevelComp(const TSpell* l, const TSpell* r)
    {
        return (l->reqLevel < r->reqLevel);
    }

    void OnShutdown() override
    {
        // clear old spells if any
        for (auto&& i : generalspells)
            delete i;
        reqskillspells.clear();
        generalspells.clear();
    }

    // Called after the world configuration is (re)loaded.
    void OnConfigLoad(bool /*reload*/) override
    {
        // self explanatory
        enableautolearn = sConfigMgr->GetBoolDefault("AutoLearn.Enable", false);

        // when to check
        checkspelloncreate = sConfigMgr->GetBoolDefault("AutoLearn.Check.Create", false);
        checkspellonlogin = sConfigMgr->GetBoolDefault("AutoLearn.Check.Login", true);
        checkspellonlevel = sConfigMgr->GetBoolDefault("AutoLearn.Check.Level", true);
        checkspellonskillupdate = sConfigMgr->GetBoolDefault("AutoLearn.Check.SkillIncrease", true);
        checkspellontalentupdate = sConfigMgr->GetBoolDefault("AutoLearn.Check.TalentpointChange", true);

        // what to learn
        autolearndualspec = sConfigMgr->GetBoolDefault("AutoLearn.DualSpec", true);
        autolearnclassspells = sConfigMgr->GetBoolDefault("AutoLearn.ClassSpells", true);
        autolearnquestclassspells = sConfigMgr->GetBoolDefault("AutoLearn.ClassSpellsQuest", true);
        autolearnweaponskills = sConfigMgr->GetBoolDefault("AutoLearn.WeaponSkills", true);
        autolearnprofessions = sConfigMgr->GetBoolDefault("AutoLearn.Professions", true);
        autolearnriding = sConfigMgr->GetBoolDefault("AutoLearn.Riding", true);
        autolearnmounts = sConfigMgr->GetBoolDefault("AutoLearn.Mounts", true);

        LoadSpells();
    }

    // handles loading and preparing the spells for learning
    static void LoadSpells()
    {
        uint32 oldMSTime = getMSTime();

        // clear old spells if any
        for (auto&& i : generalspells)
            delete i;
        reqskillspells.clear();
        generalspells.clear();

        if (!enableautolearn)
        {
            TC_LOG_INFO("server.loading", ">> autolearn disabled in config");
        }
        else
        {
            SpellUSet loadedSpells = GetSpellList();
            SpellList sortedSpells = SortSpells(loadedSpells);
            uint32 count = static_cast<uint32>(sortedSpells.size());
            for (auto&& i : sortedSpells)
            {
                if (i->reqSkill || i->reqSkillValue)
                    reqskillspells.push_back(i);
                generalspells.push_back(i);
            }
            std::stable_sort(reqskillspells.begin(), reqskillspells.end(), &TSpellSkillValueComp);

            TC_LOG_INFO("server.loading", ">> Loaded %u autotrained spells in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
        }

        reqskillspells.shrink_to_fit();
        generalspells.shrink_to_fit();
    }

    // Helper function to create trained spells
    // ObjectMgr::AddSpellToTrainer copy paste
    static TrainerSpell CreateTrainerSpell(uint32 spell, uint32 reqLevel = 0, uint32 reqSkill = 0, uint32 reqSkillValue = 0)
    {
        TrainerSpell trainerSpell;
        trainerSpell.spell = spell;
        trainerSpell.spellCost = 0;
        trainerSpell.reqSkill = reqSkill;
        trainerSpell.reqSkillValue = reqSkillValue;
        trainerSpell.reqLevel = reqLevel;
        trainerSpell.learnedSpell[0] = spell;

        const SpellInfo* spellinfo = sSpellMgr->GetSpellInfo(spell);
        if (!spellinfo)
            return trainerSpell;

        if (!trainerSpell.reqLevel)
            trainerSpell.reqLevel = spellinfo->SpellLevel;

        // calculate learned spell for profession case when stored cast-spell
        for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
        {
            if (spellinfo->Effects[i].Effect != SPELL_EFFECT_LEARN_SPELL)
                continue;
            if (trainerSpell.learnedSpell[0] == spell)
                trainerSpell.learnedSpell[0] = 0;
            // player must be able to cast spell on himself
            if (spellinfo->Effects[i].TargetA.GetTarget() != 0 && spellinfo->Effects[i].TargetA.GetTarget() != TARGET_UNIT_TARGET_ALLY
                && spellinfo->Effects[i].TargetA.GetTarget() != TARGET_UNIT_TARGET_ANY && spellinfo->Effects[i].TargetA.GetTarget() != TARGET_UNIT_CASTER)
                continue;

            trainerSpell.learnedSpell[i] = spellinfo->Effects[i].TriggerSpell;

            // This is not needed since it is used only when sending a trainer list to player
            /*
            if (trainerSpell.learnedSpell[i])
            {
            SpellInfo const* learnedSpellInfo = sSpellMgr->GetSpellInfo(trainerSpell.learnedSpell[i]);
            if (learnedSpellInfo && learnedSpellInfo->IsProfession())
            data.trainerType = 2;
            }
            */
        }

        return trainerSpell;
    }

#define COND_CHECK(a)\
    for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)\
    {\
        if (!trainerSpell.learnedSpell[i])\
            continue;\
        SpellInfo const* info = sSpellMgr->GetSpellInfo(trainerSpell.learnedSpell[i]);\
        if (info && (a))\
            return true;\
    }\
    return false;

    static bool IsProfessionSkill(uint32 skill)
    {
        return  IsPrimaryProfessionSkill(skill) || skill == SKILL_FISHING || skill == SKILL_COOKING || skill == SKILL_FIRST_AID;
    }

    static bool IsProfessionSkill(const TrainerSpell& trainerSpell)
    {
        if (IsProfessionSkill(trainerSpell.reqSkill))
            return true;
        COND_CHECK(info->IsProfession());
    }

    static bool TeachesProfession(const TrainerSpell& trainerSpell)
    {
        COND_CHECK(info->IsPrimaryProfessionFirstRank());
    }

    static bool IsRiding(const TrainerSpell& trainerSpell)
    {
        if (trainerSpell.spell == 54197)
            return true;
        SpellInfo const* info = sSpellMgr->GetSpellInfo(trainerSpell.spell);
        if (info && info->IsProfessionOrRiding() && !info->IsProfession())
            return true;
        return false;
    }

    static bool TeachesWeaponSkill(const SpellInfo* info)
    {
        if (info->EquippedItemClass != ITEM_CLASS_WEAPON)
            return false;
        if (!info->HasEffect(SPELL_EFFECT_WEAPON))
            return false;
        if (!info->HasEffect(SPELL_EFFECT_PROFICIENCY))
            return false;
        return true;
    }

    static bool IsWeaponSkill(const TrainerSpell& trainerSpell)
    {
        COND_CHECK(TeachesWeaponSkill(info));
    }

    // Helper function for DFSCycles
    static bool DFSSearchCycles(const std::vector< std::vector<size_t> >& adjacents, std::vector<int>& colors, size_t u)
    {
        colors[u] = 1;
        for (auto&& v : adjacents[u])
        {
            if (colors[v] == 1)
                return true;
            if (colors[v] == 0)
                if (DFSSearchCycles(adjacents, colors, v))
                    return true;
        }
        colors[u] = 2;
        return false;
    }

    // returns true if the graph has a cycle
    static bool DFSCycles(const std::vector< std::vector<size_t> >& adjacents)
    {
        std::vector<int> colors(adjacents.size(), 0);
        for (size_t u = 0; u < adjacents.size(); ++u)
        {
            if (colors[u] == 0)
                if (DFSSearchCycles(adjacents, colors, u))
                    return true;
        }
        return false;
    }

    // Helper function for TopologicalSort
    static void DFSvisit(std::vector<size_t>& result, const std::vector< std::vector<size_t> >& adjacents, std::vector<int>& colors, size_t u)
    {
        colors[u] = 1;
        for (auto&& w : adjacents[u])
            if (colors[w] == 0)
                DFSvisit(result, adjacents, colors, w);
        colors[u] = 2;
        result.push_back(u);
    }

    // Returns a topologically sorted vector where the rightmost element has no connections
    // The passed graph must have no cycles
    static std::vector<size_t> TopologicalSort(const std::vector< std::vector<size_t> >& adjacents)
    {
        assert(!DFSCycles(adjacents) && "Autolearn: Spell list must have no cycles in spell dependecies!");

        std::vector<size_t> result;
        std::vector<int> colors(adjacents.size(), 0);
        for (size_t u = 0; u < adjacents.size(); ++u)
            if (colors[u] == 0)
                DFSvisit(result, adjacents, colors, u);
        return result;
    }

    static bool IsValidHelper(SpellInfo const* info)
    {
        SkillLineAbilityMapBounds bounds = sSpellMgr->GetSkillLineAbilityMapBounds(info->Id);
        SkillLineAbilityMap::const_iterator _spell_idx = bounds.first;
        if (_spell_idx == bounds.second)
            return false;
        return true;
    }

    static bool IsValid(const TrainerSpell& trainerSpell)
    {
        COND_CHECK(IsValidHelper(info));
    }

    static void SpellUSetInsert(SpellUSet& spellset, TSpell* spell)
    {
        auto range = spellset.equal_range(spell->spell);
        for (auto it = range.first; it != range.second; ++it)
        {
            TSpell* spell2 = it->second;

            if (spell->reqLevel == spell2->reqLevel &&
                spell->reqSkill == spell2->reqSkill &&
                spell->reqSkillValue == spell2->reqSkillValue &&
                (spell->type == spell2->type ||
                (spell->type != QUEST && spell2->type != QUEST)))
            {
                spell2->racemask |= spell->racemask;
                spell2->classmask |= spell->classmask;
                delete spell;
                return;
            }
        }
        spellset.insert(std::make_pair(spell->spell, spell));
    }

    // loads a set of spells according to config settings
    static SpellUSet GetSpellList()
    {
        SpellUSet spellset;

        CreatureTemplateContainer const* templates = sObjectMgr->GetCreatureTemplates();
        for (auto&& it : *templates)
        {
            if (it.second.trainer_type == TRAINER_TYPE_PETS)
                continue;
            if (!(it.second.npcflag & UNIT_NPC_FLAG_TRAINER))
                continue;

            const TrainerSpellData* data = sObjectMgr->GetNpcTrainerSpells(it.first);
            if (!data)
                continue;

            for (auto&& itr : data->spellList)
            {
                // Skip actual profession learning spells
                if (TeachesProfession(itr.second))
                    continue;

                if (IsProfessionSkill(itr.second))
                {
                    if (autolearnprofessions)
                        SpellUSetInsert(spellset, new TSpell(PROFESSION, itr.second, it.second.trainer_race, it.second.trainer_class));
                    continue;
                }
                if (IsRiding(itr.second))
                {
                    if (autolearnriding)
                        SpellUSetInsert(spellset, new TSpell(RIDING, itr.second, it.second.trainer_race, it.second.trainer_class));
                    continue;
                }
                if (IsWeaponSkill(itr.second))
                {
                    if (autolearnweaponskills)
                        SpellUSetInsert(spellset, new TSpell(WEAPON, itr.second, it.second.trainer_race, it.second.trainer_class));
                    continue;
                }
                if (autolearnclassspells)
                {
                    if (IsValid(itr.second))
                        SpellUSetInsert(spellset, new TSpell(CLASS, itr.second, it.second.trainer_race, it.second.trainer_class));
                    continue;
                }
            }
        }

        // Get quest related class spells
        if (autolearnquestclassspells)
        {
            QueryResult result = WorldDatabase.Query("select distinct quest_template.id from quest_template inner join quest_template_addon on quest_template.ID = quest_template_addon.ID where -QuestSortID IN (61, 81, 82, 141, 161, 162, 261, 262, 263, 372) and (rewardspell <> 0 or rewarddisplayspell <> 0 or SourceSpellId <> 0)");
            if (result)
            {
                do
                {
                    Field* fields = result->Fetch();
                    uint32 entry = fields[0].GetUInt32();
                    const Quest* quest = sObjectMgr->GetQuestTemplate(entry);

                    if (entry == 12801) // Skip Death Gate (DK)
	                    continue;

                    if (quest->GetRewSpellCast() > 0)
                    {
                        TrainerSpell a = CreateTrainerSpell(quest->GetRewSpellCast(), quest->GetMinLevel(), quest->GetRequiredSkill(), quest->GetRequiredSkillValue());
                        if (a.IsCastable())
                            SpellUSetInsert(spellset, new TSpell(QUEST, a, quest->GetAllowableRaces(), quest->GetRequiredClasses(), true));
                    }
                    else if (quest->GetRewSpell() > 0)
                    {
                        TrainerSpell a = CreateTrainerSpell(quest->GetRewSpell(), quest->GetMinLevel(), quest->GetRequiredSkill(), quest->GetRequiredSkillValue());
                        if (a.IsCastable())
                            SpellUSetInsert(spellset, new TSpell(QUEST, a, quest->GetAllowableRaces(), quest->GetRequiredClasses(), true));
                    }

                    if (quest->GetSrcSpell() > 0)
                    {
                        TrainerSpell a = CreateTrainerSpell(quest->GetSrcSpell(), quest->GetMinLevel(), quest->GetRequiredSkill(), quest->GetRequiredSkillValue());
                        if (a.IsCastable())
                            SpellUSetInsert(spellset, new TSpell(QUEST, a, quest->GetAllowableRaces(), quest->GetRequiredClasses(), true));
                    }
                } while (result->NextRow());
            }
        }

        if (autolearnmounts)
        {
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(458, 20, SKILL_RIDING, 75), RACE_HUMAN, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23228, 40, SKILL_RIDING, 150), RACE_HUMAN, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(64658, 20, SKILL_RIDING, 75), RACE_ORC, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23251, 40, SKILL_RIDING, 150), RACE_ORC, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(6898, 20, SKILL_RIDING, 75), RACE_DWARF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23240, 40, SKILL_RIDING, 150), RACE_DWARF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(8394, 20, SKILL_RIDING, 75), RACE_NIGHTELF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23221, 40, SKILL_RIDING, 150), RACE_NIGHTELF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(64977, 20, SKILL_RIDING, 75), RACE_UNDEAD_PLAYER, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23246, 40, SKILL_RIDING, 150), RACE_UNDEAD_PLAYER, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(64657, 20, SKILL_RIDING, 75), RACE_TAUREN, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23248, 40, SKILL_RIDING, 150), RACE_TAUREN, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(10873, 20, SKILL_RIDING, 75), RACE_GNOME, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23225, 40, SKILL_RIDING, 150), RACE_GNOME, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(8395, 20, SKILL_RIDING, 75), RACE_TROLL, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(23241, 40, SKILL_RIDING, 150), RACE_TROLL, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(35022, 20, SKILL_RIDING, 75), RACE_BLOODELF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(33660, 40, SKILL_RIDING, 150), RACE_BLOODELF, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(35710, 20, SKILL_RIDING, 75), RACE_DRAENEI, 0));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(35713, 40, SKILL_RIDING, 150), RACE_DRAENEI, 0));

            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(32243, 60, SKILL_RIDING, 225), RACEMASK_HORDE, 0, true));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(32246, 70, SKILL_RIDING, 300), RACEMASK_HORDE, 0, true));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(32240, 60, SKILL_RIDING, 225), RACEMASK_ALLIANCE, 0, true));
            SpellUSetInsert(spellset, new TSpell(MOUNT, CreateTrainerSpell(32289, 70, SKILL_RIDING, 300), RACEMASK_ALLIANCE, 0, true));
        }

        return spellset;
    }

    // topologically sorts the spells and then sorts them by level requirement
    static SpellList SortSpells(const SpellUSet& spellset)
    {
        // separate spells that require something or are the requirement for something to an array type container
        // also collect additional data for graph creation
        SpellList connected_spells;
        std::unordered_map< uint32, std::set<size_t> > indexBySpell;
        std::unordered_map< uint32, std::map<uint32, std::set<size_t> > > indexBySkill;
        for (auto&& it : spellset)
        {
            TSpell* tspell = it.second;

            size_t index = connected_spells.size();
            connected_spells.push_back(tspell);

            if (const SpellLearnSkillNode* node = sSpellMgr->GetSpellLearnSkill(tspell->spell))
                indexBySkill[node->skill][node->maxvalue].insert(index);

            for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
            {
                if (!tspell->learnedSpell[i])
                    continue;
                indexBySpell[tspell->learnedSpell[i]].insert(index);
            }
        }

        // record the graph they create from their dependencies
        std::vector< std::vector<size_t> > adjacents(connected_spells.size(), std::vector<size_t>());
        for (size_t it = 0; it < connected_spells.size(); ++it)
        {
            const TSpell* trainerSpell = connected_spells[it];

            auto skillvalues = indexBySkill.find(trainerSpell->reqSkill);
            if (skillvalues != indexBySkill.end())
            {
                for (auto&& skill_range : skillvalues->second)
                {
                    if (skill_range.first > trainerSpell->reqSkillValue)
                        break;
                    for (auto&& spellindex : skill_range.second)
                        if (spellindex != it)
                            adjacents[spellindex].push_back(it);
                }
            }

            for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
            {
                uint32 spell = trainerSpell->learnedSpell[i];
                if (!spell)
                    continue;

                if (uint32 prev = sSpellMgr->GetPrevSpellInChain(spell))
                {
                    auto range = indexBySpell.find(prev);
                    if (range != indexBySpell.end())
                        for (auto&& spellindex : range->second)
                            if (spellindex != it)
                                adjacents[spellindex].push_back(it);
                }

                auto required = sSpellMgr->GetSpellsRequiredForSpellBounds(spell);
                for (auto itr = required.first; itr != required.second; ++itr)
                {
                    if (trainerSpell->IsSelfTeached(itr->second))
                        continue;
                    auto range = indexBySpell.find(itr->second);
                    if (range != indexBySpell.end())
                        for (auto&& spellindex : range->second)
                            if (spellindex != it)
                                adjacents[spellindex].push_back(it);
                }
            }
        }

        // execute a topological sort on graph
        std::vector<size_t> topo = TopologicalSort(adjacents);

        // use the new sort on the spells to create a sorted spell list
        SpellList sorted;
        for (auto it = topo.rbegin(); it != topo.rend(); ++it)
            sorted.push_back(connected_spells[*it]);
        std::stable_sort(sorted.begin(), sorted.end(), &TSpellLevelComp);

        // return the sorted spells
        return sorted;
    }
};

class auto_learn_PlayerScript : public PlayerScript
{
public:
    auto_learn_PlayerScript() : PlayerScript("auto_learn_PlayerScript") { }

    // Called when a player's skill increases
    void OnPlayerSkillUpdate(Player* player, uint16 /*SkillId*/, uint16 /*SkillValue*/, uint16 SkillNewValue) override
    {
        if (enableautolearn && checkspellonskillupdate)
        {
            learnspells(player, reqskillspells, SkillNewValue);
        }
    }

    // Called when a player is created.
    void OnCreate(Player* player) override
    {
        if (enableautolearn && checkspelloncreate)
        {
            dolearn(player);
            player->SaveToDB();
        }
    }

    // Called when a player's level changes (after the level is applied)
    void OnLevelChanged(Player* player, uint8 /*oldLevel*/) override
    {
        if (checkspellonlevel)
            dolearn(player);
    }

    // Called when a player logs in.
    void OnLogin(Player* player, bool /*firstLogin*/) override
    {
        if (checkspellonlogin)
            dolearn(player);
    }

    // Called when a player logs in.
    void OnFreeTalentPointsChanged(Player* player, uint32 /*newPoints*/) override
    {
        if (checkspellontalentupdate)
            dolearn(player);
    }

    // Teaches all spells or removes them if necessary
    static void dolearn(Player* player)
    {
        if (!enableautolearn)
            return;

        learndualspec(player);
        learnspells(player);
    }

    // Teaches dual spec, will not unlearn it.
    static void learndualspec(Player* player)
    {
        if (!autolearndualspec)
            return;

        // ongossipselect copypaste
        if (player->GetSpecsCount() == 1 && player->getLevel() >= sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL))
        {
            // Cast spells that teach dual spec
            // Both are also ImplicitTarget self and must be cast by player
            player->CastSpell(player, 63680, true, NULL, NULL, player->GetGUID());
            player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());
        }
    }

    // Teaches all class spells as well as riding and other
    static void learnspells(Player* player)
    {
        learnspells(player, generalspells);
    }

    // Calls learnspell for all spells in the set
    // if skillorder is false, then levelorder is used
    static void learnspells(Player* player, const SpellList& entries, uint16 skillvalue = 0)
    {
        if (entries.empty())
            return;
        if (!player->IsInWorld())
            return;
        uint8 level = player->getLevel();
        for (auto&& e : entries)
        {
            if (skillvalue)
            {
                if (skillvalue < e->reqSkillValue)
                    break;
            }
            else if (level < e->reqLevel)
                break;
            learnspell(player, e);
        }
    }

    // Learns the spell entry
    static void learnspell(Player* player, const TSpell* entry)
    {
        if (!(entry->classmask & player->getClassMask()))
            return;
        if (!(entry->racemask & player->getRaceMask()))
            return;

        TrainerSpellState state = player->GetTrainerSpellState(entry);
        if (state == TRAINER_SPELL_GREEN)
        {
            // Learn if spell is green
            if (entry->type == QUEST || entry->IsCastable())
                player->CastSpell(player, entry->spell, true);
            else
                player->LearnSpell(entry->spell, false);
        }
    }
};

void AddSC_auto_learn()
{
    new auto_learn_PlayerScript();
    new auto_learn_WorldScript();
}
