#include "AchievementMgr.h"
#include "Chat.h"
#include "Define.h"
#include "GossipDef.h"
#include "Item.h"
#include "Player.h"
#include "ReputationMgr.h"
#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Spell.h"
#include "SpellAuras.h"
#include "WorldSession.h"

class add_item : public PlayerScript
{
public:
    add_item() : PlayerScript("add_item") { }

    void OnLogin(Player * player, bool)
    {
        if (!player->HasItemCount(81000, 1, true))
            player->AddItem(81000, 1);
    }
};

class gm_helper : public ItemScript
{
public:
    gm_helper() : ItemScript("gm_helper") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& targets) override 
    {
        if (player->IsInCombat())
        {
            ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cffff0000No puedes utilizarme mientras estas en combate.|r");
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->InArena() || player->InBattleground())
        {
            ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cffff0000No puedes utilizarme mientras estas en Arenas o Bgs.|r");
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->isDead() && !player->HasAura(8326))
        {
            ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cffff0000Libera para poder utilizarme.|r");
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->IsAlive())
        {
            player->PlayerTalkClass->ClearMenus();
            player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\spell_shadow_teleport:35|t|r Desbloquea mi personaje", GOSSIP_SENDER_MAIN, 2);
            player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\Achievement_BG_hld4bases_EOS:35|t|r Desbugear personaje - Eliminar Auras", GOSSIP_SENDER_MAIN, 3);
            player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\achievement_general:35|t|r Repara mi Doble especializacion", GOSSIP_SENDER_MAIN, 4);
            if (player->HasAtLoginFlag(AT_LOGIN_CHANGE_FACTION))
                player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\spell_chargenegative:35|t|r Eliminar cambio de faccion pendiente", GOSSIP_SENDER_MAIN, 11);
            if (player->GetTeam() == ALLIANCE)
                player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\ability_mount_pinktiger:35|t|r Repara Instructores de sableinvernales", GOSSIP_SENDER_MAIN, 12);
            //player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\INV_Jewelry_Ring_83:35|t|r Recuperar Anillo ICC", GOSSIP_SENDER_MAIN, 13);
            //static const uint32 guids[] =
            //{
            //    1045670, /*Notdirty*/
            //    1042239, /*Kzqkzq*/
            //    1052408, /*Nohesiitaqt*/
            //    1049861, /*Exlash*/
            //    2078717 /*Eatspally*/
            //};
            //if (std::find(std::begin(guids), std::end(guids), player->GetGUID().GetCounter()) != std::end(guids))
            //    player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\achievement_featsofstrength_gladiator_07:35|t|r Reclamar Premios de Arena", GOSSIP_SENDER_MAIN, 14);
            //player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\Temp:35|t|r Preguntas y Respuestas", GOSSIP_SENDER_MAIN, 100);
            player->SEND_GOSSIP_MENU(999000, item->GetGUID());
            return false;
        }
        else
        {
            player->PlayerTalkClass->ClearMenus();
            player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\spell_holy_resurrection:35|t|r Revivir + Dolencia", GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\Achievement_BG_returnXflags_def_WSG:35|t|r Cerrar", GOSSIP_SENDER_MAIN, 0);
            player->SEND_GOSSIP_MENU(999000, item->GetGUID());
            return false;
        }
    }

    void OnGossipSelect(Player* player, Item* item, uint32 sender, uint32 action) override
    {
        player->PlayerTalkClass->ClearMenus();
        switch (action)
        {
            case 0: // Back or Close
            {
                if (player->IsAlive())
                {
                    OnUse(player, item, SpellCastTargets());
                    return;
                }
                else
                    player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 1: // Revive
            {
                player->RepopAtGraveyard();
                player->ResurrectPlayer(!AccountMgr::IsPlayerAccount(player->GetSession()->GetSecurity()) ? 1.0f : 0.5f);
                player->CastSpell(player, 15007, true);
                player->SaveToDB();
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tu personaje ha sido revivido exitosamente.|r");
                player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 2: // Unstuck
            {
                player->CastSpell(player, 35182, true);
                player->CastSpell(player, 7355, true);
                player->RemoveAura(35182);
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tu personaje ha sido desbloqueado exitosamente.|r");
                player->SaveToDB();
                player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 3: // Clean Auras
            {
                static const std::vector<uint32> auras = {26013, 71041, 15007, 38505, 74092};
                std::vector<uint32> auradata(auras.size(), 0);
                for (size_t i = 0; i < auras.size(); ++i)
                    if (Aura* aura = player->GetAura(auras[i]))
                        auradata[i] = aura->GetDuration();

                player->RemoveAllAuras();
                for (size_t i = 0; i < auras.size(); ++i)
                    if (auradata[i])
                        if (Aura* aura = player->AddAura(auras[i], player))
                            aura->SetDuration(auradata[i]);

                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tus auras se han reiniciado exitosamente, relogea para calcular tus estadisticas.|r");
                player->SaveToDB();
                player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 4: // Double Spec
            {
                if (player->HasAchieved(2716))
                {
                    player->RemoveSpell(63645, false, false);
                    player->LearnSpell(63645, false);
                    player->RemoveSpell(63644, false, false);
                    player->LearnSpell(63644, false);
                    ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tu Doble especializacion de talentos se ha reparado exitosamente.|r");
                    player->SaveToDB();
                    player->CLOSE_GOSSIP_MENU();
                }
                else
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cffff0000Tu personaje no cuenta con Doble especializacion de talentos.|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                break;
            }

            case 11: // Cancel Change Faction
            {
                player->RemoveAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tu cambio de faccion ha sido cancelado exitosamente.|r");
                player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 12: // Wintersaber Trainers
            {
                if (!player->GetReputation(589) || player->GetReputationRank(589) < REP_NEUTRAL)
                {
                    player->SetReputation(589, REP_NEUTRAL);
                    ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tu reputacion con [Instructores de sableinvernales] ha sido reparada exitosamente.|r");
                    player->SaveToDB();
                    player->CLOSE_GOSSIP_MENU();
                }
                else
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cffff0000Tu reputacion con [Instructores de sableinvernales] NO esta bug, si no puedes interactuar con ellos, abre la pestaña de reputaciones y desactiva el estado de guerra.|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                break;
            }

            case 13: // The Ashen Verdict Rings
            {
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Entra a ICC y habla con Ashen Veredict Recover para recuperar tu anillo.|r");
                player->CLOSE_GOSSIP_MENU();
                break;
            }

            case 14: // Arena Top Prizes
            {
                player->AddItem(43516, 1);
                player->AddItem(45180, 1);
                CharTitlesEntry const* titleInfo = sCharTitlesStore.LookupEntry(80);
                player->SetTitle(titleInfo);
                AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(420);
                player->CompletedAchievement(achievementEntry);
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "|cff00ff00Tus premios han sido entregados!|r");
                player->SaveToDB();
                player->CLOSE_GOSSIP_MENU();
                break;
            }

          /*case 100: // F.A.Q
            {
                player->PlayerTalkClass->ClearMenus();
                player->ADD_GOSSIP_ITEM(7, "|cff00ff00|TInterface\\icons\\Spell_Arcane_Rune:35|t|r Como reportar un bug?", GOSSIP_SENDER_MAIN, 101);
                player->ADD_GOSSIP_ITEM(7, "|cff00ff00|TInterface\\icons\\Spell_Fire_Rune:35|t|r Como reportar a un jugador?", GOSSIP_SENDER_MAIN, 102);
                player->ADD_GOSSIP_ITEM(7, "|cff00ff00|TInterface\\icons\\Spell_Holy_Rune:35|t|r Como hacer una donacion?", GOSSIP_SENDER_MAIN, 103);
                player->ADD_GOSSIP_ITEM(7, "|cff00ff00|TInterface\\icons\\Spell_Ice_Rune:35|t|r Tengo un problema", GOSSIP_SENDER_MAIN, 104);
                player->ADD_GOSSIP_ITEM(7, "|cff00ff00|TInterface\\icons\\Spell_Shadow_Rune:35|t|r Informacion del servidor", GOSSIP_SENDER_MAIN, 105);
                player->ADD_GOSSIP_ITEM(4, "|cff00ff00|TInterface\\icons\\Achievement_BG_returnXflags_def_WSG:35|t|r Volver", GOSSIP_SENDER_MAIN, 106);
                player->SEND_GOSSIP_MENU(999000, item->GetGUID());
                break;
            }

            case 101: //Reportar bug
            {
                break;
            }
            
            case 102: //Reportar jugador
            {
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "Tu cambio de faccion se ha cancelado exitosamente.");
            }

            case 103: //Hacer donacion
            {
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "Tu cambio de faccion se ha cancelado exitosamente.");
            }

            case 104: //Tengo un problema
            {
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "Tu cambio de faccion se ha cancelado exitosamente.");
            }

            case 105: //Informacion del servidor
            {
                ChatHandler(player->GetSession()).PSendSysMessage("[GM Virtual]: %s", "Tu cambio de faccion se ha cancelado exitosamente.");
            }*/

        }
    }
};

void AddSC_gm_helper()
{
    new gm_helper();
    new add_item();
}
